<%@page import="axiom.web.service.AxiomWrapper"%>
<%@page import="com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails"%>
<%@include file="header.jsp" %>
<link href="./assets/js/select2/select2.css" rel="stylesheet"/>
<script src="./assets/js/select2/select2.js"></script>
<!--<script src="./assets/js/userPortal.js"></script>-->
<div class="container-fluid">
    <h1 class="text-success">User Portal</h1>
    <p>To facilitate user conrtols</p>

    <div class="tabbable">
        <ul class="nav nav-pills">
            <li class="active"><a href="#PasswordReset" data-toggle="tab">Password Reset</a></li>
            <li><a href="#OTP" data-toggle="tab">OTP Token</a></li>
            <li><a href="#PKI" data-toggle="tab">PKI Token</a></li>
            <li><a href="#Cert" data-toggle="tab">Certificate</a></li>
            <li><a href="#QA" data-toggle="tab">Question & Answer</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="PasswordReset">
                <div class="row-fluid">
                    <form class="form-horizontal" id="userPassswordform" name="userPassswordform">

                        <div id="legend">
                            <legend class="">Password Reset</legend>
                        </div>

<!--                        <div class="control-group">
                            <label class="control-label"  for="username">Old Password</label>
                            <div class="controls">
                                <input type="password" id="_oldPassword"  name="_oldPassword" placeholder="Old Password" class="span3">
                                : <input type="text" id="_port" name="_port" placeholder="443" class="span1">
                            </div>
                        </div>-->

                         <div class="control-group">
                            <label class="control-label"  for="username">New Password</label>
                            <div class="controls">
                                <input type="password" id="_newPassword" name="_newPassword" placeholder="New Password" class="span3">                                
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label"  for="username">Confirm Password</label>
                            <div class="controls">
                                <input type="password" id="_confirmPassword" name="_confirmPassword" placeholder="Confirm Password" class="span3">
                                <!--: <input type="password" id="_password" name="_password" placeholder="password for  authentication" class="input-xlarge">-->
                            </div>
                        </div>


                       
                        <hr>


                        <!-- Submit -->
                        <div class="control-group">
                            <div class="controls">
                                <div id="user-password-primary-result"></div>
                                <button class="btn btn-primary" id="changePassword" onclick="changeuserpassword()" type="button">Reset Password </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <%
            
               rssUserObj.getTokenDetails();
                   AxiomCredentialDetails axiomCredentalOOBDetails = null;
                    AxiomCredentialDetails axiomCredentalSWDetails = null;
                    AxiomCredentialDetails axiomCredentalHWDetails = null;
                     AxiomCredentialDetails axiomCredentalCERTBDetails = null;
                      AxiomCredentialDetails axiomCredentalCRDetails = null;
                   if (rssUserObj.getTokenDetails() != null) {
                       for (int i = 0; i < rssUserObj.getTokenDetails().size(); i++) {
                          AxiomCredentialDetails axiomCredentialDetails = rssUserObj.getTokenDetails().get(i);
                            if (axiomCredentialDetails != null) {
                              if (axiomCredentialDetails.getCategory() == AxiomWrapper.OUTOFBOUND_TOKEN) {
                                  axiomCredentalOOBDetails = axiomCredentialDetails;
                               }
                              if (axiomCredentialDetails.getCategory() == AxiomWrapper.SOFTWARE_TOKEN) {
                                  axiomCredentalSWDetails = axiomCredentialDetails;
                               }
                              if (axiomCredentialDetails.getCategory() == AxiomWrapper.HARDWARE_TOKEN) {
                                  axiomCredentalHWDetails = axiomCredentialDetails;
                               }
                              if (axiomCredentialDetails.getCategory() == AxiomWrapper.CERTIFICATE) {
                                  axiomCredentalCERTBDetails = axiomCredentialDetails;
                               }
                              if (axiomCredentialDetails.getCategory() == AxiomWrapper.CHALLAEGE_RESPONSE) {
                                  axiomCredentalCRDetails = axiomCredentialDetails;
                               }
                            }
                       }
                   }
                String strLabelOOB = "label";    //unassigned
                String strLabelSW = "label";    //unassigned
                String strLabelHW = "label"; 
                 String softstatus = "Unassigned";
                String hardstatus = "Unassigned";
                String OOBstatus = "Unassigned";
                String OOBtype = "---";
                String SOBtype = "---";
                String serialNumbersoft = "------";
                String serialNumberhard = "------";
                String serialNumberoob = "";
   
                   if(axiomCredentalOOBDetails != null){
                        if (axiomCredentalOOBDetails.getStatus() == AxiomWrapper.TOKEN_STATUS_ACTIVE) {
                            strLabelOOB += " label-success";   //active
                            OOBstatus = "Active";
                        }
                        if (axiomCredentalOOBDetails.getStatus() == AxiomWrapper.TOKEN_STATUS_LOCKEd) {
                            strLabelOOB += " label-warning";   //locked
                            OOBstatus = "Locked";
                        }
                        if (axiomCredentalOOBDetails.getStatus() == AxiomWrapper.TOKEN_STATUS_ASSIGNED) {
                            strLabelOOB += " label-info";   //assigned                
                            OOBstatus = "Assigned";
                        }
                        if (axiomCredentalOOBDetails.getStatus() == AxiomWrapper.TOKEN_STATUS_UNASSIGNED) {
                            OOBstatus = "Unassigned";
                        }
                        if (axiomCredentalOOBDetails.getStatus() == AxiomWrapper.TOKEN_STATUS_SUSPENDED) {
                            strLabelOOB += " label-important";   //suspended
                            OOBstatus = "suspended";
                        }
                        if (axiomCredentalOOBDetails.getSubcategory() == AxiomWrapper.OOB__SMS_TOKEN) {
                            OOBtype = "SMS";
                        }
                        if (axiomCredentalOOBDetails.getSubcategory() == AxiomWrapper.OOB__VOICE_TOKEN) {
                            OOBtype = "VOICE";
                        }
                        if (axiomCredentalOOBDetails.getSubcategory() == AxiomWrapper.OOB__USSD_TOKEN) {
                            OOBtype = "USSD";
                        }
                        if (axiomCredentalOOBDetails.getSubcategory() == AxiomWrapper.OOB__EMAIL_TOKEN) {
                            OOBtype = "EMAIL";
                        }
                   }
                   
                   //sw token
                    if (axiomCredentalSWDetails != null) {
                        if (axiomCredentalSWDetails.getStatus() == AxiomWrapper.TOKEN_STATUS_ACTIVE) {
                            strLabelSW += " label-success";   //active
                            softstatus = "Active";
                        } else if (axiomCredentalSWDetails.getStatus()  == AxiomWrapper.TOKEN_STATUS_LOCKEd) {
                            strLabelSW += " label-warning";   //locked
                            softstatus = "Locked";
                        } else if (axiomCredentalSWDetails.getStatus()  == AxiomWrapper.TOKEN_STATUS_ASSIGNED) {
                            strLabelSW += " label-info";   //assigned                
                            softstatus = "Assigned";
                        } else if (axiomCredentalSWDetails.getStatus()  == AxiomWrapper.TOKEN_STATUS_UNASSIGNED) {
                            softstatus = "Unassigned";
                        } else if (axiomCredentalSWDetails.getStatus()  == AxiomWrapper.TOKEN_STATUS_SUSPENDED) {
                            strLabelSW += " label-important";   //suspended
                            softstatus = "suspended";
                        }
                        if (axiomCredentalSWDetails.getSubcategory() == AxiomWrapper.SW_WEB_TOKEN) {
                            SOBtype = "WEB(" + serialNumbersoft + ")";
                        }
                        if (axiomCredentalSWDetails.getSubcategory() == AxiomWrapper.SW_MOBILE_TOKEN) {
                            SOBtype = "MOBILE(" + serialNumbersoft + ")";
                        }
                        if (axiomCredentalSWDetails.getSubcategory() == AxiomWrapper.SW_PC_TOKEN) {
                            SOBtype = "PC(" + serialNumbersoft + ")";
                        }
                    }

            %>
            
            <div class="tab-pane" id="OTP">
                <div class="row-fluid">
                    <form class="form-horizontal" id="smsprimaryform" name="smsprimaryform">

                        <div id="legend">
                            <legend class="">Out Of Band Token</legend>
                        </div>

                        <div class="control-group">
                            <label class="control-label"  for="username">Manage </label>
                            <div class="controls">
                                <div class="btn-group">
                                    <span id="<%=OOBstatus%>" class="<%=strLabelOOB%>"><%=OOBstatus%></span>
                                    <button class="btn" id="<%=1%>" ><%=OOBtype%></button>                        
                                    <button class="btn  dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li class="dropdown-submenu">
                                            <% if (axiomCredentalOOBDetails != null) {%>
                                        <li><a href="#" onclick="changeotptokensstatus(1,  3, 1)">Mark as Active</a></li>
                                        <li><a href="#" onclick="changeotptokensstatus(-2, 3, 1)">Mark as Suspended</a></li>
                                        <li class="divider"></li>
                                                <%} else {%> 
                                        <li><a href="#" onclick="changeotptokensstatus(1,  3, 1)">Mark as Active</a></li>
                                        <li><a href="#" onclick="changeotptokensstatus(-2,  3, 1)">Mark as Suspended</a></li>
                                     </ul>
                                </div>

                                <div class="btn-group">
                                    <button class="btn"><div id="_change-Search-Type">Change Token Type</div></button>
                                    <button class="btn dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <% if (axiomCredentalOOBDetails != null) { %>
                                        <li><a tabindex="-1" href="#" onclick="changeOOBandSW( 3, 1)">SMS Token</a></li>
                                        <li><a tabindex="-1" href="#" onclick="changeOOBandSW( 3, 2)">Voice Token</a></li>
                                        <li><a tabindex="-1" href="#" onclick="changeOOBandSW( 3, 3)">USSD Token</a></li>
                                        <li><a tabindex="-1" href="#" onclick="changeOOBandSW( 3, 4)">Email Token</a></li>
                                            <%} else {%> 
                                        <li><a tabindex="-1" href="#" onclick="changeOOBandSW( 3, 1)">SMS Token</a></li>
                                        <li><a tabindex="-1" href="#" onclick="changeOOBandSW( 3, 2)">Voice Token</a></li>
                                        <li><a tabindex="-1" href="#" onclick="changeOOBandSW(3, 3)">USSD Token</a></li>
                                        <li><a tabindex="-1" href="#" onclick="changeOOBandSW(3, 4)">Email Token</a></li>

                                        <%}%>  
                                    </ul>
                                </div>     


                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label"  for="username">Serial No </label>
                            <div class="controls">
                                <% if (axiomCredentalOOBDetails != null) {%>
                                <p><%=axiomCredentalOOBDetails.getSerialnumber() %>#  Status As #<%= softstatus%> # with attempt #<%=axiomCredentalOOBDetails.getAttempts()%>
                                    <% } else {%>
                                <p>Not-Available #Status As #Not-Available #with attempt #Not-Available</p>

                                <% }%>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Created On</label>
                            <div class="controls">
                                <% if (axiomCredentalOOBDetails != null) {%>
                                <p><%=axiomCredentalOOBDetails.getLastaccessOn() %> Last Access On <%=axiomCredentalOOBDetails.getLastaccessOn() %></p>
                                <% } else {%>
                                <p><%="Thu Jul 31 21:45:36 "%> Last Access On <%="Thu Jul 31 21:45:36 "%></p>
                                <% }%>

                            </div>
                        </div>    

                        <div id="legend">
                            <legend class="">Software Token</legend>
                        </div>

                        <div class="control-group">
                            <label class="control-label"  for="username">Manage </label>
                            <div class="controls">
                                <div class="btn-group">
                                    <button class="btn" id="<%=1%>"><%="SOFTWARE"%></button>                        
                                    <button class="btn dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <% if (axiomCredentalSWDetails != null){%>
                                        <li><a href="#" onclick="sendreg('<%=1%>', 1, 1)">(Re)send Registration Code</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onclick="changeotptokensstatus(1, '<%=1%>', 1, 1)">Mark as Active</a></li>
                                        <li><a href="#" onclick="changeotptokensstatus(-2, '<%=1%>', 1, 1)">Mark as Suspended</a></li>
<!--                                        <li class="divider"></li>
                                        <li><a href="#" onclick="changeotptokensstatus(-10, '<%=1%>', 1, 1)"><font color="red">Un-Assign Token</font></a></li>-->

                                        <%} else {%>
                                        <li><a href="#" onclick="sendreg('<%=1%>', 1, 1)">(Re)send Registration Code</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onclick="changeotptokensstatus(1, '<%=1%>', 1, 1)">Mark as Active</a></li>
                                        <li><a href="#" onclick="changeotptokensstatus(-2, '<%=1%>', 1, 1)">Mark as Suspended</a></li>
<!--                                        <li class="divider"></li>
                                        <li><a href="#" onclick="changeotptokensstatus(-10, '<%=1%>', 1, 1)"><font color="red">Un-Assign Token</font></a></li>-->

                                        <%}%>  

                                    </ul>


                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Serial No </label>
                            <div class="controls">
                                <% if (1 == 1) {
                                        String softstatus;
                                        if (1 == 1) {
                                            softstatus = "ACTIVE";
                                        }

                                %>
                                <p><%=1%>#  Status As #<%= softstatus%> # with attempt #<%= 1%>
                                    <% } else {%>
                                <p>Not-Available #Status As #Not-Available #with attempt #Not-Available</p>

                                <% }%>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Created On</label>
                            <div class="controls">
                                <% if (1 == 1) {%>
                                <p><%=new Date()%> Last Access On <%=new Date()%></p>
                                <% } else {%>
                                <p><%="Thu Jul 31 21:45:36 "%> Last Access On <%="Thu Jul 31 21:45:36 "%></p>
                                <% }%>

                            </div>
                        </div>  

                        <div id="legend">
                            <legend class="">Hardware Token</legend>
                        </div>

                        <%


                        %>
                        <div class="control-group">
                            <label class="control-label"  for="username">Manage</label>
                            <div class="controls">

                                <div class="btn-group">
                                    <button class="btn" id="<%=1%>" ><%="HARDWARE"%></button>                        
                                    <!--<button class="btn"><div id="_change-hard-Status"></div></button>-->
                                    <button class="btn dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <% if (1 == 1) {%>

<!--                                        <li><a href="#"  onclick="loadHarddetails('<%=1%>', 2, 1)">Assign Token</a></li>
                                        <li class="divider"></li>-->
                                        <li><a href="#" onclick="changeotptokensstatus(1, '<%=1%>', 2, 1)">Mark as Active</a></li>
                                        <li><a href="#" onclick="changeotptokensstatus(-2, '<%=1%>', 2, 1)">Mark as Suspended</a></li>                            
                                        <li class="divider"></li>
                                         <li><a href="#" onclick="changeotptokensstatus(-5, '<%=1%>', 2, 1)"><font color="red">Replace?</font></a></li>
                                         <li><a href="#" onclick="changeotptokensstatus(-5, '<%=1%>', 2, 1)"><font color="red">Resync?</font></a></li>
                                        <li><a href="#" onclick="changeotptokensstatus(-5, '<%=1%>', 2, 1)"><font color="red">Mark as Lost</font></a></li>
                                        <!--<li><a href="#" onclick="changeotptokensstatus(-10, '<%=1%>', 2, 1)"><font color="red">Un-Assign Token</font></a></li>-->                                                        
                                                <%} else {%> 
<!--                                        <li><a href="#"  onclick="loadHarddetails('<%=1%>', 2, 1)">Assign Token</a></li>
                                        <li class="divider"></li>-->
                                        <li><a href="#" onclick="changeotptokensstatus(1, '<%=1%>', 2, 1)">Mark as Active</a></li>
                                        <li><a href="#" onclick="changeotptokensstatus(-2, '<%=1%>', 2, 1)">Mark as Suspended</a></li>                            
                                        <li class="divider"></li>
                                         <li><a href="#" onclick="changeotptokensstatus(-5, '<%=1%>', 2, 1)">Replace?</a></li>
                                         <li><a href="#" onclick="changeotptokensstatus(-5, '<%=1%>', 2, 1)">Resync?</a></li>
                                        <li><a href="#" onclick="changeotptokensstatus(-5, '<%=1%>', 2, 1)">Mark as Lost</a></li>
                                        <!--<li><a href="#" onclick="changeotptokensstatus(-10, '<%=1%>', 2, 1)"><font color="red">Un-Assign Token</font></a></li>-->                                                        

                                        <%}%> 
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Serial No </label>
                            <div class="controls">
                                <% if (1 == 1) {
                                        String hardstatus;
                                        if (1 == 1) {
                                            hardstatus = "ACTIVE";
                                        }


                                %>


                                <p><%=12545555%>#  Status As #<%=hardstatus%> # with attempt #<%=1%>
                                    <% } else {%>
                                <p>Not-Available #Status As #Not-Available #with attempt #Not-Available</p>

                                <% }%>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Created On</label>
                            <div class="controls">
                                <% if (1 == 1) {

                                %>
                                <p><%=new Date()%> Last Access On <%=new Date()%></p>
                                <% } else {%>
                                <p><%="Thu Jul 31 21:45:36 "%> Last Access On <%="Thu Jul 31 21:45:36 "%></p>
                                <% }%>

                            </div>
                        </div>       

                        <!--<hr>-->
                        <div id="legend">
                            <legend class="">Audit Management</legend>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Audit </label>

                            <input type="hidden" id="_oobreportType" name="_oobreportType">
                            <div class="controls">  
                                <div class="input-prepend">
                                    <span class="add-on">Starts From:</span>   
                                    <div id="datetimepickerauditOOB1" class="input-append date">
                                        <input id="startoobdate" name="startoobdate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>

                                    </div>
                                    <!--</div>-->
                                </div>
                                <div class="input-prepend" >
                                    <!--<div class="well">-->

                                    <span class="add-on">till:</span>   
                                    <div id="datetimepickerauditOOB2" class="input-append date">
                                        <input id="endoobdate" name="endoobdate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                    Report Format   
                                    <div class="btn-group">
                                        <button class="btn"><div id="_oobreportType-div">Select Type</div></button>
                                        <button class="btn dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeReportOOBForBridge(1)">CSV</a></li>
                                            <li><a href="#" onclick="ChangeReportOOBForBridge(0)">PDF</a></li>
                                        </ul>
                                    </div>

                                    <a href="#" class="btn btn-success" onclick="CardUserOOBAudits('<%=1%>', '<%=1%>', '<%=1%>')">Download Report</a>
                                </div>
                            </div>

                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username"> </label>
                            <div class="controls">  
                                <div class="input-prepend">
                                    <select  span="1"  name="_hid2" id="_hid2" >
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username"> </label>
                            <div class="controls">  
                                <div class="input-prepend">
                                    <select  span="1"  name="_hid3" id="_hid3" >
                                    </select>
                                </div>
                            </div>
                        </div>
                        <script>
                            document.getElementById("_hid2").style.display = 'none';
                            document.getElementById("_hid3").style.display = 'none';
                        </script>


                    </form>
                </div>
            </div>
            <div class="tab-pane" id="PKI">
                <div class="row-fluid">
                    <form class="form-horizontal" id="smsprimaryform" name="smsprimaryform">

                        <div id="legend">
                            <legend class="">Hardware PKI Token</legend>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Manage</label>
                            <div class="controls">

                                <div class="btn-group">
                                    <span class="<%=1%>" id="<%=1%>">Assign Token</span>
                                    <button class="btn" id="<%=1%>">Assign Token</button>
                                    <button class="btn  dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <!--<li><a href="#"   onclick="loadUsersID('<%=1%>')">Assign Token</a></li>-->
                                        <!--<li><a href="#" onclick="assignewtoken('<%=1%>','HARDWARE_TOKEN')">Assign New Token</a></li>-->
                                        <li><a href="#" onclick="changepkitokenstatus(1, '<%=1%>', 2, '<%=1%>')">Activate Token</a></li>
                                        <li><a href="#" onclick="changepkitokenstatus(1, '<%=1%>', 2, '<%=1%>')">Suspend Token</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onclick="sendpfxfile('<%=1%>')">(Re)send PFX File (via email)</a></li>
                                        <li><a href="#" onclick="sendpfxpassword('<%=1%>')">(Re)send PFX Password (via mobile)</a></li>
                                        <li class="divider"></li>
                                         <li><a href="#" onclick="changeotptokensstatus(-5, '<%=1%>', 2, 1)">Replace</a></li>
                                         <li><a href="#" onclick="changeotptokensstatus(-5, '<%=1%>', 2, 1)">Resync?</a></li>
                                        <!--<li><a href="#" onclick="changepkitokenstatus(-10, '<%=1%>', 2, '<%=1%>')" ><font color="red">Un-Assign Token</font></a></li>-->
                                        <li><a href="#" onclick="changepkitokenstatus(-5, '<%=1%>', 2, '<%=1%>')" ><font color="red">Mark as Lost</font></a></li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Serial No </label>
                            <div class="controls">
                                <% if (1 == 1) {
                                        String hardstatus;
                                        if (1 == 1) {
                                            hardstatus = "ACTIVE";
                                        }


                                %>


                                <p><%=12545555%>#  Status As #<%=hardstatus%> # with attempt #<%=1%>
                                    <% } else {%>
                                <p>Not-Available #Status As #Not-Available #with attempt #Not-Available</p>

                                <% }%>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Created On</label>
                            <div class="controls">
                                <% if (1 == 1) {

                                %>
                                <p><%=new Date()%> Last Access On <%=new Date()%></p>
                                <% } else {%>
                                <p><%="Thu Jul 31 21:45:36 "%> Last Access On <%="Thu Jul 31 21:45:36 "%></p>
                                <% }%>

                            </div>
                        </div> 
                        <div id="legend">
                            <legend class="">Software PKI Token</legend>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Manage</label>
                            <div class="controls">

                                <div class="btn-group">
                                    <span class="<%=1%>" id="<%=1%>">Assign Token</span>
                                    <button class="btn" id="<%=1%>">Assign Token</button>
                                    <button class="btn  dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <!--<li><a href="#"   onclick="loadUsersID('<%=1%>')">Assign Token</a></li>-->
                                        <!--<li><a href="#" onclick="assignewtoken('<%=1%>','HARDWARE_TOKEN')">Assign New Token</a></li>-->
                                        <li><a href="#" onclick="changepkitokenstatus(1, '<%=1%>', 2, '<%=1%>')">Activate</a></li>
                                        <li><a href="#" onclick="changepkitokenstatus(1, '<%=1%>', 2, '<%=1%>')">Suspended</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" onclick="sendpfxfile('<%=1%>')">(Re)send PFX File (via email)</a></li>
                                        <li><a href="#" onclick="sendpfxpassword('<%=1%>')">(Re)send PFX Password (via mobile)</a></li>
                                        <li class="divider"></li>
                                        <!--<li><a href="#" onclick="changepkitokenstatus(-10, '<%=1%>', 2, '<%=1%>')" ><font color="red">Un-Assign Token</font></a></li>-->
                                        <!--<li><a href="#" onclick="changepkitokenstatus(-5, '<%=1%>', 2, '<%=1%>')" ><font color="red">Mark as Lost</font></a></li>-->
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Serial No </label>
                            <div class="controls">
                                <% if (1 == 1) {
                                        String hardstatus;
                                        if (1 == 1) {
                                            hardstatus = "ACTIVE";
                                        }


                                %>


                                <p><%=12545555%>#  Status As #<%=hardstatus%> # with attempt #<%=1%>
                                    <% } else {%>
                                <p>Not-Available #Status As #Not-Available #with attempt #Not-Available</p>

                                <% }%>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Created On</label>
                            <div class="controls">
                                <% if (1 == 1) {

                                %>
                                <p><%=new Date()%> Last Access On <%=new Date()%></p>
                                <% } else {%>
                                <p><%="Thu Jul 31 21:45:36 "%> Last Access On <%="Thu Jul 31 21:45:36 "%></p>
                                <% }%>

                            </div>
                        </div> 
                        <div id="legend">
                            <legend class="">Audit Management</legend>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Audit </label>

                            <input type="hidden" id="_oobreportType" name="_oobreportType">
                            <div class="controls">  
                                <div class="input-prepend">
                                    <span class="add-on">Starts From:</span>   
                                    <div id="datetimepickerauditOOB1" class="input-append date">
                                        <input id="startoobdate" name="startoobdate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>

                                    </div>
                                    <!--</div>-->
                                </div>
                                <div class="input-prepend" >
                                    <!--<div class="well">-->

                                    <span class="add-on">till:</span>   
                                    <div id="datetimepickerauditOOB2" class="input-append date">
                                        <input id="endoobdate" name="endoobdate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                    Report Format   
                                    <div class="btn-group">
                                        <button class="btn"><div id="_oobreportType-div">Select Type</div></button>
                                        <button class="btn dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeReportOOBForBridge(1)">CSV</a></li>
                                            <li><a href="#" onclick="ChangeReportOOBForBridge(0)">PDF</a></li>
                                        </ul>
                                    </div>

                                    <a href="#" class="btn btn-success" onclick="CardUserOOBAudits('<%=1%>', '<%=1%>', '<%=1%>')">Download Report</a>
                                </div>
                            </div>

                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username"> </label>
                            <div class="controls">  
                                <div class="input-prepend">
                                    <select  span="1"  name="_hid21" id="_hid21" >
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username"> </label>
                            <div class="controls">  
                                <div class="input-prepend">
                                    <select  span="1"  name="_hid31" id="_hid31" >
                                    </select>
                                </div>
                            </div>
                        </div>
                        <script>
                            document.getElementById("_hid21").style.display = 'none';
                            document.getElementById("_hid31").style.display = 'none';
                        </script>

                    </form>
                </div>


            </div>
            <div class="tab-pane" id="Cert">
                <div class="row-fluid">
                    <form class="form-horizontal" id="smsprimaryform" name="smsprimaryform">
                        <div id="legend">
                            <legend class="">Certifacate</legend>
                        </div>
                         <!--<td><a href="#" onclick="generatecertificate('<%=1%>')" class="btn btn-mini btn-primary">Issue Now</a></td>-->
                        <!--                       <div class="control-group">
                                                    <label class="control-label"  for="username">Manage</label>
                                                    <div class="controls">
                                                <div class="btn-group">
                                                    <a href="#" onclick="generatecertificate('<%=1%>')" class="btn ">Issue Now</a>
                                                </div>
                                                    </div>
                                                  </div> -->
                        <div class="control-group">
                            <label class="control-label"  for="username"> Issue Now</label>
                            <div class="controls">

<!--                                <div class="btn-group">
                                    <a href="#" onclick="generatecertificate('<%=1%>')" class="btn ">Issue Now</a>
                                </div>-->
                                Manage Certificate
                                <div class="btn-group">
                                    <button class="btn ">Manage</button>
                                    <button class="btn dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="loadsendcertfile('<%=1%>')">Share Certificate(via Email)</a></li>
                                        <li class="divider"></li>                    
                                        <li><a href="#" onclick="renewcertificate('<%=1%>')">Renew Certificate</a></li>                    
                                        <li><a href="#" onclick="loadrevoke('<%=1%>')"><font color="red">Revoke Certificate</font></a></li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                                    
                             <div class="control-group">
                            <label class="control-label"  for="username">Created On</label>
                            <div class="controls">
                                <% if (1 == 1) {

                                %>
                                <p><%=new Date()%> Last Access On <%=new Date()%></p>
                                <% } else {%>
                                <p><%="Thu Jul 31 21:45:36 "%> Last Access On <%="Thu Jul 31 21:45:36 "%></p>
                                <% }%>

                            </div>
                        </div> 

                        <div id="legend">
                            <legend class="">Audit Management</legend>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username">Audit </label>

                            <input type="hidden" id="_oobreportType" name="_oobreportType">
                            <div class="controls">  
                                <div class="input-prepend">
                                    <span class="add-on">Starts From:</span>   
                                    <div id="datetimepickerauditOOB1" class="input-append date">
                                        <input id="startoobdate" name="startoobdate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>

                                    </div>
                                    <!--</div>-->
                                </div>
                                <div class="input-prepend" >
                                    <!--<div class="well">-->

                                    <span class="add-on">till:</span>   
                                    <div id="datetimepickerauditOOB2" class="input-append date">
                                        <input id="endoobdate" name="endoobdate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                    Report Format   
                                    <div class="btn-group">
                                        <button class="btn"><div id="_oobreportType-div">Select Type</div></button>
                                        <button class="btn dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeReportOOBForBridge(1)">CSV</a></li>
                                            <li><a href="#" onclick="ChangeReportOOBForBridge(0)">PDF</a></li>
                                        </ul>
                                    </div>

                                    <a href="#" class="btn btn-success" onclick="CardUserOOBAudits('<%=1%>', '<%=1%>', '<%=1%>')">Download Report</a>
                                </div>
                            </div>

                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username"> </label>
                            <div class="controls">  
                                <div class="input-prepend">
                                    <select  span="1"  name="_hid22" id="_hid22" >
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username"> </label>
                            <div class="controls">  
                                <div class="input-prepend">
                                    <select  span="1"  name="_hid32" id="_hid32" >
                                    </select>
                                </div>
                            </div>
                        </div>
                        <script>
                            document.getElementById("_hid22").style.display = 'none';
                            document.getElementById("_hid32").style.display = 'none';
                        </script>
                    </form>
                </div>

            </div>

            <div class="tab-pane" id="QA">
                <div class="row-fluid">
                    <form class="form-horizontal" id="smsprimaryform" name="smsprimaryform">
                        <div id="legend">
                            <legend class="">Questions And Answer</legend>
                        </div>

                        <div class="control-group">
                            <label class="control-label"  for="username">Manage </label>
                            <div class="controls"> 
                               
                                
                                <div class="btn-group">
                                    <button class="btn" >Manage</button>
                                    <button class="btn dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#"  onclick="changeuserChallengestatus(1, '<%=1%>', '<%=1%>')" >Unlock?</a></li>
                                        <li><a href="./UserQuestionList.jsp?_userID=<%=1%>" data-toggle="modal">List Questions?</a></li>

                                    </ul>
                                </div>
                            </div>
<!--                        </div>            <div class="control-group">
                            <label class="control-label"  for="username">Created On</label>
                            <div class="controls">
                                <% if (1 == 1) {

                                %>
                                <p><%=new Date()%> Last Access On <%=new Date()%></p>
                                <% } else {%>
                                <p><%="Thu Jul 31 21:45:36 "%> Last Access On <%="Thu Jul 31 21:45:36 "%></p>
                                <% }%>

                            </div>-->
                        </div> 
                          <div class="control-group">
                            <label class="control-label"  for="username">Attempts</label>
                            <div class="controls">
                               <p> 5 attempts</p>
                            </div>
                        </div> 
                                         <div class="control-group">
                            <label class="control-label"  for="username">Created On</label>
                            <div class="controls">
                                <% if (1 == 1) {

                                %>
                                <p><%=new Date()%> Last Access On <%=new Date()%></p>
                                <% } else {%>
                                <p><%="Thu Jul 31 21:45:36 "%> Last Access On <%="Thu Jul 31 21:45:36 "%></p>
                                <% }%>

                            </div>
                        </div> 

                        <div class="control-group">
                            <label class="control-label"  for="username">Audit </label>

                            <input type="hidden" id="_oobreportType" name="_oobreportType">
                            <div class="controls">  
                                <div class="input-prepend">
                                    <span class="add-on">Starts From:</span>   
                                    <div id="datetimepickerauditOOB1" class="input-append date">
                                        <input id="startoobdate" name="startoobdate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth"></input>

                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>

                                    </div>
                                    <!--</div>-->
                                </div>
                                <div class="input-prepend" >
                                    <!--<div class="well">-->

                                    <span class="add-on">till:</span>   
                                    <div id="datetimepickerauditOOB2" class="input-append date">
                                        <input id="endoobdate" name="endoobdate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth"></input>
                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                    Report Format   
                                    <div class="btn-group">
                                        <button class="btn"><div id="_oobreportType-div">Select Type</div></button>
                                        <button class="btn dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="ChangeReportOOBForBridge(1)">CSV</a></li>
                                            <li><a href="#" onclick="ChangeReportOOBForBridge(0)">PDF</a></li>
                                        </ul>
                                    </div>

                                    <a href="#" class="btn btn-success" onclick="CardUserOOBAudits('<%=1%>', '<%=1%>', '<%=1%>')">Download Report</a>
                                </div>
                            </div>

                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username"> </label>
                            <div class="controls">  
                                <div class="input-prepend">
                                    <select  span="1"  name="_hid23" id="_hid23" >
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"  for="username"> </label>
                            <div class="controls">  
                                <div class="input-prepend">
                                    <select  span="1"  name="_hid33" id="_hid33" >
                                    </select>
                                </div>
                            </div>
                        </div>
                        <script>
                            document.getElementById("_hid23").style.display = 'none';
                            document.getElementById("_hid33").style.display = 'none';
                        </script>
                    </form>
                </div>

            </div>
        </div>
    </div>



</div>

<script>
    //$(document).ready(function() { $("#_keywords").select2() }); 

    $(document).ready(function() {
        $("#_keywords").select2({
            tags: [],
            dropdownCss: {display: 'none'},
            tokenSeparators: [","]
        });
    });
</script>
<script>
    //$(document).ready(function() { $("#_keywords").select2() }); 

    $(document).ready(function() {
        $("#_ip").select2({
            tags: [],
            dropdownCss: {display: 'none'},
            tokenSeparators: [","]
        });
    });
</script>

<script>
    //$(document).ready(function() { $("#_keywords").select2() }); 

    $(document).ready(function() {
        $("#_prefix").select2({
            tags: [],
            dropdownCss: {display: 'none'},
            tokenSeparators: [","]
        });
    });
</script>

<script>
    //$(document).ready(function() { $("#_keywords").select2() }); 

    $(document).ready(function() {
        $("#_domain").select2({
            tags: [],
            dropdownCss: {display: 'none'},
            tokenSeparators: [","]
        });
    });
</script>




<%@include file="footer.jsp" %>