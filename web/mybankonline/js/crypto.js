//crypto.js
/*
 * Crypto-JS v2.0.X
 * http://code.google.com/p/crypto-js/
 * Copyright (c) 2009, Jeff Mott. All rights reserved.
 * http://code.google.com/p/crypto-js/wiki/License
 */
if (typeof Crypto == "undefined" || !Crypto.util) {
    (function() {
        var c = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        var d = window.Crypto = {};

        var a = d.util = {
            rotl: function(h, g) {
                return(h << g) | (h >>> (32 - g))
            },
            rotr: function(h, g) {
                return(h << (32 - g)) | (h >>> g)
            },
            endian: function(h) {
                if (h.constructor == Number) {
                    return a.rotl(h, 8) & 16711935 | a.rotl(h, 24) & 4278255360
                }
                for (var g = 0; g < h.length; g++) {
                    h[g] = a.endian(h[g])
                }
                return h
            },
            randomBytes: function(h) {
                for (var g = []; h > 0; h--) {
                    g.push(Math.floor(Math.random() * 256))
                }
                return g
            },
            bytesToWords: function(h) {
                for (var k = [], j = 0, g = 0; j < h.length; j++, g += 8) {
                    k[g >>> 5] |= h[j] << (24 - g % 32)
                }
                return k
            },
            wordsToBytes: function(i) {
                for (var h = [], g = 0; g < i.length * 32; g += 8) {
                    h.push((i[g >>> 5] >>> (24 - g % 32)) & 255)
                }
                return h
            },
            bytesToHex: function(g) {
                for (var j = [], h = 0; h < g.length; h++) {
                    j.push((g[h] >>> 4).toString(16));
                    j.push((g[h] & 15).toString(16))
                }
                return j.join("")
            },
            hexToBytes: function(h) {
                for (var g = [], i = 0; i < h.length; i += 2) {
                    g.push(parseInt(h.substr(i, 2), 16))
                }
                return g
            },
            bytesToBase64: function(h) {
                if (typeof btoa == "function") {
                    return btoa(e.bytesToString(h))
                }
                for (var g = [], l = 0; l < h.length; l += 3) {
                    var m = (h[l] << 16) | (h[l + 1] << 8) | h[l + 2];
                    for (var k = 0; k < 4; k++) {
                        if (l * 8 + k * 6 <= h.length * 8) {
                            g.push(c.charAt((m >>> 6 * (3 - k)) & 63))
                        } else {
                            g.push("=")
                        }
                    }
                }
                return g.join("")
            },
            base64ToBytes: function(h) {
                if (typeof atob == "function") {
                    return e.stringToBytes(atob(h))
                }
                h = h.replace(/[^A-Z0-9+\/]/ig, "");
                for (var g = [], j = 0, k = 0; j < h.length; k = ++j % 4) {
                    if (k == 0) {
                        continue
                    }
                    g.push(((c.indexOf(h.charAt(j - 1)) & (Math.pow(2, -2 * k + 8) - 1)) << (k * 2)) | (c.indexOf(h.charAt(j)) >>> (6 - k * 2)))
                }
                return g
            }
        };

        d.mode = {};

        var b = d.charenc = {};

        var f = b.UTF8 = {
            stringToBytes: function(g) {
                return e.stringToBytes(unescape(encodeURIComponent(g)))
            },
            bytesToString: function(g) {
                return decodeURIComponent(escape(e.bytesToString(g)))
            }
        };

        var e = b.Binary = {
            stringToBytes: function(j) {
                for (var g = [], h = 0; h < j.length; h++) {
                    g.push(j.charCodeAt(h))
                }
                return g
            },
            bytesToString: function(g) {
                for (var j = [], h = 0; h < g.length; h++) {
                    j.push(String.fromCharCode(g[h]))
                }
                return j.join("")
            }
        }
    })()
}
(function() {
    var f = Crypto, a = f.util, b = f.charenc, e = b.UTF8, d = b.Binary;
    var c = f.SHA1 = function(i, g) {
        var h = a.wordsToBytes(c._sha1(i));
        return g && g.asBytes ? h : g && g.asString ? d.bytesToString(h) : a.bytesToHex(h)
    };

    c._sha1 = function(o) {
        if (o.constructor == String) {
            o = e.stringToBytes(o)
        }
        var v = a.bytesToWords(o), x = o.length * 8, p = [], r = 1732584193, q = -271733879, k = -1732584194, h = 271733878, g = -1009589776;
        v[x >> 5] |= 128 << (24 - x % 32);
        v[((x + 64 >>> 9) << 4) + 15] = x;
        for (var z = 0; z < v.length; z += 16) {
            var E = r, D = q, C = k, B = h, A = g;
            for (var y = 0; y < 80; y++) {
                if (y < 16) {
                    p[y] = v[z + y]
                } else {
                    var u = p[y - 3] ^ p[y - 8] ^ p[y - 14] ^ p[y - 16];
                    p[y] = (u << 1) | (u >>> 31)
                }
                var s = ((r << 5) | (r >>> 27)) + g + (p[y] >>> 0) + (y < 20 ? (q & k | ~q & h) + 1518500249 : y < 40 ? (q ^ k ^ h) + 1859775393 : y < 60 ? (q & k | q & h | k & h) - 1894007588 : (q ^ k ^ h) - 899497514);
                g = h;
                h = k;
                k = (q << 30) | (q >>> 2);
                q = r;
                r = s
            }
            r += E;
            q += D;
            k += C;
            h += B;
            g += A
        }
        return[r, q, k, h, g]
    };

    c._blocksize = 16
})();
(function() {
    var e = Crypto, a = e.util, b = e.charenc, d = b.UTF8, c = b.Binary;
    e.HMAC = function(l, m, k, h) {
        if (m.constructor == String) {
            m = d.stringToBytes(m)
        }
        if (k.constructor == String) {
            k = d.stringToBytes(k)
        }
        if (k.length > l._blocksize * 4) {
            k = l(k, {
                asBytes: true
            })
        }
        var g = k.slice(0), n = k.slice(0);
        for (var j = 0; j < l._blocksize * 4; j++) {
            g[j] ^= 92;
            n[j] ^= 54
        }
        var f = l(g.concat(l(n.concat(m), {
            asBytes: true
        })), {
            asBytes: true
        });
        return h && h.asBytes ? f : h && h.asString ? c.bytesToString(f) : a.bytesToHex(f)
    }
})();
(function() {
    var e = Crypto, a = e.util, b = e.charenc, d = b.UTF8, c = b.Binary;
    e.PBKDF2 = function(q, o, f, t) {
        if (q.constructor == String) {
            q = d.stringToBytes(q)
        }
        if (o.constructor == String) {
            o = d.stringToBytes(o)
        }
        var s = t && t.hasher || e.SHA1, k = t && t.iterations || 1;
        function p(i, j) {
            return e.HMAC(s, j, i, {
                asBytes: true
            })
        }
        var h = [], g = 1;
        while (h.length < f) {
            var l = p(q, o.concat(a.wordsToBytes([g])));
            for (var r = l, n = 1; n < k; n++) {
                r = p(q, r);
                for (var m = 0; m < l.length; m++) {
                    l[m] ^= r[m]
                }
            }
            h = h.concat(l);
            g++
        }
        h.length = f;
        return t && t.asBytes ? h : t && t.asString ? c.bytesToString(h) : a.bytesToHex(h)
    }
})();
(function() {
    var a = Crypto, f = a.util, j = a.charenc, d = j.UTF8, e = j.Binary;
    var i = [], g = [], h;
    var k = a.Rabbit = {
        encrypt: function(o, n) {
            var b = d.stringToBytes(o), l = f.randomBytes(8), c = n.constructor == String ? a.PBKDF2(n, l, 32, {
                asBytes: true
            }) : n;
            k._rabbit(b, c, f.bytesToWords(l));
            return f.bytesToBase64(l.concat(b))
        },
        decrypt: function(n, m) {
            var o = f.base64ToBytes(n), l = o.splice(0, 8), b = m.constructor == String ? a.PBKDF2(m, l, 32, {
                asBytes: true
            }) : m;
            k._rabbit(o, b, f.bytesToWords(l));
            return d.bytesToString(o)
        },
        _rabbit: function(l, n, p) {
            k._keysetup(n);
            if (p) {
                k._ivsetup(p)
            }
            for (var r = [], q = 0; q < l.length; q++) {
                if (q % 16 == 0) {
                    k._nextstate();
                    r[0] = i[0] ^ (i[5] >>> 16) ^ (i[3] << 16);
                    r[1] = i[2] ^ (i[7] >>> 16) ^ (i[5] << 16);
                    r[2] = i[4] ^ (i[1] >>> 16) ^ (i[7] << 16);
                    r[3] = i[6] ^ (i[3] >>> 16) ^ (i[1] << 16);
                    for (var o = 0; o < 4; o++) {
                        r[o] = ((r[o] << 8) | (r[o] >>> 24)) & 16711935 | ((r[o] << 24) | (r[o] >>> 8)) & 4278255360
                    }
                    for (var c = 120; c >= 0; c -= 8) {
                        r[c / 8] = (r[c >>> 5] >>> (24 - c % 32)) & 255
                    }
                }
                l[q] ^= r[q % 16]
            }
        },
        _keysetup: function(b) {
            i[0] = b[0];
            i[2] = b[1];
            i[4] = b[2];
            i[6] = b[3];
            i[1] = (b[3] << 16) | (b[2] >>> 16);
            i[3] = (b[0] << 16) | (b[3] >>> 16);
            i[5] = (b[1] << 16) | (b[0] >>> 16);
            i[7] = (b[2] << 16) | (b[1] >>> 16);
            g[0] = f.rotl(b[2], 16);
            g[2] = f.rotl(b[3], 16);
            g[4] = f.rotl(b[0], 16);
            g[6] = f.rotl(b[1], 16);
            g[1] = (b[0] & 4294901760) | (b[1] & 65535);
            g[3] = (b[1] & 4294901760) | (b[2] & 65535);
            g[5] = (b[2] & 4294901760) | (b[3] & 65535);
            g[7] = (b[3] & 4294901760) | (b[0] & 65535);
            h = 0;
            for (var c = 0; c < 4; c++) {
                k._nextstate()
            }
            for (var c = 0; c < 8; c++) {
                g[c] ^= i[(c + 4) & 7]
            }
        },
        _ivsetup: function(b) {
            var o = f.endian(b[0]), m = f.endian(b[1]), n = (o >>> 16) | (m & 4294901760), l = (m << 16) | (o & 65535);
            g[0] ^= o;
            g[1] ^= n;
            g[2] ^= m;
            g[3] ^= l;
            g[4] ^= o;
            g[5] ^= n;
            g[6] ^= m;
            g[7] ^= l;
            for (var c = 0; c < 4; c++) {
                k._nextstate()
            }
        },
        _nextstate: function() {
            for (var c = [], l = 0; l < 8; l++) {
                c[l] = g[l]
            }
            g[0] = (g[0] + 1295307597 + h) >>> 0;
            g[1] = (g[1] + 3545052371 + ((g[0] >>> 0) < (c[0] >>> 0) ? 1 : 0)) >>> 0;
            g[2] = (g[2] + 886263092 + ((g[1] >>> 0) < (c[1] >>> 0) ? 1 : 0)) >>> 0;
            g[3] = (g[3] + 1295307597 + ((g[2] >>> 0) < (c[2] >>> 0) ? 1 : 0)) >>> 0;
            g[4] = (g[4] + 3545052371 + ((g[3] >>> 0) < (c[3] >>> 0) ? 1 : 0)) >>> 0;
            g[5] = (g[5] + 886263092 + ((g[4] >>> 0) < (c[4] >>> 0) ? 1 : 0)) >>> 0;
            g[6] = (g[6] + 1295307597 + ((g[5] >>> 0) < (c[5] >>> 0) ? 1 : 0)) >>> 0;
            g[7] = (g[7] + 3545052371 + ((g[6] >>> 0) < (c[6] >>> 0) ? 1 : 0)) >>> 0;
            h = (g[7] >>> 0) < (c[7] >>> 0) ? 1 : 0;
            for (var m = [], l = 0; l < 8; l++) {
                var o = (i[l] + g[l]) >>> 0;
                var q = o & 65535, n = o >>> 16;
                var b = ((((q * q) >>> 17) + q * n) >>> 15) + n * n, p = (((o & 4294901760) * o) >>> 0) + (((o & 65535) * o) >>> 0) >>> 0;
                m[l] = b ^ p
            }
            i[0] = m[0] + ((m[7] << 16) | (m[7] >>> 16)) + ((m[6] << 16) | (m[6] >>> 16));
            i[1] = m[1] + ((m[0] << 8) | (m[0] >>> 24)) + m[7];
            i[2] = m[2] + ((m[1] << 16) | (m[1] >>> 16)) + ((m[0] << 16) | (m[0] >>> 16));
            i[3] = m[3] + ((m[2] << 8) | (m[2] >>> 24)) + m[1];
            i[4] = m[4] + ((m[3] << 16) | (m[3] >>> 16)) + ((m[2] << 16) | (m[2] >>> 16));
            i[5] = m[5] + ((m[4] << 8) | (m[4] >>> 24)) + m[3];
            i[6] = m[6] + ((m[5] << 16) | (m[5] >>> 16)) + ((m[4] << 16) | (m[4] >>> 16));
            i[7] = m[7] + ((m[6] << 8) | (m[6] >>> 24)) + m[5]
        }
    }
})();
//end crypto.js

//base32.js
(function() {

// This would be the place to edit if you want a different
// Base32 implementation

    var alphabet = '0123456789abcdefghjkmnpqrtuvwxyz'
    var alias = {o: 0, i: 1, l: 1, s: 5}

    /**
     * Build a lookup table and memoize it
     *
     * Return an object that maps a character to its
     * byte value.
     */

    var lookup = function() {
        var table = {}
        // Invert 'alphabet'
        for (var i = 0; i < alphabet.length; i++) {
            table[alphabet[i]] = i
        }
        // Splice in 'alias'
        for (var key in alias) {
            if (!alias.hasOwnProperty(key))
                continue
            table[key] = table['' + alias[key]]
        }
        lookup = function() {
            return table
        }
        return table
    }

    /**
     * A streaming encoder
     *
     *     var encoder = new base32.Encoder()
     *     var output1 = encoder.update(input1)
     *     var output2 = encoder.update(input2)
     *     var lastoutput = encode.update(lastinput, true)
     */

    function Encoder() {
        var skip = 0 // how many bits we will skip from the first byte
        var bits = 0 // 5 high bits, carry from one byte to the next

        this.output = ''

        // Read one byte of input
        // Should not really be used except by "update"
        this.readByte = function(byte) {
            // coerce the byte to an int
            if (typeof byte == 'string')
                byte = byte.charCodeAt(0)

            if (skip < 0) { // we have a carry from the previous byte
                bits |= (byte >> (-skip))
            } else { // no carry
                bits = (byte << skip) & 248
            }

            if (skip > 3) {
                // not enough data to produce a character, get us another one
                skip -= 8
                return 1
            }

            if (skip < 4) {
                // produce a character
                this.output += alphabet[bits >> 3]
                skip += 5
            }

            return 0
        }

        // Flush any remaining bits left in the stream
        this.finish = function(check) {
            var output = this.output + (skip < 0 ? alphabet[bits >> 3] : '') + (check ? '$' : '')
            this.output = ''
            return output
        }
    }

    /**
     * Process additional input
     *
     * input: string of bytes to convert
     * flush: boolean, should we flush any trailing bits left
     *        in the stream
     * returns: a string of characters representing 'input' in base32
     */

    Encoder.prototype.update = function(input, flush) {
        for (var i = 0; i < input.length; ) {
            i += this.readByte(input[i])
        }
        // consume all output
        var output = this.output
        this.output = ''
        if (flush) {
            output += this.finish()
        }
        return output
    }

// Functions analogously to Encoder

    function Decoder() {
        var skip = 0 // how many bits we have from the previous character
        var byte = 0 // current byte we're producing

        this.output = ''

        // Consume a character from the stream, store
        // the output in this.output. As before, better
        // to use update().
        this.readChar = function(char) {
            if (typeof char != 'string') {
                if (typeof char == 'number') {
                    char = String.fromCharCode(char)
                }
            }
            char = char.toLowerCase()
            var val = lookup()[char]
            if (typeof val == 'undefined') {
                // character does not exist in our lookup table
                return // skip silently. An alternative would be:
                // throw Error('Could not find character "' + char + '" in lookup table.')
            }
            val <<= 3 // move to the high bits
            byte |= val >>> skip
            skip += 5
            if (skip >= 8) {
                // we have enough to preduce output
                this.output += String.fromCharCode(byte)
                skip -= 8
                if (skip > 0)
                    byte = (val << (5 - skip)) & 255
                else
                    byte = 0
            }

        }

        this.finish = function(check) {
            var output = this.output + (skip < 0 ? alphabet[bits >> 3] : '') + (check ? '$' : '')
            this.output = ''
            return output
        }
    }

    Decoder.prototype.update = function(input, flush) {
        for (var i = 0; i < input.length; i++) {
            this.readChar(input[i])
        }
        var output = this.output
        this.output = ''
        if (flush) {
            output += this.finish()
        }
        return output
    }

    /** Convenience functions
     *
     * These are the ones to use if you just have a string and
     * want to convert it without dealing with streams and whatnot.
     */

// String of data goes in, Base32-encoded string comes out.
    function encode(input) {
        var encoder = new Encoder()
        var output = encoder.update(input, true)
        return output
    }

// Base32-encoded string goes in, decoded data comes out.
    function decode(input) {
        var decoder = new Decoder()
        var output = decoder.update(input, true)
        return output
    }

    /**
     * sha1 functions wrap the hash function from Node.js
     *
     * Several ways to use this:
     *
     *     var hash = base32.sha1('Hello World')
     *     base32.sha1(process.stdin, function (err, data) {
     *       if (err) return console.log("Something went wrong: " + err.message)
     *       console.log("Your SHA1: " + data)
     *     }
     *     base32.sha1.file('/my/file/path', console.log)
     */

    var crypto, fs
    function sha1(input, cb) {
        if (typeof crypto == 'undefined')
            crypto = require('crypto')
        var hash = crypto.createHash('sha1')
        hash.digest = (function(digest) {
            return function() {
                return encode(digest.call(this, 'binary'))
            }
        })(hash.digest)
        if (cb) { // streaming
            if (typeof input == 'string' || Buffer.isBuffer(input)) {
                try {
                    return cb(null, sha1(input))
                } catch (err) {
                    return cb(err, null)
                }
            }
            if (!typeof input.on == 'function')
                return cb({message: "Not a stream!"})
            input.on('data', function(chunk) {
                hash.update(chunk)
            })
            input.on('end', function() {
                cb(null, hash.digest())
            })
            return
        }

        // non-streaming
        if (input) {
            return hash.update(input).digest()
        }
        return hash
    }
    sha1.file = function(filename, cb) {
        if (filename == '-') {
            process.stdin.resume()
            return sha1(process.stdin, cb)
        }
        if (typeof fs == 'undefined')
            fs = require('fs')
        return fs.stat(filename, function(err, stats) {
            if (err)
                return cb(err, null)
            if (stats.isDirectory())
                return cb({dir: true, message: "Is a directory"})
            return sha1(require('fs').createReadStream(filename), cb)
        })
    }

    base32 = {
        Decoder: Decoder,
        Encoder: Encoder,
        encode: encode,
        decode: decode,
        sha1: sha1
    }

    if (typeof window !== 'undefined') {
        // we're in a browser - OMG!
        window.base32 = base32
    } else {
        module.exports = base32
    }
})();


var _bin2hex = [
    '0', '1', '2', '3', '4', '5', '6', '7',
    '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
];

var _hex2bin = [
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0, 0, 0, // 0-9
    0, 10, 11, 12, 13, 14, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, // A-F
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 10, 11, 12, 13, 14, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, // a-f
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
];

function bin2hex(str) {
    var len = str.length;
    var rv = '';
    var i = 0;
    var c;

    while (len-- > 0) {
        c = str.charCodeAt(i++);

        rv += _bin2hex[(c & 0xf0) >> 4];
        rv += _bin2hex[(c & 0x0f)];
    }

    return rv;
}

function hex2bin(str) {
    var len = str.length;
    var rv = '';
    var i = 0;

    var c1;
    var c2;

    while (len > 1) {
        h1 = str.charAt(i++);
        c1 = h1.charCodeAt(0);
        h2 = str.charAt(i++);
        c2 = h2.charCodeAt(0);

        rv += String.fromCharCode((_hex2bin[c1] << 4) + _hex2bin[c2]);
        len -= 2;
    }

    return rv;
}
//end base32.js

function hotp(key, counter, format) {

    function hotp_hexkeytobytestream(s) {
        // s is the key to be converted in bytes
        var b = new Array();
        var last = s.length;
        for (var i = 0; i < last; i = i + 2) {
            var x = s[i] + s[i + 1];
            //alert(x);
            x.toUpperCase();
            x = "0x" + x;
            x = parseInt(x);
            b[i] = String.fromCharCode(x);
            //alert(b);
        }

        //alert(b);
        var ret = new String();
        ret = b.join('');
        return ret;

    }
    function hotp_movingfactortohex(count) {
        // count is the moving factor in OTP to be converted in bytes
        v = decimaltohex(count, 16);
        var decb = new Array();
        lhex = Crypto.util.hexToBytes(v);
        for (var i = 0; i < lhex.length; i++) {
            decb[i] = String.fromCharCode(lhex[i]);
        }
        var retval = new String();
        retval = decb.join('');
        return retval;
    }

    function decimaltohex(d, padding) {
        // d is the decimal value
        // padding is the padding to apply (O pad)
        var hex = Number(d).toString(16);
        padding = typeof (padding) === "undefined" || padding === null ? padding = 2 : padding;
        while (hex.length < padding) {
            hex = "0" + hex;
        }
        return hex;
    }

    function truncatedvalue(h, p) {
        // h is the hash value
        // p is precision
        offset = h[19] & 0xf;
        v = (h[offset] & 0x7f) << 24 | (h[offset + 1] & 0xff) << 16 | (h[offset + 2] & 0xff) << 8 | (h[offset + 3] & 0xff);
        v = "" + v;
        v = v.substr(v.length - p, p);
        return v;
    }

    var hmacBytes = Crypto.HMAC(Crypto.SHA1, Crypto.charenc.Binary.stringToBytes((hotp_movingfactortohex(counter))), Crypto.charenc.Binary.stringToBytes(hotp_hexkeytobytestream(key)));

    if (format == "hex40") {
        return hmacBytes.substring(0, 10);
    } else if (format == "dec6") {
        return truncatedvalue(Crypto.util.hexToBytes(hmacBytes), 6);
    } else if (format == "dec7") {
        return truncatedvalue(Crypto.util.hexToBytes(hmacBytes), 7);
    } else if (format == "dec8") {
        return truncatedvalue(Crypto.util.hexToBytes(hmacBytes), 8);
    }
    else {
        return "unknown format";
    }

}

function dec2hex(s) { return (s < 15.5 ? '0' : '') + Math.round(s).toString(16); }
function hex2dec(s) { return parseInt(s, 16); }

function base32tohex(base32) {
    var base32chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
    var bits = "";
    var hex = "";

    for (var i = 0; i < base32.length; i++) {
        var val = base32chars.indexOf(base32.charAt(i).toUpperCase());
        bits += leftpad(val.toString(2), 5, '0');
    }

    for (var i = 0; i+4 <= bits.length; i+=4) {
        var chunk = bits.substr(i, 4);
        hex = hex + parseInt(chunk, 2).toString(16) ;
    }
    return hex;

}

function leftpad(str, len, pad) {
    if (len + 1 >= str.length) {
        str = Array(len + 1 - str.length).join(pad) + str;
    }
    return str;
}

function updateOtp( key, duration) {
            
    //var key = base32tohex($('#secret').val());
    var epoch = Math.round(new Date().getTime() / 1000.0);
    //var time = leftpad(dec2hex(Math.floor(epoch / 60)), 16, '0');

    //var key = ;
    //var epoch = ;
    var time = leftpad(dec2hex(Math.floor(epoch / duration)), 16, '0');

    var hmacObj = new jsSHA(time, 'HEX');
    var hmac = hmacObj.getHMAC(key, 'HEX', 'SHA-1', "HEX");

    //$('#qrImg').attr('src', 'https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=200x200&chld=M|0&cht=qr&chl=otpauth://totp/user@host.com%3Fsecret%3D' + $('#secret').val());
    //$('#secretHex').text(key);
    //$('#secretHexLength').text((key.length * 4) + ' bits'); 
    //$('#epoch').text(time);
    //$('#hmac').empty();

    if (hmac == 'KEY MUST BE IN BYTE INCREMENTS') {
        $('#hmac').append($('<span/>').addClass('label important').append(hmac));
    } else {
        var offset = hex2dec(hmac.substring(hmac.length - 1));
        var part1 = hmac.substr(0, offset * 2);
        var part2 = hmac.substr(offset * 2, 8);
        var part3 = hmac.substr(offset * 2 + 8, hmac.length - offset);
        //if (part1.length > 0 ) 
        //    $('#hmac').append($('<span/>').addClass('label label-default').append(part1));
        //$('#hmac').append($('<span/>').addClass('label label-primary').append(part2));
        //if (part3.length > 0) 
        //    $('#hmac').append($('<span/>').addClass('label label-default').append(part3));
    }

    var otp = (hex2dec(hmac.substr(offset * 2, 8)) & hex2dec('7fffffff')) + '';
    otp = (otp).substr(otp.length - 6, 6);

    //$('#otp').text(otp);
    document.getElementById("txtotp").innerHTML = otp;
    //document.getElementById("txtotp").value = otp;
    //document.getElementById("sotp").value = otp;
    //document.getElementById("Tz").innerHTML = otp;
    return otp;
}

//function timer()
//{
//    var epoch = Math.round(new Date().getTime() / 1000.0);
//    var countDown = 60 - (epoch % 30);
//    if (epoch % 60 == 0) updateOtp();
//    $('#updatingIn').text(countDown);
    
//}

//$(function () {
//    updateOtp();

//    $('#update').click(function (event) {
//        updateOtp();
//        event.preventDefault();
//    });

//    $('#secret').keyup(function () {
//        updateOtp();
//    });
        
//    setInterval(timer, 1000);
//});


function totp(key, format, durationOTP) {
   
    var increment = durationOTP * 1000;
    var T = parseInt(new Date().getTime() / increment); 
   
    return updateOtp(key, durationOTP);
}


function totpSilent(key, format, durationOTP) {

    var increment = durationOTP * 1000;
    var T = parseInt(((new Date().getTime())-60000) / increment);
    return updateOtp(key, durationOTP);
}




