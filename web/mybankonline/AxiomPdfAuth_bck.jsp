<%-- 
    Document   : AxiomPdfAuth.jsp
    Created on : Jan 24, 2016, 9:06:59 AM
    Author     : Ideasventure
--%>

<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.axiom.v2.face.pdfsigning.dateTimeConvert"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.axiom.common.AxiomProtectConnector"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.mollatech.axiom.v2.core.rss.Remotesignature"%>
<%@page import="com.mollatech.axiom.bridge.crypto.LoadSettings"%>
<%@page import="axiom.web.service.AxiomWrapper"%>
<%@page import="com.mollatech.axiom.v2.core.rss.RssUserCerdentials"%>
<%@page import="org.bouncycastle.util.encoders.Base64"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Axiom PdfAuthentication(Download PDF)</title>
        <script src="../assets/js/jquery.js" type="text/javascript"></script>
        <script>
            function base64Encode(text) {

                if (/([^\u0000-\u00ff])/.test(text)) {
                    throw new Error("Can't base64 encode non-ASCII characters.");
                }

                var digits = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
                        i = 0,
                        cur, prev, byteNum,
                        result = [];

                while (i < text.length) {

                    cur = text.charCodeAt(i);
                    byteNum = i % 3;

                    switch (byteNum) {
                        case 0: //first byte
                            result.push(digits.charAt(cur >> 2));
                            break;

                        case 1: //second byte
                            result.push(digits.charAt((prev & 3) << 4 | (cur >> 4)));
                            break;

                        case 2: //third byte
                            result.push(digits.charAt((prev & 0x0f) << 2 | (cur >> 6)));
                            result.push(digits.charAt(cur & 0x3f));
                            break;
                    }

                    prev = cur;
                    i++;
                }

                if (byteNum == 0) {
                    result.push(digits.charAt((prev & 3) << 4));
                    result.push("==");
                } else if (byteNum == 1) {
                    result.push(digits.charAt((prev & 0x0f) << 2));
                    result.push("=");
                }

                return result.join("");
            }


            function isNullOrWhitespace(text) {
                if (text == null) {
                    return true;
                }
                return text.replace(/\s/gi, '').length < 1;
            }
        </script>
    </head>
    <body>

        <script>
            var latitude = "";
            var longitude = "";
            var altitude = "";
            var accuracy = "";





            //start
//   if (navigator.geolocation) {
//         alert("get geolocation ");
//        navigator.geolocation.getCurrentPosition(success, error, geo_options);
//    }function success(position) {
//         latitude = position.coords.latitude;
//         longitude = position.coords.longitude;
//         altitude = position.coords.altitude;
//         accuracy = position.coords.accuracy;
//
//    }
//    var geo_options = {
//        enableHighAccuracy: true,
//        maximumAge: 30000,
//        timeout: 27000
//    };
//     function error(error) {
//       alert("Unable to retrieve your location due to ");
//    }
//    ;




//end



            var deviceCheck = 'false';
            var emailAvailable = 'false';
        </script>
        <%
            boolean deviceCheck = false;
            boolean countryCheck = false;
            String emailChecked = "";
            boolean locationCheck = true;

            String locationFlag = request.getParameter("CountryO");
            if (locationFlag != null) {
                locationCheck = false;
            }
            String userAgent = request.getHeader("User-Agent");
            System.out.print("Useragent" + userAgent);
            String deviceid = new String(Base64.encode(userAgent.getBytes()));
            AxiomWrapper axWrapper = new AxiomWrapper();
            String sessionId = null;
            String qrDataUrl = "";
            String refid = request.getParameter("refid");
            String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
            String password = LoadSettings.g_sSettings.getProperty("axiom.password");
            String channelid = LoadSettings.g_sSettings.getProperty("axiom.channelid");
            qrDataUrl = LoadSettings.g_sSettings.getProperty("axiom.qrcodeurl");
            sessionId = axWrapper.OpenSession(channelid, remotelogin, password);
            session.setAttribute("deviceid", deviceid);
            session.setAttribute("sessionId", sessionId);
            session.setAttribute("axWrapper", axWrapper);
            session.setAttribute("refid", refid);
            Remotesignature[] signInfoByUser = axWrapper.getAllSignatureByUserid(sessionId, refid, 2);
            Remotesignature remSign = signInfoByUser[0];
            request.getSession().setAttribute("pdfPath", remSign.getDataToSign());
            request.getSession().setAttribute("remSignObj", remSign);
            if (remSign.getAllowedPhones().contains("Windows")) {
                if (userAgent.contains("Windows")) {
                    session.setAttribute("_phoneos", "windows");
                    deviceCheck = true;
                }
            } else if (userAgent.contains("Android")) {
                if (remSign.getAllowedPhones().contains("Android")) {
                    session.setAttribute("_phoneos", "Android");
                    deviceCheck = true;
                }
            } else if (userAgent.toLowerCase().contains("ios")) {
                if (remSign.getAllowedPhones().toLowerCase().contains("iphone")) {
                    session.setAttribute("_phoneos", "Iphone");
                    deviceCheck = true;
                }

            }

            if (remSign.getAllowedPhones().contains("docheck")) {
                if (remSign.getAllowedUsers() != null) {
                    HashMap deviceH = (HashMap) AxiomProtectConnector.deserializeFromObject(new ByteArrayInputStream(remSign.getAllowedUsers()));
                    if (deviceH != null) {
                        String authpdf = (String) deviceH.get(deviceid);
                        if (authpdf != null) {
                            if (authpdf.toLowerCase().contains("yes")) {
                                emailChecked = "YES";
                                session.setAttribute("emailChecked", "YES");
                            }else {
                            emailChecked = "NOTAUTH";
                            session.setAttribute("emailChecked", "NOTAUTH");
                        }

                    } else {
                        emailChecked = "NOTRECIEVED";
                        session.setAttribute("emailChecked", "NOTRECIEVED");
                    }
                    }
                } else {
                    emailChecked = "NO";
                }
            } else {
                emailChecked = "YES";
            }

            request.getSession().setAttribute("emailChecked", emailChecked);

        %>

        <h1></h1>
        <form id="frmAuth" name="frmAuth" action="./pdfdownload">
            <input type="hidden" id="_locationChunk" name="_locationChunk">

            <%if (deviceCheck == true && emailChecked.equals("YES")) {%>
            <h1>Axiom Protect Document Autherntication</h1>
            <input type="submit" id="btnSubmit" value="Click Here to Download" />
            <%}%>

            <%if (emailChecked.equals("NOTRECIEVED")) {%>
            <h1>Axiom Protect Document Autherntication</h1>
            <input type="text" id="txtEmailId" name="txtEmailId"/>
            <input type="submit" id="btnSubmit" value="Sumbit Request" />
            <%}
               if (emailChecked.equals("NOTAUTH")) {%>
            <h1>Axiom Protect Document Autherntication</h1>
            Your request for downloading doc is not accesspted yet,Please try after some time.
            <%}
              if (locationCheck == false) {%>
            <h1>Axiom Protect Document Autherntication</h1>
            Your Location seems to be Invalid
            <%} else if (emailChecked.equals("NO")) {%>
            <h1>Axiom Protect Document Autherntication</h1>
            <input type="text" id="txtEmailId" name="txtEmailId"/>
            <input type="submit" id="btnSubmit" value="Sumbit Request" />
            <%}%>

            <input type="hidden" name="demo" id="demo"/>
        </form>

    </body>
    <script>
        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                alert("Geolocation is not supported by this browser.");
            }
        }
        function showPosition(position) {

            latitude = position.coords.latitude;
            longitude = position.coords.longitude;

            getLocationbyLatLong(latitude, longitude, "");
        }

        function getLocationbyLatLong(latitude, longitude, username) {
//    alert("hi");
            var latlang = encodeURIComponent(latitude + "," + longitude);
            alert("Get logitude and lattitude");
            alert(latlang);
            var s = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + latlang + "&sensor=false";
            $.ajax({
                type: 'GET',
                url: s,
                async: false,
                dataType: 'json',
                success: function (data) {
                    var loc = JSON.stringify(data);
                    //var results="";
                    //results=data;
                    //alert(loc);
                    //  loc = base64Encode(loc);
                    for (var i = 0; i < data.results[0].address_components.length; i++)
                    {
                        var shortname = data.results[0].address_components[i].short_name;
                        var longname = data.results[0].address_components[i].long_name;
                        var type = data.results[0].address_components[i].types;

                        if (type.indexOf("country") != -1)
                        {
                            if (!isNullOrWhitespace(shortname))
                            {
                                $("#_locationChunk").val(longname);
                                return shortname;
                            }
                            else
                            {
                                $("#_locationChunk").val(longname);
                                return longname;
                            }
                        }
                    }


//         var s = './setLocationforVerification?locationchunk=' + $("#_locationChunk").val(base64Encode(loc));
//        $.ajax({
//        type: 'GET',
//        url: s,
//        dataType: 'json',
//        success: function (data) {
//            var loc = JSON.stringify(data);
////            loc = base64Encode(loc);
//            $("#_locationChunk").val(loc);
////            document.getElementById("locations").value = loc.toString();
//     //       GetSecurePhrase(username, latitude, longitude, ilocation, ibrowser);
////             alert(locations);
//        }
//    });


                }
            });

        }
        getLocation();
    </script>
</html>
