<%@page import="java.nio.channels.Channels"%>
<%@page import="com.mollatech.axiom.v2.core.rss.Operators"%>
<%@page import="java.io.BufferedWriter"%>
<%@page import="com.mollatech.axiom.common.AxiomProtectConnector"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.axiom.v2.core.rss.AxiomTokenData"%>
<%@page import="com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails"%>
<%@page import="com.mollatech.axiom.v2.core.rss.RssUserCerdentials"%>
<%@page import="org.json.JSONObject"%>
<%@page import="axiom.web.service.AxiomWrapper"%>
<%
    try {

        AxiomWrapper axWrapper = new AxiomWrapper();
        String host = request.getContextPath().toLowerCase();
        
        String resultString = "Failed";
        String result = "success";
        String message = "Token Assigned Successfully....";
        BufferedWriter bufferedWriter = null;
      
                  Operators operatorS = (Operators) request.getSession().getAttribute("_apOprDetail");
      
            Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
            String sessionId1 = (String) request.getSession().getAttribute("_apSessionID");

            //audit parameter
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
      

            String _fileName = request.getParameter("_filename");
            String _emailid = request.getParameter("_emailId");
            String _otp = request.getParameter("_otp");
            String _referneceid = request.getParameter("_referneceid");

            String _user_name = request.getParameter("savemepost");

        
        JSONObject json = new JSONObject();

        //String regcode =(string)Request.Params["_apregid"];
        String regcode = session.getAttribute("_apregid").toString();   // cm.getString("_apregid");     

        if (regcode != null
                && session.getAttribute("_apsession") != null
                && session.getAttribute("_apuserObj") != null) {

            String ip = request.getRemoteAddr();

            //strPayLoad = MakeMyPayLoad(Request.QueryString["_lattitude"].ToString(), Request.QueryString["_longitude"].ToString(), ip
            String sessionId = session.getAttribute("_apsession").toString();
            RssUserCerdentials _userObj = (RssUserCerdentials) session.getAttribute("_apuserObj");

            response.setContentType("application/json");

            int icategory = 2;
            int isubcategory = 1;

            if (_userObj.getTokenDetails() != null) {
                for (int i = 0; i < _userObj.getTokenDetails().size(); i++) {
                    AxiomCredentialDetails axiomCredentialDetails = _userObj.getTokenDetails().get(i);
                    if (axiomCredentialDetails != null) {
                        if (icategory == axiomCredentialDetails.getCategory()) {
                            axiomCredentialDetails.setStatus(1); //mark as active

                            _userObj.getTokenDetails().add(axiomCredentialDetails);
                        }

                    }
                }
            }

            //2 - get token data
            AxiomTokenData secret = axWrapper.ActivateToken(sessionId, _userObj, regcode, null);

            if (secret != null) { // success

                String UtcTime = (String) request.getParameter("_apusertime");
                String deviceType = (String) request.getParameter("_apdevicetype");

                // String UtcTime = cm.getString("_apusertime");
                //String deviceType = cm.getString("_apdevicetype");
                if (deviceType == null || UtcTime == null) {

                    json.put("_result", "error");
                    json.put("_message", "Device Type OR Time is not defined!!!");
                    response.setContentType("application/json");
                    out.print(json);

                    return;
                }

                long pubExpiretime = 0;

                if (deviceType.equals("private")) {
                    long expirytime = secret.getPublicWebTokenExpiryTime() * 60 * 1000 * 365 * 2;  // 2 years
                    pubExpiretime = Integer.parseInt(UtcTime) + expirytime;
                    //DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    //DateTime date = start.AddMilliseconds(pubExpiretime).ToLocalTime();
                } else if (deviceType.equals("public")) {
                    long expirytime = secret.getPublicWebTokenExpiryTime() * 60 * 1000;     // 10 minutes
                    pubExpiretime = Integer.parseInt(UtcTime) + expirytime;
                    // DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    // DateTime date = start.AddMilliseconds(pubExpiretime).ToLocalTime();
                }

                // axWrapper.PrintLogs("Client PC UtcTime is " + date);
                json.put("_result", "success");
                Date d = new Date();

                byte[] data = AxiomProtectConnector.Bas64SHA1Inner(_userObj.getRssUser().getUserId());
                String wtidq = new String(data);
                String hex = new String(data).replace("", "-");
                String wtid = hex;
                json.put("_pubTokenExpirytime", pubExpiretime);
                json.put("_identifier", wtid);
                json.put("_secret", secret.getOTPSecret());
                json.put("_otplength", secret.getOTPLength());
                json.put("_otpduration", secret.getOTPduration());
                json.put("_pinAttemptCount", secret.getInvalidPinAttempt());
                response.setContentType("application/json");
                out.print(json);

                return;
            } else {
                result = "error";
                message = "Failed to activate your token!!!";
                json.put("_result", result);
                json.put("_message", message);

                //String callbackstrr = Request.Params["jsoncallback"];
                //String strresponser = callbackstrr + "(" + json.ToString() + ")";
                response.setContentType("application/json");
                out.print(json);

                return;

            }
        } else {
            result = "error";
            message = "Insufficient Parameters!!!";

            json.put("_result", result);
            json.put("_message", message);
            //String callbackstrr = Request.Params["jsoncallback"];
            //String strresponser = callbackstrr + "(" + json.ToString() + ")";
            response.setContentType("application/json");
            out.print(json);

            return;

        }
    } catch (Exception ex) {
        ex.printStackTrace();
    }


%>