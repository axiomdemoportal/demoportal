















var type;

function validateRepeat() {
	var message = "Repeat payments cannot be set to today's date.";
	if ($('input:radio[name=repeatOption]:checked').val() == 'true' && $('#firstProcessDate').val() == getToday()) {
		alert(formatText(message));
		return false;
	}
}

function validateOneTimeIbgFTForm(theform){
	type = "add";
	return validateRequired(theform)
		&& validateMask(theform)
		&& validateDecimal(theform.amount, "Amount (RM)", 0, 9999999999, 10, 2)
		&& validateRepeat();
}

function required() {
	if (type == "add") {
		this.aa = new Array("fromAccountNo", "From Account");
		
		if ($('input:radio[name=registeredBeneAcc]:checked').val() == 'true') {
			this.ab = new Array("myBeneAccountNo", "Favourite Accounts");
		} else {
			this.ab = new Array("adhocAccount", "Other Account");
		}
		
		this.ac = new Array("beneficiaryBank", "Beneficiary Bank");		
		this.ad = new Array("beneficiaryName", "Beneficiary Name");
		
		if ($('input:radio[name=beneIdValidation]:checked').val() == 'true') {
			this.ae = new Array("beneIdType", "Beneficiary ID Type");
			this.af = new Array("beneIdNo", "Beneficiary ID No");
		}
		
		this.ag = new Array("paymentType", "Payment Type");
		this.ah = new Array("amount", "Amount (RM)");
		this.ai = new Array("remarks", decodeEntities("Recipient&#039;s Reference"));
		this.aj = new Array("firstProcessDate", "Effective Date");
		
		if ($('input:radio[name=repeatOption]:checked').val() == 'true') {
  		 	this.ak = new Array("frequency", "Frequency");
  		 	this.al = new Array("numberOfTransfer", "Total Number of Transfers"); 
  		}
	}
	else if (type == "confirm") {
		this.aa = new Array("tac", "Transaction Authorization Code");
	}
}

function mask() {
	if (type == "add") {
		this.aa = new Array("otherAccount", "Other Account", "^[0-9]*$", "{0} must be a number");
		this.ab = new Array("beneficiaryName", "Beneficiary Name", "^[a-zA-Z 0-9@'/.()&]*$", "Invalid char found in {0}");
		this.ac = new Array("remarks", decodeEntities("Recipient&#039;s Reference"), "^[a-zA-Z 0-9@'/.()&]*$", "Invalid char found in {0}");
		this.ad = new Array("paymentDescription", "Other Payment Details", "^[a-zA-Z 0-9@'/.()&]*$", "Invalid char found in {0}");
	} else if (type == "confirm") {
		this.ad = new Array("tac", "Transaction Authorization Code", "^[0-9]*$", "{0} must be a number");
	}
}

function validateConfirm(theform) {
	type = "confirm";
	if (validateRequired(theform) && validateMask(theform)) {
		if (!validateLength("tac", "Transaction Authorization Code", 6, 6, 'Invalid field length for {0}'))
			return false;
	} else {
		return false;
	}
}

function decodeEntities(str) {
	return str.replace(/&#(\d+);/g, function(match, number){ return String.fromCharCode(number); });
}

function populateBeneficiaryDetails(form) {
	if ($('#myBeneAccountNo').val() == '') {
		$('#beneficiaryBank, #beneBank, #beneficiaryName, #remarks, #paymentDescription, #amount, #beneIdType, #beneIdNo, #paymentType').val('');
		$('input:radio[name=beneIdValidation][value=false]').prop('checked', true);
		$('.beneIdValidate').hide();
	}
	else {
		$(form).attr('action', encodeURL()).submit();
	}
}

function populatePaymentType(form) {
	$(form).attr('action', encodeURL()).submit();
}
