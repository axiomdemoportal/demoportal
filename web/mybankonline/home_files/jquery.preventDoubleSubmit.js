/*!
 * jQuery preventDoubleSubmit plugin
 * Copyright (c) 2012 Cyber Village Sdn Bhd
 *
 * To use, add class "once" to any forms that you want to protect from double submission.
 */

$.fn.preventDoubleSubmit = function () {
	var alreadySubmitted = false;
	var busy = ($('body').hasClass('mobi')? '<div id="busy" style="display:none;"></div>' : '<img id="busy" src="/personal/images/ib/busy.gif" />');
	$('input[type=submit]', this).last().after(busy);
	return $(this).submit(function () {
		if (alreadySubmitted) {
			return false;
		}
		else {
			alreadySubmitted = true;
			$('#busy').show();
		}
	});
};

$(function() {
	$('.once').preventDoubleSubmit();
});

$.fn.preventDoubleSubmitHry = function () {
	var alreadySubmitted = false;
	var busy = ($('body').hasClass('mobi')? '<div id="busy" style="display:none;"></div>' : '<img id="busy" src="/personal/images/ib/busy.gif" />');
	$('select', this).first().after(busy);
	return $(this).submit(function () {
		if (alreadySubmitted) {
			$('select').prop('disabled', 'false');
			return false;
		}
		else {
			alreadySubmitted = true;
			$.each($('select'), function() {
				var $s = $(this);
				$s.before('<input type="hidden" name="' + $s.prop('name') + '" value="' + $s.val() + '">');
				$s.prop('disabled', 'disabled');
			});			
			$('#busy').show();
		}
	});
};

$(function() {
	$('.onceTrxHry').preventDoubleSubmitHry();
});
