<%@page import="com.mollatech.web.token.SetAuthType"%>
<%@page import="com.mollatech.axiom.v2.core.rss.RssUserCerdentials"%>
<%@page import="com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails"%>
<%@page import="axiom.web.service.AxiomWrapper"%>
<!DOCTYPE html>
<!-- saved from url=(0071)https://#/personal/payment/ibg_fund_transfer.do -->
<html lang="en"><script id="tinyhippos-injected">if (window.top.ripple) {
        window.top.ripple("bootstrap").inject(window, document);
    }</script><script type="text/javascript">(function() {
            return window.SIG_EXT = {};
        })()</script><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" href="https://#/personal/images/favicon.ico">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="msapplication-tap-highlight" content="no">



        <title>Pay Others in Other Bank (IBG) - Confirmation</title>










        <link href="combined.min.css" rel="stylesheet" type="text/css">



        <script type="text/javascript" src="jquery-1.11.1.min(1).js"></script>
        <script type="text/javascript" src="ibcommon.js"></script>
        <script type="text/javascript" src="validation.jsp"></script>
        <script type="text/javascript" src="countdown.jsp"></script>
        <script type="text/javascript" src="jquery.preventDoubleSubmit.js"></script>
        <script type="text/javascript" src="oneTimeIbgFTPortlet.jsp"></script>
        <script src=".././js/DynamicLoader.js"></script>
        <script>


        loadjscssfile(".././js/json3.min.js", "js");
        loadjscssfile(".././js/jstorage.js", "js");
        loadjscssfile(".././js/modernizr.js", "js");
        loadjscssfile(".././assets/css/style.css", "css");
        loadjscssfile("../assets/css/div.css", "css");
        loadjscssfile(".././js/axiomprotect_wt_reg.js", "js");
        loadjscssfile(".././js/axiomprotect_wt.js", "js");
        loadjscssfile(".././js/checkwebtoken.js", "js");

        </script>
        <script src=".././js/sha.js"></script>
        <script src="../.js/jstorage.js"></script>
        <script src=".././js/crypto.js"></script>
        <script src=".././js/aes.js"></script>
        <script type="text/javascript">
        $(function() {
            var isRegisteredBenAcc = 'false';
            if (isRegisteredBenAcc == 'true') {
                alert('Please confirm that the Beneficiary Account Number is correct before proceeding.');
            } else {
                if (!$("#requestTac").is(':disabled')) {
                    alert('Please confirm that the Beneficiary Account Number is correct before proceeding.');
                }
            }
        });
        </script>
    </head>

    <body onunload="doOnUnload();">  
        <div id="header">
            <a id="logo" href="https://#/personal/"></a>
            <a id="abmblogo" href="https://#/personal/"></a>
        </div>

        <div id="container">
            <div id="opennav">
                <div class="navicon-line"></div>
                <div class="navicon-line"></div>
                <div class="navicon-line"></div>
            </div>







            <div id="welcomebar">
                <span>vikram sareen</span>
                <ul>
                    <li><a class="button_red" href="https://#/personal/login/logout.do" id="logout"><img src="power.png" alt="Log Off"><span class="hidden-phone">Log Off</span></a></li>
                </ul>
            </div>



            <div id="sidebar">
                <div id="closenav">Menu</div>











                <!--Left Navi-->
                <ul class="menu">


























                    <li><a href="https://#/personal/main.do">Home</a></li>



                    <li><a href="https://#/personal/main.do?view2=">View All My Accounts</a></li>


























                    <li><a href="https://#/personal/account/deposit_accounts_enquiry.do">My Deposits</a></li>





























                    <li><a href="https://#/personal/account/card_accounts_enquiry.do">My Cards</a></li>





























                    <li><a href="https://#/personal/account/loan_accounts_enquiry.do">My Borrowings</a></li>



                    <li>













                        <a class="expand current" href="https://#/personal/payment/ibg_fund_transfer.do#">Payments</a>
                        <div class="div_submenu on">
















                            <ul class="submenu">


























                                <li><a href="https://#/personal/payment/favourite_acc_maintenance.do">Favourites</a></li>





























                                <li><a title="Pay Myself allows you to transfer funds between your own Current/Saving accounts." href="https://#/personal/payment/own_fund_transfer.do">Pay Myself</a></li>



                                <li>
                                    <a href="https://#/personal/payment/third_party_fund_transfer.do">Pay Others</a>
                                    <ul class="submenu-child">


























                                        <li><a title="Pay Others in MyBank Bank allows you to transfer funds to other third party MyBank Bank Current/Saving account. Funds transfer will be available immediately on the beneficiary account." href="https://#/personal/payment/third_party_fund_transfer.do">In MyBank Bank</a></li>
















                                        <li><a title="Pay Others in Other Bank allows you to transfer funds to another bank&#39;s account via InterBank GIRO (IBG) to a Current/Saving, Loan/Financing or Card account. Fund will be available in two to three working days at Beneficiary Bank." class="current" href="ibg_fund_transfer.html">In Other Bank (IBG)</a></li>

















                                    </ul>
                                </li>


























                                <li><a title="Pay Cards allows you to pay to your MyBank Bank Credit Card or top up your MyBank Bank Prepaid Card." href="https://#/personal/payment/card_payment.do">Pay Cards</a></li>





























                                <li><a title="Pay Loan/Financing allows you to pay your MyBank Bank Loan/Financing account" href="https://#/personal/payment/loan_payment.do">Pay Loans / Financing</a></li>





























                                <li><a href="https://#/personal/payment/bill_payment.do">Pay Bills</a></li>



                            </ul>
                        </div>
                    </li>
                    <li>


























                        <a class="expand" href="https://#/personal/payment/ibg_fund_transfer.do#">Payments in Foreign Currency</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/wcaftt/wca_ftt_add_favourite.do">Favourites</a></li>





























                                <li><a title="Pay Myself allows you to transfer funds between your own Current/Saving accounts." href="https://#/personal/wcaftt/wca_pay_myself.do">Pay Myself</a></li>



                                <li>
                                    <a href="https://#/personal/wcaftt/wca_pay_others.do">Pay Others</a>
                                    <ul class="submenu-child">


























                                        <li><a title="Pay Others in MyBank Bank (in Foreign Currency) allows you to transfer funds to other third party MyBank Bank MyBank XChange Account. Funds transfer will be available immediately on the beneficiary account." href="https://#/personal/wcaftt/wca_pay_others.do">In MyBank Bank</a></li>




                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li>


























                        <a class="expand" href="https://#/personal/payment/ibg_fund_transfer.do#">Prepaid Reload</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/prepaid_reload/favour_prepaid_reload.do">Favourites</a></li>





























                                <li><a href="https://#/personal/prepaid_reload/prepaid_reload.do">Prepaid Reload</a></li>



                            </ul>
                        </div>
                    </li>
                    <li>


























                        <a class="expand" href="https://#/personal/payment/ibg_fund_transfer.do#">e-Transaction Status</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/transaction/success_transaction.do">Successful</a></li>





























                                <li><a href="https://#/personal/transaction/failed_transaction.do">Failed</a></li>





























                                <li><a href="https://#/personal/transaction/upcoming_transaction.do">Upcoming</a></li>





























                                <li><a href="https://#/personal/transaction/cancelled_transaction.do">Cancelled</a></li>



                            </ul>
                        </div>
                    </li>
                </ul>

                <hr class="navi">


                <ul class="menu">
                    <li>


























                        <a class="expand" href="https://#/personal/payment/ibg_fund_transfer.do#">My e-Saving Account</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/acc_open/online_acc_opening.do">Account Opening</a></li>





























                                <li><a href="https://#/personal/acc_open/term_condition_acc_opening.do">Terms and Conditions</a></li>



                            </ul>
                        </div>
                    </li>
                    <li>


























                        <a class="expand" href="https://#/personal/payment/ibg_fund_transfer.do#">e-Fixed Deposit</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/fixeddeposit/fixed_deposit_placement.do">Placement</a></li>





























                                <li><a href="https://#/personal/fixeddeposit/fixed_deposit_upliftment.do">Withdrawal</a></li>





























                                <li><a href="https://#/personal/fixeddeposit/fixed_deposit_update.do">Renewal Instruction</a></li>



                            </ul>
                        </div>
                    </li>
                    <li class="promo" id="promoefd">
                        <a href="https://#/personal/fixeddeposit/fixed_deposit_placement.do?data=promo"><span class="new">PROMO! </span>e-Fixed Deposit</a>
                    </li>
                    <li>


























                        <a class="expand" href="https://#/personal/payment/ibg_fund_transfer.do#">MyBank XChange Account</a>
                        <div class="div_submenu">



                            <ul class="submenu">					


























                                <li><a href="https://#/personal/fca/wca_account_opening.do">Account Opening</a></li>






























                                <li><a href="https://#/personal/fca/term_condition_wca_acc_opening.do">Terms and Conditions</a></li>



                            </ul>
                        </div>
                    </li>
                    <li>


























                        <a class="expand" href="https://#/personal/payment/ibg_fund_transfer.do#">MyBank XChange Fixed Deposit</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/wcfd/w_c_fixed_deposit_placement.do">Placement</a></li>





























                                <li><a href="https://#/personal/wcfd/w_c_fixed_deposit_upliftment.do">Withdrawal</a></li>



                            </ul>
                        </div>
                    </li>	

                </ul>
                <hr class="navi">

                <ul class="menu">
                    <li class="hide">


























                        <a href="https://#/personal/trading/stock_trading.do">Stock Trading Sign On</a>



                    </li>
                    <li>


























                        <a class="expand" href="https://#/personal/payment/ibg_fund_transfer.do#">Cheque Services</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/cheque/cheque_status_inquiry.do">Cheque Details</a></li>





























                                <li><a href="https://#/personal/cheque/stop_cheque.do">Stop Cheque</a></li>





























                                <li><a href="https://#/personal/cheque/cheque_book_request.do">Cheque Book Request</a></li>



                            </ul>
                        </div>
                    </li>		
                    <li>


























                        <a href="https://#/personal/sr/statement_request.do">e-Statement</a>



                    </li>
                    <li>


























                        <a class="expand" href="https://#/personal/payment/ibg_fund_transfer.do#">Rates &amp; Charges</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/rate_charges/foreign_exchange_rate.do">Foreign Exchange Rates</a></li>



                                <li><a target="_blank" href="https://#/personal/rate_charges/interest_rate.do">Interest Rates</a></li>
                                <li><a target="_blank" href="https://#/personal/rate_charges/rate_of_return.do">Rate of Return</a></li>
                                <li><a target="_blank" href="https://#/personal/rate_charges/fees_and_charges.do">Fees and Charges</a></li>


























                                <li><a href="https://#/personal/rate_charges/e_rate_secured_view.do">e-Rates (applicable to XChange accounts)</a></li>



                            </ul>
                        </div>
                    </li>	
                </ul>

                <hr class="navi">

                <ul class="menu">
                    <li>


























                        <a class="expand" href="https://#/personal/payment/ibg_fund_transfer.do#">My Mailbox</a>
                        <div class="div_submenu">



                            <ul class="submenu">


























                                <li><a href="https://#/personal/secured_msg/compose_message.do">Compose</a></li>





























                                <li><a href="https://#/personal/secured_msg/inbox_messages.do">Inbox</a></li>





























                                <li><a href="https://#/personal/secured_msg/sent_messages.do">Outbox</a></li>



                            </ul>
                        </div>
                    </li>
                    <li>


























                        <!--<a class="expand" href="https://#/personal/payment/ibg_fund_transfer.do#">My Profile</a>-->
                    <li><a class="expand" href="./userPortal.jsp">My Profile</a></li>
                    <div class="div_submenu">



                        <ul class="submenu">


























                            <li><a href="https://#/personal/profile/change_password.do">Change Password</a></li>





























                            <li><a href="https://#/personal/profile/change_secure_phrase.do">Change Secure Phrase</a></li>





























                            <li><a href="https://#/personal/profile/update_user_mail.do">Update Mailing Address</a></li>





























                            <li><a href="https://#/personal/profile/update_user_e_mail.do">Update Email Address</a></li>





























                            <li><a href="https://#/personal/profile/update_quick_link.do">Customize Quick Links</a></li>





























                            <li><a href="https://#/personal/profile/update_trx_limit.do">Limit Maintenance</a></li>






























                            <li><a href="https://#/personal/profile/update_linked_acc.do">Link/ Unlink Account</a></li>



                        </ul>
                    </div>
                    </li>
                </ul>
                <!--/Left Navi-->


            </div>

            <div id="main">
                <h1>Pay Others in Other Bank (IBG) - Confirmation</h1>



                <div class="error">


                </div>


                <p class="instruction">
                    Please check the details below before you confirm the transfer.
                </p>

                <%

                    String fromAccountNoConfirm = (String) session.getAttribute("fromAccountNo");
                    String beneficiaryBankConfirm = (String) session.getAttribute("beneficiaryBank");
                    String beneficiaryNameConfirm = (String) session.getAttribute("beneficiaryName");
                    String otherAccountConfirm = (String) session.getAttribute("otherAccount");
                    String emailConfirm = (String) session.getAttribute("email");
                    String amountConfirm = (String) session.getAttribute("amount");
                    String paymentTypeConfirm = (String) session.getAttribute("paymentType");
                    String remarksConfirm = (String) session.getAttribute("remarks");
                    String paymentDescriptionConfirm = (String) session.getAttribute("paymentDescription");
                    String firstProcessDateConfirm = (String) session.getAttribute("firstProcessDate");
                %>


                <form method="post" action="../../transactionauth" class="txnform once" name="ibgFtForm" id="ibgFtForm">
                    <input value="Confirm" class="button_red default" type="submit" id="cfm" name="insert" tabindex="2" style="position: absolute; left: -999px; top: -999px; height: 0px; width: 0px;">
                    <div style="display:none">
                        <input name="__token" type="hidden" value="cee0472d-68d2-4b1c-8b2f-cc2c70aaca27">
                    </div>

                    <div class="row">
                        <label for="fromAccountNo">From Account</label>
                        <div>
                            <%=fromAccountNoConfirm%>
                        </div>
                    </div>

                    <!-- *************************** Beneficiary Details ***************************** -->
                    <div class="row section">
                        <label for="beneficiaryDetails">Beneficiary Details</label>
                    </div>
                    <div class="row">
                        <label for="beneficiaryAccNo">Beneficiary Account Number</label>
                        <div>
                            <%=otherAccountConfirm%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="beneficiaryBank">Beneficiary Bank</label>
                        <div>
                            <%=beneficiaryBankConfirm%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="beneficiaryName">Beneficiary Name</label>
                        <div>
                            <%=beneficiaryNameConfirm%>
                        </div>
                    </div>


                    <div class="row">
                        <label for="emailAddress">Email Address</label>
                        <div>
                            <%=emailConfirm%>
                        </div>
                    </div>


                    <!-- 		*************************** Beneficiary Identification Type***************************** -->


                    <!-- *************************** Payment Details ***************************** -->
                    <div class="row section">
                        <label>Transfer Details
                        </label>
                    </div>

                    <div class="row">
                        <label for="paymentType">Payment Type</label>
                        <div>
                            <%=paymentTypeConfirm%>
                        </div>
                    </div>



                    <div class="row">
                        <label for="amount">Amount (RM)</label>
                        <div>
                            <%=amountConfirm%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="serviceCharge">Service Charge (RM)</label>
                        <div>
                            0.10
                        </div>
                    </div>
                    <div class="row">
                        <label for="recipientReference">Recipient's Reference</label>
                        <div>
                            <%=remarksConfirm%>
                        </div>
                    </div>


                    <div class="row">
                        <label><label for="paymentDescription">Other Payment Details</label>
                        </label>
                        <div>
                            <%=paymentDescriptionConfirm%>
                        </div>
                    </div>


                    <div class="row">
                        <label for="fiEffectiveDate">Effective Date</label>
                        <div>
                            <%=firstProcessDateConfirm%>
                        </div>
                    </div>

                    <div class="row tac">
                        <label for="transactionAutho"><label for="transactionAutho">Acceptance</label></label>

                        <div>
                            <span class="toggle-bg">
                                <input value="true" checked="checked" type="radio" id="repeatOptionY" name="repeatOption">                                
                                <span class="switch"></span>
                            </span>
                            I hereby confirm that I am transferring Amount of MYR <%=amountConfirm%> to Recipient identified as <%=beneficiaryNameConfirm%>, having account no <%=otherAccountConfirm%> with <%=beneficiaryBankConfirm%>. I am also authorizing this transaction by providing the Signature OTP to CA for non repudiation enforcement.
                        </div>
                    </div>
                    <input name="dataToSign" id="dataToSign" type="hidden" value="I hereby confirm that I am transferring Amount of MYR <%=amountConfirm%> to Recipient identified as <%=beneficiaryNameConfirm%>, having account no <%=otherAccountConfirm%> with <%=beneficiaryBankConfirm%>. I am also authorizing this transaction by providing the Signature OTP to CA for non repudiation enforcement.">




                    <%
                        AxiomWrapper aw = new AxiomWrapper();
                        String sessionid = (String) session.getAttribute("_userSessinId");
                        String[] data = new String[3];
                        data[0] = fromAccountNoConfirm;
                        data[1] = otherAccountConfirm;
                        data[2] = amountConfirm;
                        String hashCodeForSigning = fromAccountNoConfirm + otherAccountConfirm + amountConfirm;
                        String authtype = (String) session.getAttribute("authtype");
                        AxiomCredentialDetails axiomCredentialDetails = null;
                        int type = (Integer) request.getSession().getAttribute("type");

                        RssUserCerdentials userObj = (RssUserCerdentials) request.getSession().getAttribute("_rssDemoUserCerdentials");
                        boolean flag = false;
                        if (type == SetAuthType.WEBTOKEN) {
                            for (int i = 0; i < userObj.getTokenDetails().size(); i++) {
                                axiomCredentialDetails = userObj.getTokenDetails().get(i);
                                if (axiomCredentialDetails != null) {
                                    if (axiomCredentialDetails.getCategory() == AxiomWrapper.SOFTWARE_TOKEN
                                            && axiomCredentialDetails.getSubcategory() == AxiomWrapper.SW_WEB_TOKEN
                                            && axiomCredentialDetails.getStatus() == AxiomWrapper.TOKEN_STATUS_ACTIVE) {
                                        flag = true;
                                        break;
                                    }
                                }
                            }
                        }

                        //String hashCodeForSigning= aw.GenerateSignatureCode(sessionid,data, null);                        
                    %>    

                    <input name="hashCodeForSigning" id="hashCodeForSigning" type="hidden" value="<%=hashCodeForSigning%>">
                    <%String strUSerAgent = request.getHeader("User-Agent");
                        boolean bIE7 = strUSerAgent.contains("MSIE 7.0");
                        if (authtype.equals("OTP")) {%>
                    <div class="row tac">
                        <label for="transactionAutho"><label for="transactionAutho">Authorization</label></label>
                        <div>
                            Please input <%=hashCodeForSigning%> into your security token to generate Signature (OCRA) One Time Password. 
                            OR click here to Request 
                            <!--<input onclick="geoSendSignatureOOB()" type="button"  name="requestTac" id="requestTac" tabindex="4" >-->
                            <button type="button" name="requestTac" id="requestTac" tabindex="4" onclick="geoSendSignatureOOB();
                                    return false;">Add Item</button>
                        </div>
                    </div>

                    <div class="row tac">
                        <label for="transactionAutho"><label for="transactionAutho">Signature OTP Result</label></label>
                        <div>   
                            <%=((String) session.getAttribute("_errorOTPToken"))%>
                        </div>
                    </div>       
                    <%
//                        int type = (Integer) request.getSession().getAttribute("type");
                        if (flag == true && type == SetAuthType.WEBTOKEN) {
                    %>
                    <div class="image" style="text-align:center">
                        <script> var urlGetWTUser2FA = ".././GetWebTokenUser.jsp";</script>


                        <img id="showimageWebToken" 
                             src="../AxisBankToken.png"/>

                        <style type="text/css">
                            .image {
                                position:relative;
                            }
                            <% if (bIE7 == true) { %>

                            .image .text {
                                position:absolute;
                                top:60px;
                                left:120px;

                            }

                            <% } else { %>
                            .image .text {
                                position:absolute;
                                top:40px;
                                left:350px;
                            }
                            <% } %>
                        </style>

                        <div class="text">
                            <h3>
                                <p id="Tz">ENTER PIN</p>
                            </h3>
                        </div>
                        <div style ="display:none">
                            X:<p id="Tx"></p>
                            Y:<p id="Ty"></p>
                            Digit:<p id="DigitSelected" ></p>
                        </div>
                    </div>
                    <%}
                        }%>



                    <div class="row tac">
                        <label for="transactionAutho"><label for="transactionAutho">Transaction Authorization Code</label></label>
                        <div>		
                            <input size="8" maxlength="8" type="password" name="sotp" id="sotp">


                        </div>
                    </div>
                    <div class="row buttons">
                        <div>
                            <input value="Back" class="button_blue" type="submit" id="clear" name="back" tabindex="3">


                            <input value="Confirm" class="button_red default" type="submit" id="cfm" name="insert" tabindex="2"><img id="busy" src="busy.gif">

                        </div>
                    </div>
                    <div style="display: none;"><input type="hidden" name="_sourcePage" value="tBiqLs0iUtr-Q8DOXz10QeZa6fJHNqj42ZQC12ENHPR4YGLh1XPzgGRDYQhSZ8iIDcypq5A2vyk="><input value="F" type="hidden" name="paymentType"><input value="100002050" type="hidden" name="beneficiaryBank"><input value="641990020014608" type="hidden" name="fromAccountNo"><input value="false" type="hidden" name="registeredBeneAcc"><input value="" type="hidden" name="myBeneAccountNo"><input value="" type="hidden" name="numberOfTransfer"><input value="2014-11-05" type="hidden" name="firstProcessDate"><input value="false" type="hidden" name="beneIdValidation"><input value="" type="hidden" name="clearValues"><input value="" type="hidden" name="beneIdType"><input value="vikram sareen" type="hidden" name="beneficiaryName"><input value="false" type="hidden" name="repeatOption"><input value="Fund Transfer" type="hidden" name="remarks"><input value="" type="hidden" name="beneIdNo"><input value="10RMTransferForDemo" type="hidden" name="paymentDescription"><input value="100002050" type="hidden" name="beneBank"><input value="10" type="hidden" name="amount"><input value="" type="hidden" name="__token"><input value="vikramsareen@gmail.com" type="hidden" name="email"><input value="" type="hidden" name="frequency"><input value="7006608635" type="hidden" name="otherAccount"><input type="hidden" name="__fp" value="-bq4Gu4CAhDS0qSsZNXzqw2oTHXFGH53eb7w8cdF-oSqeHg8LTTfO1upZnCEqeUwbi5Z-r3u3PUStUe-x4Wf2j5Qhz6fj7lwTGX4q_3-AveZr3pFLqzlSXoKWPhJyQ80DSV5DoJIboe0UIMW9vCZEgj4UHOLPfUUHFpPdI2-tomHR_Ia06xPTdZSCijDfVRZ3CApZjmejRBtyeCvcMG3tvCbE9gfTYeHq1VA01DuqKOLnbjJfe0uQFJs0ovow5P0yFi2Y-_vhjcEQqSMZxI2UaHZwaKklI_1YB0sM_z9YpQx0HtB331pO5Idx_ViZbi3QJHdiAVgDGmKdcdEAhVeLPVpau5mmGRrrTzUr0digVshJusMcM0aFZfbsYt95GXO9sEnOpwKKTslO8KGzFbFojgTrjC5BVBPV5--qb6H96BDqtPsz5skCA=="></div></form>
            </div>

            <!-- start #footer -->
            <div id="footer">
                <div class="compatibleView">Best viewed on Internet Explorer 7 &amp; above, Firefox and Chrome</div>
                <div class="footerlink"><a href="http://#/About-Us/About-the-Website/Privacy-Policy.aspx" target="_blank">Privacy Policy</a> | <a href="http://#/About-Us/About-the-Website/Client-Charter.aspx" target="_blank">Client Charter</a> | <a href="http://#/ABMB/media/MyLibrary/Shared/Files/Customer-Service-Charter.pdf" target="_blank">Customer Service Charter</a> | <a href="http://#/ABMB/media/MyLibrary/ABMB/PDF/aop_tnc.pdf" target="_blank">Terms &amp; Conditions</a></div>
                <div class="copyright">Copyright © 2014 MyBank (Demo site) </div>
            </div>
            <!-- end #footer -->
        </div>

        <script type="text/javascript">
            $('#container').on('click', 'a[href*=".do"]', function() {
                if (this.target == null || this.target == "") {
                    var loadercontent = '<div id="spinner"><img src="/personal/images/ib/ajax-loader.gif" alt="Loading" width="128" height="15" /></div>';
                    setTimeout(function() {
                        $('#main').html(loadercontent);
                    }, 500);
                }
            });

            $('#sidebar').on('click', 'a.expand', function(e) {
                e.preventDefault();
                window.location = $(this).siblings('.div_submenu').find('a').get(0).href;
            });
            var regImage = document.getElementById("showimageWebToken");
            $('#showimageWebToken').click(function(e) {
                var ImgPos = FindPosition(regImage);
                var relX = e.pageX - ImgPos[0];
                var relY = e.pageY - ImgPos[1];
                GetUserPINForWebToken(relX, relY, <%=type%>);
            });
            function fireWhenReady() {
                if (typeof GetWTUser2FA1 != 'undefined') {
                    GetWTUser2FA1();
                }
                else {
                    setTimeout(fireWhenReady, 50);
                }
            }


            $(document).ready(fireWhenReady);
        </script>


    </body></html>