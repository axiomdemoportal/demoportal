<%@page import="com.mollatech.axiom.v2.core.rss.RssUserCerdentials"%>
<%@ page import="java.text.SimpleDateFormat" %>

<%
    String strContextPath = this.getServletContext().getContextPath();
    
    String status = (String) session.getAttribute("_apOprAuth");
//    String status ="yes";      //(String) session.getAttribute("_apOprAuth");
    if (status == null || status.compareTo("yes") != 0) {
        String nextJSP = "./index.jsp";
//        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
//        dispatcher.forward(request, response);
        response.sendRedirect("index.jsp");

        return;
    }
    RssUserCerdentials rssUserObj = (RssUserCerdentials) session.getAttribute("_rssUserCerdentials");
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="google-translate-customization" content="26e4d928eae0659b-61f8bcc48ec8af9a-g74ee54aac640948a-11"></meta>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="./assets/css/bootstrap.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 60px;
                padding-bottom: 40px;
            }
        </style>
        <link href="./assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="../assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="./assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="./assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="./assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="./assets/ico/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="./assets/ico/favicon.png">
        <link rel="stylesheet" href="./assets/css/datepicker.css">
        <link rel="stylesheet" href="./assets/css/jquery.sidr.dark.css">

        <script src="./assets/js/jquery.js"></script>
        <script src="./assets/js/bootstrap-transition.js"></script>
        <script src="./assets/js/bootstrap-alert.js"></script>
        <script src="./assets/js/bootstrap-modal.js"></script>
        <script src="./assets/js/bootstrap-dropdown.js"></script>
        <script src="./assets/js/bootstrap-scrollspy.js"></script>
        <script src="./assets/js/bootstrap-tab.js"></script>
        <script src="./assets/js/bootstrap-tooltip.js"></script>
        <script src="./assets/js/bootstrap-popover.js"></script>
        <script src="./assets/js/bootstrap-button.js"></script>
        <script src="./assets/js/bootstrap-collapse.js"></script>
        <script src="./assets/js/bootstrap-carousel.js"></script>
        <script src="./assets/js/bootstrap-typeahead.js"></script>
        <script src="./assets/js/bootstrap-datepicker.js"></script>


        <script src="./assets/js/json_sans_eval.js"></script>
        <script src="./assets/js/bootbox.min.js"></script>
        <script src="./assets/js/channels.js"></script>
        <script src="./assets/js/operators.js"></script>

        <script src="./assets/js/jquery.sidr.min.js"></script>


    </head>

    <body>

        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="brand" href="validation.jsp"><%=strContextPath%></a> 
                     <div class="pull-right">
                        <ul class="nav pull-right">
                            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome, <%=rssUserObj.getRssUser().getUserName()%><b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="signout.jsp"><i class="icon-off"></i>Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                   
                </div>

            </div>

        </div>
        
         


        <script>
            $(document).ready(function() {
                $('#left-menu').sidr({
                    name: 'sidr-left',
                    side: 'left' // By default
                });

            });
            $(document).ready(function() {
                $('#simple-menu').sidr();
            });
        </script>

