function strcmpUsers(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4Users(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}

function RefreshUsersList() {
    window.location.href = "./users.jsp";
}


function OperatorAudits(_userid, _duration) {

    var s = './getuseraudits?_oprid=' + encodeURIComponent(_userid) + "&_duration=" + _duration + "&_format=pdf";
    window.location.href = s;
    return false;

}



function adduser() {

    $('#addUserButton').attr("disabled", true);


    var s = './adduser';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#addUserForm").serialize(),
        success: function(data) {
            if (strcmpUsers(data._result, "error") == 0) {
                $('#addUser-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#addUserButton').attr("disabled", false);
                //                $('#_Name').val("");
                //                $('#_Email').val("");
                //                $('#_Phone').val("");
            }
            else if (strcmpUsers(data._result, "success") == 0) {
                $('#addUser-result').html("<span><font color=blue>" + data._message + "</font></span>");
                $('#addUserButton').attr("disabled", false);
                //window.setTimeout(RefreshUsersList, 2000);
                $('#_Name').val("");
                $('#_Email').val("");
                $('#_Phone').val("");
            }
        }
    });
}

function edituser() {
     var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var s = './edituser';
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editUserForm").serialize(),
        success: function(data) {
            pleaseWaitDiv.modal('hide');
            if (strcmpUsers(data._result, "error") == 0) {
                $('#editUser-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#_NameE').val("");
                $('#_EmailE').val("");
                $('#_PhoneE').val("");
            }
            else if (strcmpUsers(data._result, "success") == 0) {
                $('#editUser-result').html("<span><font color=blue>" + data._message + "</font></span>");
                $('#editUserButton').attr("disabled", true);
                 $('#_NameE').val("");
                $('#_EmailE').val("");
                $('#_PhoneE').val("");
                
            }
        }
    });
}

function changeuserstatus(status, userid, uidiv) {

    var s = './changeuserstatus?_status=' + status + '&_userid=' + encodeURIComponent(userid);
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpUsers(data._result, "error") == 0) {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpUsers(data._result, "success") == 0) {
                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                //                window.setTimeout(RefreshUsersList, 2000);
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);

            }
        }
    });
}

function changeunlockpassword(status, userid) {

    var s = './unlockuserpassword?_status=' + status + '&_userid=' + encodeURIComponent(userid);
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpUsers(data._result, "error") == 0) {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpUsers(data._result, "success") == 0) {
                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function removeUser(userid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './removeuser?_userid=' + encodeURIComponent(userid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpUsers(data._result, "success") == 0) {
                        Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshUsersList, 2000);
                    } else {
                        Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}



function resendpassword(userid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './resenduserpassword?_userid=' + encodeURIComponent(userid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpUsers(data._result, "success") == 0) {
                        Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                    } else {
                        Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}


function unlockpassword(userid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './unlockuserpassword?_userid=' + encodeURIComponent(userid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpUsers(data._result, "success") == 0) {
                        Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                    } else {
                        Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}
function resetpassword(userid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './resetuserpassword?_userid=' + encodeURIComponent(userid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpUsers(data._result, "error") == 0) {
                        Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpUsers(data._result, "success") == 0) {
                        Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}
function resetandsendpassword(userid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './setandresenduserpassword?_userid=' + encodeURIComponent(userid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpUsers(data._result, "error") == 0) {
                        Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpUsers(data._result, "success") == 0) {
                        Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}

function loadUserDetails(_uid) {
    var s = './loadUser?_uid=' + encodeURIComponent(_uid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpUsers(data._result, "success") == 0) {
                $('#idEditUser').html(data._Name);
                $('#_userIdE').val(data._uid);
                $("#_NameE").val(data._Name);
                $("#_EmailE").val(data._Email);
                $("#_PhoneE").val(data._Phone);
                $('#editUser-result').html("");
                $("#editUser").modal();
            } else {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}

function searchUsers() {
    var val = document.getElementById('_keyword').value;
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');

//    if ( val.length <= 3 ){
//        Alert4Users("Keyword should be meaningful and/or more than 3 characters");
//        return;
//    }

    if (val.length < 1) {
        //Alert4Tokens("Keyword should be meaningful and/or more than 3 characters");
        Alert4Users("Search keyword cannot be blank!!!");
        return;
    }

    var s = './userstable.jsp?_searchtext=' + encodeURIComponent(val);
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#users_table_main').html(data);
            pleaseWaitDiv.modal('hide');
        }
    });
}


function UsersAudits(_userid, _duration) {
    var s = './getuseraudits?_userid=' + encodeURIComponent(_userid) + "&_duration=" + _duration + "&_format=pdf";
    window.location.href = s;
    return false;
}

function changeunlockpassword(status, userid, uidiv) {

    var s = './unlockuserpassword?_status=' + status + '&_userid=' + encodeURIComponent(userid);
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpUsers(data._result, "error") == 0) {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpUsers(data._result, "success") == 0) {
                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}


function passwordAudits(_userid, _itemtype, _duration) {
    var s = './getuseraudits?_userid=' + encodeURIComponent(_userid) + "&_itemType=" + _itemtype + "&_duration=" + _duration + "&_format=pdf";
    window.location.href = s;
    return false;

}

function ChangeStatusType(value) {
    //1 for enabled
    //0 for disabled
    if (value === 1) {
        $('#_changeStatus').val("1");
        $('#_change-status-Axiom').html("Active");
    } else if (value === 0) {
        $('#_changeStatus').val("0");
        $('#_change-status-Axiom').html("Suspended");
    } else if (value === -1) {
        $('#_changeStatus').val("-1");
        $('#_change-status-Axiom').html("Locked");
    } else if (value === -99) {
        $('#_changeStatus').val("-99");
        $('#_change-status-Axiom').html("Removed");
    } else if (value === 99) {
        $('#_changeStatus').val("99");
        $('#_change-status-Axiom').html("All");
    }
}

//function searchUserReport() {
//    var val = document.getElementById('_searchtext').value;
//    var val1 = document.getElementById('_changeStatus').value;
// 
//
//    var s = './usersreportstable.jsp?_searchtext=' + val +"&_status="+val1;
//    $.ajax({
//        type: 'GET',
//        url: s,
//        success: function(data) {
//            $('#search_user_main').html(data);
//        }
//    });
//}


//function UserReportDownload(){
//     var val = document.getElementById('_searchtext').value;
//    var val1 = document.getElementById('_changeStatus').value;
//    var s = './userreportdown?_strSearch='+val +"&_status="+val1;
//    window.location.href = s;
//    return false;
//}


function UserReportDonutChart(val, val1) {
    var s = './userdonut?_searchtext=' + val + "&_status=" + val1;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;

}
function UserReportBarChart(val, val1) {
    var s = './userbarchart?_searchtext=' + val + "&_status=" + val1;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;

}

function searchUserReport() {

    var valK = document.getElementById('_searchtext').value;
    

    if (valK.length <= 3) {
        Alert4Users("Keyword should be meaningful and/or more than 3 characters");
        return;
    }

    var val = document.getElementById('_searchtext').value;
    var val1 = document.getElementById('_changeStatus').value;
    
    var s = './usersreportstable.jsp?_searchtext=' + val + "&_status=" + val1;
    
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#licenses_data_table').html(data);
            //$("#UserReportgraph").empty();
            //$("#UserReportgraph1").empty();


            var day_data = UserReportDonutChart(val, val1);


            Morris.Donut({
                element: 'UserReportgraph',
                data: day_data

            });

            var day_data = UserReportBarChart(val, val1);
            Morris.Bar({
                element: 'UserReportgraph1',
                data: day_data,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {

                        return '#0066CC';
                    }
                    else {

                        return '#0066CC';
                    }
                }
            });
        
        }
    });






}

function userReportCSV() {
    var val = document.getElementById('_searchtext').value;
    var val1 = document.getElementById('_changeStatus').value;
    var s = './userreportdown?_strSearch=' + val + "&_status=" + val1 + "&_reporttype=" + 1;
    window.location.href = s;
    return false;
}

function userReportPDF() {
    var val = document.getElementById('_searchtext').value;
    var val1 = document.getElementById('_changeStatus').value;
    var s = './userreportdown?_strSearch=' + val + "&_status=" + val1 + "&_reporttype=" + 0;
    window.location.href = s;
    return false;
}


function removemobiletrustUser(userid) {
    bootbox.confirm("<h2><font color=red>Mobile Trust includes Mobile OTP,Mobile PKI, Digital Certificate,Device Profile and Geo Tracking information. All will be removed. Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {

            bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
                if (result == false) {
                } else {
                    var s = './eraseMobileTrustCredential?_userid=' + encodeURIComponent(userid);
                    $.ajax({
                        type: 'GET',
                        url: s,
                        dataType: 'json',
                        success: function(data) {
                            if (strcmpUsers(data._result, "success") == 0) {
                                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                                window.setTimeout(RefreshUsersList, 2000);
                            } else {
                                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                            }
                        }
                    });
                }
            });
        }
    });
}

//var myApp;
//myApp = myApp || (function() {
//    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
//    return {
//        showPleaseWait: function() {
//            pleaseWaitDiv.modal();
//        },
//        hidePleaseWait: function() {
//            pleaseWaitDiv.modal('hide');
//        },
//    };
//})(); 
//
//myApp.showPleaseWait();