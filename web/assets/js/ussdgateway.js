function strcmpUSSD(a, b)
{
    return (a<b?-1:(a>b?1:0));
}


function Alert4USSDSetting(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
    //end here
    }
    });
}


function ChangeActiveStatusUSSD(perference,value) {
    if ( perference == 1) {
        if ( value == 1) {
            $('#_statusUSSD').val("1");
            $('#_status-primary-ussd').html("Active");
        } else {
            $('#_statusUSSD').val("0");
            $('#_status-primary-ussd').html("Suspended");
        }
    } else if ( perference == 2) {
        if ( value == 1) {
            $('#_statusUSSDS').val("1");
            $('#_status-secondary-ussd').html("Active");
        } else {
            $('#_statusUSSDS').val("0");
            $('#_status-secondary-ussd').html("Suspended");
        }
    }
}

function ChangeFailOverUSSD(value) {
    //1 for enabled
    //0 for disabled
    if ( value == 1) {
        $('#_autofailoverUSSD').val("1");
        $('#_autofailover-primary-ussd').html("Enabled");
    } else {
        $('#_autofailoverUSSD').val("0");
        $('#_autofailover-primary-ussd').html("Disabled");
    }
}
function ChangeRetryUSSD(preference,count) {
    //1 for enabled
    //0 for disabled
    if ( preference == 1) {
        if ( count == 2) {
            $('#_retriesUSSD').val("2");
            $('#_retries-primary-ussd').html("2 retries");
        } else if ( count == 3) {
            $('#_retriesUSSD').val("3");
            $('#_retries-primary-ussd').html("3 retries");
        }
        else if ( count == 5) {
            $('#_retriesUSSD').val("5");
            $('#_retries-primary-ussd').html("5 retries");
        }
    } else if ( preference == 2) {
        if ( count == 2) {
            $('#_retriesUSSDS').val("2");
            $('#_retries-secondary-ussd').html("2 retries");
        } else if ( count == 3) {
            $('#_retriesUSSDS').val("3");
            $('#_retries-secondary-ussd').html("3 retries");
        }
        else if ( count == 5) {
            $('#_retriesUSSDS').val("5");
            $('#_retries-secondary-ussd').html("5 retries");
        }
    }
}
function ChangeRetryDurationUSSD(perference,duration) {
    //1 for enabled
    //0 for disabled
    if ( perference == 1) {
        if ( duration == 10) {
            $('#_retrydurationUSSD').val("10");
            $('#_retryduration-primary-ussd').html("10 seconds");
        } else if ( duration == 30) {
            $('#_retrydurationUSSD').val("30");
            $('#_retryduration-primary-ussd').html("30 seconds");
        }
        else if ( duration == 60) {
            $('#_retrydurationUSSD').val("60");
            $('#_retryduration-primary-ussd').html("60 seconds");
        }
    } else if ( perference == 2) {
        if ( duration == 10) {
            $('#_retrydurationUSSDS').val("10");
            $('#_retryduration-secondary-ussd').html("10 seconds");
        } else if ( duration == 30) {
            $('#_retrydurationS').val("30");
            $('#_retryduration-secondary-ussd').html("30 seconds");
        }
        else if ( duration == 60) {
            $('#_retrydurationUSSDS').val("60");
            $('#_retryduration-secondary-ussd').html("60 seconds");
        }
    }
}



function ussdprimary(){
    var s = './loadussdsettings?_preference=1';

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            //$('#_status').val(data._status);
            $('#_ipUSSD').val(data._ip);
            $('#_portUSSD').val(data._port);
            $('#_userIdUSSD').val(data._userId);
            $('#_passwordUSSD').val(data._password);
            $('#_phoneNumberUSSD').val(data._phoneNumber);
            
            if (data._logConfirmation == true) {
                //alert("data._logConfirmation true");
                $('#_logConfirmationUSSD').attr("checked", true);
            //$('#_logConfirmation').attr("value", "true");
                
            }else{
                //alert("data._logConfirmation false");
                $('#_logConfirmationUSSD').attr("checked", false);
            //$('#_logConfirmation').attr("value", "false");
            }
            $('#_classNameUSSD').val(data._className);
            $('#_reserve1USSD').val(data._reserve1);
            $('#_reserve2USSD').val(data._reserve2);
            $('#_reserve3USSD').val(data._reserve3);
            $('#_autofailoverUSSD').val(data._autofailover);
            $('#_retriesUSSD').val(data._retries);
            $('#_retrydurationUSSD').val(data._retryduration);

            //alert(data._status);

            if (data._status == 1)
                ChangeActiveStatusUSSD(1,1);
            else
                ChangeActiveStatusUSSD(1,0);

            if (data._autofailover == 1)
                ChangeFailOverUSSD(1);
            else
                ChangeFailOverUSSD(0);

            if (data._retryduration == 10 )
                ChangeRetryDurationUSSD(1,10);
            else if (data._retryduration == 30)
                ChangeRetryDurationUSSD(1,30);
            else if (data._retryduration == 60)
                ChangeRetryDurationUSSD(1,60);
            else
                ChangeRetryDurationUSSD(1,10);

            if (data._retries == 2)
                ChangeRetryUSSD(1,2);
            else if (data._retries == 3)
                ChangeRetryUSSD(1,3);
            else if (data._retries == 5)
                ChangeRetryUSSD(1,5);
            else
                ChangeRetryUSSD(1,2);
        }
    });
}

function ussdsecondary(){
    var s = './loadussdsettings?_preference=2';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            //$('#_statusS').val(data._statusS);
            $('#_ipUSSDS').val(data._ipS);
            $('#_portUSSDS').val(data._portS);
            $('#_userIdUSSDS').val(data._userIdS);
            $('#_passwordUSSDS').val(data._passwordS);
            $('#_phoneNumberUSSDS').val(data._phoneNumberS);

            if (data._logConfirmationS == true) {
                $('#_logConfirmationUSSDS').attr("checked", true);
            }else{
                $('#_logConfirmationUSSDS').attr("checked", false);
            }
            $('#_classNameUSSDS').val(data._classNameS);
            $('#_reserve1USSDS').val(data._reserve1S);
            $('#_reserve2USSDS').val(data._reserve2S);
            $('#_reserve3USSDS').val(data._reserve3S);
            $('#_retriesUSSDS').val(data._retriesS);
            $('#_retrydurationUSSDS').val(data._retrydurationS);

            if (data._statusS == 1)
                ChangeActiveStatusUSSD(2,1);
            else
                ChangeActiveStatusUSSD(2,0);

            if (data._retrydurationS == 10)
                ChangeRetryDurationUSSD(2,10);
            else if (data._retrydurationS == 30)
                ChangeRetryDurationUSSD(2,30);
            else if (data._retrydurationS == 60)
                ChangeRetryDurationUSSD(2,60);
            else
                ChangeRetryDurationUSSD(2,10);

            if (data._retriesS == 2)
                ChangeRetryUSSD(2,2);
            else if (data._retriesS == 3)
                ChangeRetryUSSD(2,3);
            else if (data._retriesS == 5)
                ChangeRetryUSSD(2,5);
            else
                ChangeRetryUSSD(2,2);
        }
    }
    );
}

function LoadUSSDSetting(type){
    if (type == 1) {
        ussdprimary();
    } else if (type == 2 ){
        ussdsecondary();
    }
}


function editUSSDprimary(){
    var s = './editussdsettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#ussdprimaryform").serialize(),
        success: function(data) {
            if ( strcmpUSSD(data._result,"error") == 0 ) {
                $('#save-ussd-gateway-primary-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4USSDSetting("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpUSSD(data._result,"success") == 0 ) {
//                $('#save-ussd-gateway-primary-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4USSDSetting("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function editUSSDsecondary(){
    var s = './editussdsettings';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#ussdsecondaryform").serialize(),
        success: function(data) {
            if ( strcmpUSSD(data._result,"error") == 0 ) {
                $('#save-ussd-gateway-secondary-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4USSDSetting("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpUSSD(data._result,"success") == 0 ) {
//                $('#save-ussd-gateway-secondary-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4USSDSetting("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function EditUSSDSetting(type){
    if (type == 1) {
        editUSSDprimary();
    } else if (type == 2 ){
        editUSSDsecondary();
    }
}

function LoadTestUSSDConnectionUI(type){
    if ( type == 1 )
        $("#testUSSDPrimary").modal();
    else
        $("#testUSSDSecondary").modal();
}

function testconnectionUSSDprimary(){
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var s = './testconnection';
    pleaseWaitDiv.modal();

    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#testUSSDPrimaryForm").serialize(),
        success: function(data) {
            if ( strcmpUSSD(data._result,"error") == 0 ) {
                $('#test-ussd-primary-configuration-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }
            else if ( strcmpUSSD(data._result,"success") == 0 ) {
                $('#test-ussd-primary-configuration-result').html("<span><font color=blue>" + data._message + "</font></span>");
            }
            pleaseWaitDiv.modal('hide');
        }
    });
}

function testconnectionUSSDsecondary(){
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var s = './testconnection';
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#testUSSDSecondaryForm").serialize(),
        success: function(data) {
            if ( strcmpUSSD(data._result,"error") == 0 ) {
                $('#test-ussd-secondary-configuration-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }
            else if ( strcmpUSSD(data._result,"success") == 0 ) {
                $('#test-ussd-secondary-configuration-result').html("<span><font color=blue>" + data._message + "</font></span>");
            }
            pleaseWaitDiv.modal('hide');
        }
    });
}


function TestUSSDConnection(type){
    if (type == 1) {
        testconnectionUSSDprimary();
    } else if (type == 2 ){
        testconnectionUSSDsecondary();
    }
}

