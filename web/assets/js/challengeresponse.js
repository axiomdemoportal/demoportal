function strcmpChallenge(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4Challege(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
        //end here
        }
    });
}

function RefreshUsersList() {
    window.location.href = "./ChallengeResponse.jsp";
}


function changeuserChallengestatus(status, userid, uidiv) {

    var s = './changeChallengestatus?_status=' + status + '&_userid=' + encodeURIComponent(userid);
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpChallenge(data._result, "error") == 0) {
                Alert4Challege("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strcmpChallenge(data._result, "success") == 0) {
                Alert4Challege("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);

            }
        }
    });
}



function removeChallengeUser(userid) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './removeChallengeuser?_userid=' + encodeURIComponent(userid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpChallenge(data._result, "success") == 0) {
                        Alert4Challege("<span><font color=red>" + data._message + "</font></span>");
                        window.setTimeout(RefreshUsersList, 2000);
                    } else {
                        Alert4Challege("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}








function loadQuestionsDetails(_uid) {
          
         $("#_userid").val(_uid);
         $("#listquestions").modal();
           
}

function searchChallengeUsers() {
    var val = document.getElementById('_keyword').value;
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    
    if ( val.length <= 3 ){
        Alert4Challege("Keyword should be meaningful and/or more than 3 characters");
        return;
    }
    var s = './ChallengeResponsetable.jsp?_searchtext=' + val;
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#challenge_table_main').html(data);
            pleaseWaitDiv.modal('hide');
        }
    });
}






function challengeAudits(_userid,_itemtype,_duration){
    var s = './getChallengeaudits?_userid='+encodeURIComponent(_userid) +"&_itemType="+_itemtype+"&_duration="+_duration + "&_format=pdf";
    window.location.href = s;
    return false;

}


