function strcmpInteractions(a, b)
{
    return (a<b?-1:(a>b?1:0));
}


function Alert4InteractionsSetting(msg) {
    bootbox.alert("<h2>"+ msg+"</h2>", function(result) {
        if (result == false) {
        } else {
        //end here
        }
    });
}


function saveNewInteraction(){
    var s = './createSurvey';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#createInteractionForm").serialize(),
        success: function(data) {
            if ( strcmpInteractions(data._result,"error") == 0 ) {
                $('#save-interactions-settings-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4InteractionsSetting("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpInteractions(data._result,"success") == 0 ) {
                $('#ave-interactions-settings-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4InteractionsSetting("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function saveEditInteraction(){
    var s = './editSurvey';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editInteractionForm").serialize(),
        success: function(data) {
            if ( strcmpInteractions(data._result,"error") == 0 ) {
                $('#save-interactions-edit-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4InteractionsSetting("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpInteractions(data._result,"success") == 0 ) {
                $('#ave-interactions-edit-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4InteractionsSetting("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}


function runInteraction(){
    var s = './executeSurvey';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#RunInteractionForm").serialize(),
        success: function(data) {
            if ( strcmpInteractions(data._result,"error") == 0 ) {
                $('#runInteractionButton-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4InteractionsSetting("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpInteractions(data._result,"success") == 0 ) {
                $('#runInteractionButton-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4InteractionsSetting("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}

function RefreshInteractionsList() {
    window.location.href = "./InteractionsList.jsp";    
}

//var callerid = $('#_CallerId').val();
//var funcName = "RefreshPushmappersList2("+callerid+")";
//window.setTimeout(funcName, 2000);



function deleteSurveyJS(_irid){
    bootbox.confirm("<h2> Are you sure you wish to remove this interaction?"+"</h2>", function(result) {
        if (result == false) {
        } else {
            var s = './deleteSurvey?_irid='+_irid;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if ( strcmpInteractions(data._result,"error") == 0 ) {
                        $('#save-interactions-edit-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                        Alert4InteractionsSetting("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if ( strcmpInteractions(data._result,"success") == 0 ) {
                        $('#ave-interactions-edit-result').html("<span><font color=blue>" + data._message + "</font></span>");
                        Alert4InteractionsSetting("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshInteractionsList, 2000);
                    }
                }
            });
        }
    });
}

function stopSurveyJS(_irid){
    bootbox.confirm("<h2> Are you sure you wish to stop interaction?"+"</h2>", function(result) {
        if (result == false) {
        } else {
            var s = './changestatusSurvey?_irid='+_irid + "&_status=-1";
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if ( strcmpInteractions(data._result,"error") == 0 ) {
                        //$('#save-interactions-edit-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                        Alert4InteractionsSetting("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if ( strcmpInteractions(data._result,"success") == 0 ) {
                        //$('#ave-interactions-edit-result').html("<span><font color=blue>" + data._message + "</font></span>");
                        Alert4InteractionsSetting("<span><font color=blue>" + data._message + "</font></span>");
                        window.setTimeout(RefreshInteractionsList, 2000);
                    }
                }
            });
        }
    });
}


function InteractionReportTable(val, val1, val2) {

    var s = './InteractionResponseReportTable.jsp?_interactionID=' + val + '&_QNo=' + val2 + '&_interactionExecutionID=' + val1;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {

            if (val2 == 0) {//greetings
              
                var ele = document.getElementById("greetings_data_table");
                ele.style.display = "block";
                $('#greetings_data_table').html(data);
                document.getElementById("SearchResponseButton").style.display = 'none';
                var ele = document.getElementById("HideSearchResponseButton");
                ele.style.display = "block";
            } else if (val2 == 1) {//question1
                var ele = document.getElementById("question1_data_table");
                ele.style.display = "block";
                $('#question1_data_table').html(data);
                document.getElementById("Searchquestion1ResponseButton").style.display = 'none';
                var ele = document.getElementById("Hidequestion1ResponseButton");
                ele.style.display = "block";
            } else if (val2 == 2) {//question2
                var ele = document.getElementById("question2_data_table");
                ele.style.display = "block";
                $('#question2_data_table').html(data);
                document.getElementById("Searchquestion2ResponseButton").style.display = 'none';
                var ele = document.getElementById("Hidequestion2ResponseButton");
                ele.style.display = "block";
            } else if (val2 == 3) {//question3
                var ele = document.getElementById("question3_data_table");
                ele.style.display = "block";
                $('#question3_data_table').html(data);
                document.getElementById("Searchquestion3ResponseButton").style.display = 'none';
                var ele = document.getElementById("Hidequestion3ResponseButton");
                ele.style.display = "block";
            } else if (val2 == 4) {//question4
                var ele = document.getElementById("question4_data_table");
                ele.style.display = "block";
                $('#question4_data_table').html(data);
                document.getElementById("Searchquestion4ResponseButton").style.display = 'none';
                var ele = document.getElementById("Hidequestion4ResponseButton");
                ele.style.display = "block";
            } else if (val2 == 5) {//question4
                var ele = document.getElementById("question5_data_table");
                ele.style.display = "block";
                $('#question5_data_table').html(data);
                document.getElementById("Searchquestion5ResponseButton").style.display = 'none';
                var ele = document.getElementById("Hidequestion5ResponseButton");
                ele.style.display = "block";
            }

        }
    });
}
function HidInteractionReportTable(val) {
    if (val == 0) {
        document.getElementById("greetings_data_table").style.display = 'none';
        var ele = document.getElementById("SearchResponseButton");
        ele.style.display = "block";
    } else if (val == 1) {
        document.getElementById("question1_data_table").style.display = 'none';
        var ele = document.getElementById("Searchquestion1ResponseButton");
        ele.style.display = "block";
    } else if (val == 2) {
        document.getElementById("question2_data_table").style.display = 'none';
        var ele = document.getElementById("Searchquestion2ResponseButton");
        ele.style.display = "block";
    } else if (val == 3) {
        document.getElementById("question3_data_table").style.display = 'none';
        var ele = document.getElementById("Searchquestion3ResponseButton");
        ele.style.display = "block";
    } else if (val == 4) {
        document.getElementById("question4_data_table").style.display = 'none';
        var ele = document.getElementById("Searchquestion4ResponseButton");
        ele.style.display = "block";
    } else if (val == 5) {
        document.getElementById("question5_data_table").style.display = 'none';
        var ele = document.getElementById("Searchquestion5ResponseButton");
        ele.style.display = "block";
    }
}

function HideforGreetings() {
//    document.getElementById("question1").style.display = 'none';
    document.getElementById("question2").style.display = 'none';
    document.getElementById("question3").style.display = 'none';
    document.getElementById("question4").style.display = 'none';
    document.getElementById("question5").style.display = 'none';
    var ele = document.getElementById("greetings");
    ele.style.display = "block";
}


function InteractionReport(val, val3) {

    //q1
    
    var s = './InteractionsReportTable.jsp?_interactionID=' + val + '&_interactionExecutionID=' + val3;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#licenses_data_table').html(data);
            $("#greetingsBarChart").empty();
            $("#greetingsDonutChart").empty();
            var day_data1 = null;
            day_data1 = BarChartInteractionReport(val,val3, 0);

            Morris.Bar({
                element: 'greetingsBarChart',
                data: day_data1,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {
                        return '#0066CC';
                    }
                    else {
                        return '#0066CC';
                    }
                }
            });
            var day_data = null;
            day_data = DonutChartInteractionReport(val,val3, 0);
            Morris.Donut({
                element: 'greetingsDonutChart',
                data: day_data
            });
//q1
//$('#InteractionData').html(data);
            $("#question1BarChart").empty();
            $("#question1DonutChart").empty();
            var day_data1 = null;
            day_data1 = BarChartInteractionReport(val,val3, 1);

            Morris.Bar({
                element: 'question1BarChart',
                data: day_data1,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {
                        return '#0066CC';
                    }
                    else {
                        return '#0066CC';
                    }
                }
            });
            var day_data = null;
            day_data = DonutChartInteractionReport(val,val3, 1);
            Morris.Donut({
                element: 'question1DonutChart',
                data: day_data
            });

//    q2
            $("#question2BarChart").empty();
            $("#question2DonutChart").empty();
            var day_data2 = null;
            day_data2 = BarChartInteractionReport(val,val3, 2);
            Morris.Bar({
                element: 'question2BarChart',
                data: day_data2,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {
                        return '#0066CC';
                    }
                    else {
                        return '#0066CC';
                    }
                }
            });
            var day_data2 = null;
            day_data2 = DonutChartInteractionReport(val,val3, 2);
            Morris.Donut({
                element: 'question2DonutChart',
                data: day_data2
            });
//
            //q3
            $("#question3BarChart").empty();
            $("#question3DonutChart").empty();
            var day_data3 = null;
            day_data3 = BarChartInteractionReport(val,val3, 3);
            Morris.Bar({
                element: 'question3BarChart',
                data: day_data3,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {
                        return '#0066CC';
                    }
                    else {
                        return '#0066CC';
                    }
                }
            });
            var day_data3 = null;
            day_data3 = DonutChartInteractionReport(val,val3, 3);
            Morris.Donut({
                element: 'question3DonutChart',
                data: day_data3
            });

            //q4
            $("#question4BarChart").empty();
            $("#question4DonutChart").empty();
            var day_data4 = null;
            day_data4 = BarChartInteractionReport(val,val3, 4);
            Morris.Bar({
                element: 'question4BarChart',
                data: day_data4,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {
                        return '#0066CC';
                    }
                    else {
                        return '#0066CC';
                    }
                }
            });
            var day_data4 = null;
            day_data4 = DonutChartInteractionReport(val,val3, 4);
            Morris.Donut({
                element: 'question4DonutChart',
                data: day_data4
            });

            //q5
            $("#question5BarChart").empty();
            $("#question5DonutChart").empty();
            var day_data5 = null;
            day_data5 = BarChartInteractionReport(val,val3, 5);
            Morris.Bar({
                element: 'question5BarChart',
                data: day_data5,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {
                        return '#0066CC';
                    }
                    else {
                        return '#0066CC';
                    }
                }
            });
            var day_data5 = null;
            day_data5 = DonutChartInteractionReport(val,val3, 5);
            Morris.Donut({
                element: 'question5DonutChart',
                data: day_data5
            });
            
           //dropOut
            $("#dropoutDonutChart").empty();
            $("#dropoutBarChart").empty();
            var day_data6 = null;
            day_data6 = BarChartInteractionReport(val,val3, 999);//999 for dropout
            Morris.Bar({
                element: 'dropoutBarChart',
                data: day_data6,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {
                        return '#0066CC';
                    }
                    else {
                        return '#0066CC';
                    }
                }
            });
            var day_data6 = null;
            day_data6 = DonutChartInteractionReport(val,val3, 999);//999 for dropout
            Morris.Donut({
                element: 'dropoutDonutChart',
                data: day_data6
            });
//            var ele = document.getElementById("TABS");
//            ele.style.display = "block";
        }});
    
}

function BarChartInteractionReport(val,val3, val1) {
//    alert(val)
    var s = './interactionBarChart?_interactionID=' + val + "&_questionNo=" + val1 +"&_interactionExecutionID="+val3;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}
function DonutChartInteractionReport(val,val3, val1) {
    var s = './interactionDonutChart?_interactionID=' + val + "&_questionNo=" + val1+"&_interactionExecutionID="+val3;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}


function InteractionUserReport() {
    var node = document.getElementById("_interaction2SearchU");
    var index = node.selectedIndex;
    var val = node.options[index].value;
    var val2 = document.getElementById('_strSearch').value;

    var s = './InteractionUserReportTable.jsp?_interactionUID=' + val + '&_strSearch=' + val2;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {

            $('#InteractionUserData').html(data);

        }});
}


function interactionReportCSV(val,val1,val2) {
    var s = './InteractionReportDownload?_reportformat=' + 1 + "&_interactionID=" + val + "&_QNo=" + val2 + "&_interactionExecutionID=" + val1;
    window.location.href = s;
    return false;
}

function interactionReportPDF(val,val1,val2) {
  var s = './InteractionReportDownload?_reportformat=' + 0 + "&_interactionID=" + val + "&_QNo=" + val2 + "&_interactionExecutionID=" + val1;
    window.location.href = s;
    return false;
}
