function strcmpfxlistener(a, b) {
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function Alert4fxlistener(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
//end here
        }
    });
}

//
//$("#_pushmessageSearch").change(function() {
//	$("#_pushmappersSearch").load("getter.php?choice=" + $("#first-choice").val());
//});

$(document).ready(function() {
    $('#_pushmessageSearch').change(function() {
        var selectedValue = $(this).val();
        var servletUrl = 'pushmapperselect?value=' + selectedValue;

        $.getJSON(servletUrl, function(options) {
            var _pushmappersSearch = $('#_pushmappersSearch');
            $('>option', _pushmappersSearch).remove(); // Clean old options first.
            if (options) {
                $.each(options, function(key, value) {
                    _pushmappersSearch.append($('<option/>').val(key).text(value));
                });
            } else {
                _pushmappersSearch.append($('<option/>').text("Please select dropdown1"));
            }
        });
    });
});

function RefreshTokenList() {
    window.location.href = "./FXListenerReports.jsp";
}

function searchFXReport(val) {
    if (val == 1) {
        var val1 = encodeURIComponent(document.getElementById('startdate').value);
        var val2 = encodeURIComponent(document.getElementById('enddate').value);
        var val4 = document.getElementById('_pushmessageSearch').value;
        var val3 = document.getElementById('_pushmappersSearch').value;

        if (document.getElementById('startdate').value.length == 0 || document.getElementById('enddate').value.length == 0) {
            Alert4fxlistener("Date Range is not selected!!!");
            return;
        }

        var s = './FXListenerReportTable.jsp?_startdate=' + val1 + "&_pushmessageSearch=" + val4 + "&_enddate=" + val2 + "&_pushmappersSearch=" + val3;
        $.ajax({
            type: 'GET',
            url: s,
            success: function(data) {

//        $('#users_table_main_MSG').html(data);
                $('#licenses_data_table').html(data);
                $("#licenses_data_table_Social").empty();
                $("#licenses_data_table_push").empty();
                var day_data1 = null;
                day_data1 = BarChartDemo1(val1, val2, val4, val3);
//        day_data1 = BarChartCostReport(val1, val2,val);
                Morris.Bar({
                    element: 'MsgReportgraph1',
                    data: day_data1,
                    xkey: 'label',
                    ykeys: ['value'],
                    labels: ['value'],
                    barColors: function(type) {
                        if (type === 'bar') {

                            return '#0066CC';
                        }
                        else {

                            return '#0066CC';
                        }
                    }
                });
                var day_data = null;
//        day_data = DonutChartCostReport(val1, val2,val);
                day_data = DonutChart1(val1, val2, val4, val3);
                Morris.Donut({
                    element: 'MsgReportgraph',
                    data: day_data
                });
            }});
    }
}

function searchFXRecords() {
    var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
    var val = document.getElementById('_searchtext').value;
    var val3 = document.getElementById('_changeType').value;
    if (document.getElementById('startdate').value.length == 0 || document.getElementById('enddate').value.length == 0) {
        Alert4fxlistener("Date Range is not selected!!!");
        return;
    }


    var s = './FXListenerReportTable.jsp?_startdate=' + val1 + "&_searchtext=" + val + "&_enddate=" + val2 + "&_type=" + val3;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#licenses_data_table').html(data);
            $("#MsgReportgraph").empty();
            $("#MsgReportgraph1").empty();
            //document.getElementById('graph11').html("");
            //document.getElementById('barchartid').html(""); 

            var day_data1 = null;
            day_data1 = BarChartDemo1(val1, val2, val3, val);
            Morris.Bar({
                element: 'MsgReportgraph1',
                data: day_data1,
                xkey: 'label',
                ykeys: ['value'],
                labels: ['value'],
                barColors: function(type) {
                    if (type === 'bar') {

                        return '#0066CC';
                    }
                    else {

                        return '#0066CC';
                    }
                }
            });
            var day_data = null;
            day_data = DonutChart1(val1, val2, val3, val);
            Morris.Donut({
                element: 'MsgReportgraph',
                data: day_data
            });
        }
    });
}

function ChangeResponseType(value) {
//1 for enabled
//0 for disabled
    if (value === 1) {
        $('#_changeType').val("1");
        $('#_change-Type-Axiom').html("SUCCESS");
    } else if (value === -1) {
        $('#_changeType').val("-1");
        $('#_change-Type-Axiom').html("FAILURE");
    }
}




function BarChartDemo1(val1, val2, val3, val) {
    var s = './FXListenerreportbar?_startdate=' + val1 + "&_enddate=" + val2 + "&_pushmessageSearch=" + val3 + "&_pushmappersSearch=" + val;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}

function DonutChart1(val1, val2, val3, val) {
    var s = './FXlListenerreportdonut?_startdate=' + val1 + "&_enddate=" + val2 + "&_pushmessageSearch=" + val3 + "&_pushmappersSearch=" + val;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}

function fxmsgReportCSV() {
    var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
    var val4 = document.getElementById('_pushmessageSearch').value;
    var val3 = document.getElementById('_pushmappersSearch').value;
    var s = './FXListenerreportdown?_startdate=' + val1 + "&_pushmessageSearch=" + val4 + "&_enddate=" + val2 + "&_pushmappersSearch=" + val3 + "&_reporttype=" + 1;
    window.location.href = s;
    return false;
}

function fxmsgReportPDF() {
    var val1 = encodeURIComponent(document.getElementById('startdate').value);
    var val2 = encodeURIComponent(document.getElementById('enddate').value);
    var val4 = document.getElementById('_pushmessageSearch').value;
    var val3 = document.getElementById('_pushmappersSearch').value;
    var s = './FXListenerreportdown?_startdate=' + val1 + "&_pushmessageSearch=" + val4 + "&_enddate=" + val2 + "&_pushmappersSearch=" + val3 + "&_reporttype=" + 0;
    window.location.href = s;
    return false;
}