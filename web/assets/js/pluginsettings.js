function strcmppluginFilter(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function RefreshPluginList() {
    window.location.href = "./pluginsetting.jsp";
}
function Alert4PluginSetting(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}


function SavePluginFilerList(value) {
    $('#SavePluginButton').attr("disabled", true);

    var s = './editpluginsetting?_fileNames='+ value;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#UploadFileForm").serialize(),
        success: function(data) {
            if (strcmppluginFilter(data._result, "error") == 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                Alert4PluginSetting("<span><font color=red>" + data._message + "</font></span>");
                $('#SaveIpFilteringButton').attr("disabled", false);
            }
            else if (strcmppluginFilter(data._result, "success") == 0) {
                //$('#save-ContentFilter-gateway-result').html("<span><font color=blue>" + data._message + "</font></span>");
                Alert4PluginSetting("<span><font color=blue>" + data._message + "</font></span>");
                 window.setTimeout(RefreshPluginList, 2000);

            }
        }
    });
}


//function UploadNow() {
//    $('#SavePluginButton').attr("disabled", true);
//    var s = './uploadplugin';
//    $.ajaxFileUpload({
//        fileElementId: 'licensefile',
//        url: s,
//        //enctype: 'application/x-www-form-urlencoded',
//        dataType: 'json',
//        data: $("#UploadFileForm").serialize(),
//        success: function(data,status) {
//            alert(data.result);
//            alert(data.message);
//
//            if (strcmppluginFilter(data.result, "error") === 0) {
//            }
//            else if (strcmppluginFilter(data.result, "success") === 0) {
//
//              SavePluginFilerList(data.fileName);
//            }
//        },
////        error: function(data, status, e)
////        {
////            alert(e);
////        }
//    });
//}

function UploadNow() {
    $('#SavePluginButton').attr("disabled", true);
    var s = './uploadplugin';
    $.ajaxFileUpload({
        fileElementId: 'licensefile',
        url: s,
        //enctype: 'application/x-www-form-urlencoded',
        dataType: 'json',
        //data: $("#RegisterLicenseFileForm").serialize(),
        success: function(data, status) {

            if (strcmppluginFilter(data.result, "error") == 0) {
                Alert4PluginSetting("<span><font color=red>" + data.message + "</font></span>");
                $('#SavePluginButton').attr("disabled", false);
            }
            else if (strcmppluginFilter(data.result, "success") == 0) {                
                $('#SavePluginButton').attr("disabled", false);
                var input = $("#_fileNames").val();
                input = input + "," + data.fileName;
                //alert(input);
                //$("#_fileNames").val(input);  
                //$("#_fileNames").select2("val", [data.fileName]); 
                var selectedItems = $("#_fileNames").select2("val");
                selectedItems.push(data.fileName);   // I used "WA" here to test.
                $("#_fileNames").select2("val", selectedItems);
            }
        },
        error: function(data, status, e)
        {
            alert(e);
        }
    });
}



