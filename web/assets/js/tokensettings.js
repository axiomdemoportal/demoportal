/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function otptokensetting(){
    
   
   var s = './loadsettings?_type='+7+ '&_preference='+1;
  
    $.ajax({
         type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
           
           $('#_otpAllowed').val(data._otpAllowed);
           $('#_sotpAllowed').val(data._sotpAllowed);
           $('#_p1host').val(data._p1host);
           $('#_otpLengthOOBToken').val(data._otpLengthOOBToken);
           $('#_otpLengthSWToken').val(data._otpLengthSWToken);
           $('#_sotpLengthSWToken').val(data._sotpLengthSWToken);
           $('#_otpLengthHWToken').val(data._otpLengthHWToken);
           
           
        }
    }); 
}
function tokenallocation(){
    
    var s = './loadsettings?_type='+7+ '&_preference='+1;
  
    $.ajax({
         type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            
            
           
            if (data._oOBOTPTokenEnabled == "true") {
                $('#_oOBOTPTokenEnabled').attr("checked", true);
            } else {
                $('#_oOBOTPTokenEnabled').attr("checked", false);
            }
            $('#_oobTokenCount').val(data._oobTokenCount);
            $('#_bOOBOTPTokenUnlimited').val(data._bOOBOTPTokenUnlimited);
            
            
            if (data._sWOTPTokenEnabled == true) {
                $('#_sWOTPTokenEnabled').attr("checked", true);
            } else {
                $('#_sWOTPTokenEnabled').attr("checked", false);
            }
            $('#_swTokenCount').val(data._swTokenCount);
            $('#_bSWOTPTokenUnlimited').val(data._bSWOTPTokenUnlimited);
            
            
            if (data._hWOTPTokenEnabled == true) {
                $('#_hWOTPTokenEnabled').attr("checked", true);
            } else {
                $('#_hWOTPTokenEnabled').attr("checked", false);
            }
            $('#_swTokenCount').val(data._swTokenCount);
            $('#_bSWOTPTokenUnlimited').val(data._bSWOTPTokenUnlimited);
            
            
        }
    }); 
}

function editotptokensetting(){
    var s = './editsettings?_type='+7+ '&_preference='+1;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#primaryform").serialize(),
        success: function(data) {
             if ( strcmp(data._result,"error") == 0 ) {
                $('#save-otp-token-setting-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }
            else if ( strcmp(data._result,"success") == 0 ) {
                $('#save-otp-token-setting-result').html("<span><font color=blue>" + data._message + "</font></span>");
                
            }
        }
    }); 
}

function editallocationtokensetting(){
    var s = './editsettings?_type='+7+ '&_preference='+1;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#secondaryform").serialize(),
        success: function(data) {
            if ( strcmp(data._result,"error") == 0 ) {
                $('#save-allocation-token-setting-result').html("<span><font color=red>" + data._message + "</font></span></small>");
            }
            else if ( strcmp(data._result,"success") == 0 ) {
                $('#save-allocation-token-setting-result').html("<span><font color=blue>" + data._message + "</font></span>");
                
            }
        }
    }); 
}

function UploadTokensFile()
{
  
    $.ajaxFileUpload
    (
    {
        url:'./uploadhwotptokens',
        fileElementId:'fileToUploadHWOTP',
        dataType: 'json',
        success: function (data, status)
        {
            if ( strcmpSettings(data._result,"error") == 0 ) {
               Alert4Settings("<span><font color=blue>" + data._message + "</font></span>");
            //ClearAddTemplateForm();
            }
            else if ( strcmpSettings(data._result,"success") == 0 ) {
                Alert4Settings("<span><font color=blue>" + data._message + "</font></span>");
            //window.setTimeout(RefreshContacts, 3000);
            }
        },
        error: function (data, status, e)
        {
            Alert4Contacts(e);
        }
    }
    )
    return false;
}



