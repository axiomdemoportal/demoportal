/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function strcmpBridge(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function Alert4Bridge(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}

function RefreshBridgeList() {
    window.location.href = "./otptokens.jsp";
}
function ChangeISExempt(value) {

    if (value == 1) {
//            $('#_status').val("1");
        $('#_card-ISEXempt').html("YES");
    } else {
//            $('#_status').val("0");
        $('#_card-ISEXempt').html("NO");
    }

}

function ChangeReporttxForBridge(value) {

    if (value == 1) {
        $('#_txReportType').val("1");
        $('#_txreportType-div').html("CSV");
    } else {
            $('#_txReportType').val("0");
        $('#_txreportType-div').html("PDF");
    }

}

function ChangeISPermanent(value) {

    if (value == 1) {
            $('#_cardISPermanent').val("1");
        $('#_cardISPermanent-div').html("Permanently");
    } else {
            $('#_cardISPermanent').val("0");
        $('#_cardISPermanent-div').html("Disabled");
    }

}
function ChangeISPrimary(value) {

    if (value == 1) {
            $('#_cardISPrimary').val("1");
        $('#_cardISPrimary-div').html("Primary Card");
    } else {
            $('#_cardISPrimary').val("0");
        $('#_cardISPrimary-div').html("Secondary Card");
    }

}
function ChangeReportTypeForBridge(value) {

    if (value == 1) {
            $('#_reportType').val("1");
        $('#_reportType-div').html("CSV");
    } else {
            $('#_reportType').val("0");
        $('#_reportType-div').html("PDF");
    }

}
//function ChangeCardType(value) {
//    
//        if ( value == 1) {
////            $('#_status').val("1");
//            $('#_card-type').html("Master");
//        } else {
////            $('#_status').val("0");
//            $('#_card-type').html("VISA");
//        }
//    
//}


function ChangeSearchTypeBridge(value) {
    if (value === 1) {
        $('#_changeBridgeSearchType').val("1");
        $('#_changeBridgeSearchType-div').html("CardNumber");
    } else if (value === 2) {
        $('#_changeBridgeSearchType').val("2");
        $('#_changeBridgeSearchType-div').html("Name");
    } else if (value === 3) {
        $('#_changeBridgeSearchType').val("3");
        $('#_changeBridgeSearchType-div').html("Bank ID");
    }
}

 
function changesoftStatus(value) {
    if (value == 1) {
        $('#_change-soft-Status').html("ACTIVE");
    } else {
        $('#_change-soft-Status').html("SUSPENDED");
    }
}


function changehardStatus(value) {
    if (value == 1) {
        $('#_change-hard-Status').html("ACTIVE");
    } else {
        $('#_change-hard-Status').html("SUSPENDED");
    }
}

function changeOOBStatus(value) {
    if (value == 1) {
//            $('#_status'val).val("1");
        $('#_change-OOB-Status').html("ACTIVE");
    } else {
//            $('#_status').val("0");
        $('#_change-OOB-Status').html("SUSPENDED");
    }
}

function changeTokenOOB(value) {
    if (value == 1) {
//            $('#_status'val).val("1");
        $('#_change-status').html("SMS TOKEN");
    } else if (value == 2) {
        $('#_change-status').html("VOICE TOKEN");

    } else if (value == 3) {

        $('#_change-status').html("USSD TOKEN");
    } else {
//            $('#_status').val("0");
        $('#_change-status').html("EMAIL TOKEN");
    }
}

function assigntokensoft(value) {
    if (value == 1) {
        $('#_assign-soft-token').html("WEB TOKEN");
    } else if (value == 2) {
        $('#_assign-soft-token').html("MOBILE TOKEN");
    } else {
        $('#_assign-soft-token').html("PC TOKEN");
    }
}

function assigntokenOOB(value) {
    if (value == 1) {
        $('#_assign-status').html("SMS TOKEN");
    } else if (value == 2) {
        $('#_assign-status').html("VOICE TOKEN");
    } else if (value == 3) {
        $('#_assign-status').html("USSD TOKEN");
    } else {
        $('#_assign-status').html("EMAIL TOKEN");
    }
}


//
//function ChangeCardStatus(value) {
//    if (value == 1) {
//        $('#_card-status').html("Active");
//    } else {
//        $('#_card-status').html("Suspend");
//    }
//}
function ChangeCardStatus(value) {
        if ( value == 1) {
            $('#_cardstatus').val("1");
            $('#_cardstatus-div').html("Active");
        } else {
            $('#_cardstatus').val("0");
            $('#_cardstatus-div').html("Suspended");
        }
  }

function searchCardsv2() {
    var val = document.getElementById('_keyword').value;
    var s = './CardDetails.jsp?_searchtext=' + encodeURIComponent(val);
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#users_table_main').html(data);

        }
    });
}

function searchCards() {
    var val = document.getElementById('_keyword').value;
    var val1 = document.getElementById('_changeBridgeSearchType').value;
//    alert(val1);
    var s = './CardTables.jsp?_searchtext=' + encodeURIComponent(val) +'&_searchTypeBridge='+encodeURIComponent(val1);
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#users_table_main').html(data);
        }
    });
}

$(document).ready(function() {
    $('#_ISExempt').change(function() {
        var selectedValue = $(this).val();

        if(selectedValue == 0){
             document.getElementById("hidestartdate").style.display = 'none'; 
             document.getElementById("hideenddate").style.display = 'none';
             //document.getElementById("bypasDate").style.display = 'none';
            
        }else if(selectedValue == 1){
             var ele = document.getElementById("hidestartdate");
             ele.style.display = "block"; 
              var ele = document.getElementById("hideenddate");
             ele.style.display = "block"; 
             //var ele = document.getElementById("bypasDate");
             //ele.style.display = "block"; 
            
        }

    });
});

function editCarddetails(){
    var s = './editCarddetails';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#primaryform").serialize(),
        success: function(data) {
            if ( strcmpBridge(data._result,"error") == 0 ) {
                Alert4Bridge("<span><font color=red>" + data._message + "</font></span>");
            }
            else if ( strcmpBridge(data._result,"success") == 0 ) {
                Alert4Bridge("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}
function removeCardDetails(val){
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './removeCardDetails?_cardnumber='+val;
        $.ajax({
                type: 'GET',    
                url: s,
                dataType: 'json',
                success: function(data) {
                    if ( strcmpBridge(data._result,"success") == 0 ) {
                        Alert4Bridge("<span><font color=red>" + data._message + "</font></span>");                        
//                        window.setTimeout(RefreshEmailTemplatesList, 2000);
              
                    } else {
                        Alert4Bridge("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}

function UploadCardFile() {
    $('#buttonUploadXMLEAD').attr("disabled", true);
    var s = './uploadCardFile';
    $.ajaxFileUpload({
        fileElementId: 'fileXMLToUploadEAD',
        url: s,
        dataType: 'json',
        success: function(data, status) {

            if (strcmpBridge(data.result, "error") == 0) {
                Alert4Bridge("<span><font color=red>" + data.message + "</font></span>");
            }
            else if (strcmpBridge(data.result, "success") == 0) {
                Alert4Bridge("<span><font color=blue>" + data.message + "</font></span>");
                $('#buttonUploadPASSEAD').attr("disabled", false);
            }
        },
        error: function(data, status, e)
        {
            alert(e);
        }
    });
}

function addCardsFile(){
       
    $('#save-card-details').attr("disabled", true);
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    //alert('hello');
    var s = './addCardDetails';
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#uploadXMLFormEAD").serialize(),
        success: function(data) {
            if ( strcmpBridge(data._result,"error") == 0 ) {

            }
            else if ( strcmpBridge(data._result,"success") == 0 ) {
//                alert("hi");
            pleaseWaitDiv.modal('hide');
             var ele = document.getElementById("result-div");
             ele.style.display = "block"; 
//                 $('#add-new-contact-result').html("<span><font color=blue>" + data._message + "</font></span>");
                 $('#download-card-details').html("<a href=./downloadSuccessEntries"+data._iformat+">Download Failed Entries</a>");
               showCardTable();
               
            }
        }
    }); 
}
function showCardTable() {
    var s = './CardTables.jsp?_searchtext=' + encodeURIComponent(val) +'&_searchTypeBridge='+encodeURIComponent(val1);
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#card_table_main').html(data);
        }
    });
}
//
function SuccessCardEntriesDownload() {
    var s = './downloadSuccessEntries?_type='+1;
    window.location.href = s;
    return false;
}

function FailedCardEntriesDownload() {
    var s = './downloadSuccessEntries?_type='+0;
    window.location.href = s;
    return false;
}

function CardUserAudits(_cardNumber,_cardUserid,_cardIbUserid){
    alert("hello");
    var val1 = encodeURIComponent(document.getElementById('auditstartdate').value);
    var val2 = encodeURIComponent(document.getElementById('auditenddate').value);
    var val3 = encodeURIComponent(document.getElementById('_reportType').value);
    var s = './getCardUserAudits?_cardNumber='+encodeURIComponent(_cardNumber)+'&_cardUserid='+encodeURIComponent(_cardUserid)+ '&_cardIbUserid='+encodeURIComponent(_cardIbUserid)+ "&_format="+val3+"&_startdate="+val1+"&_enddate="+val2+"&_itemtype=CARD_MANAGEMENT";
    window.location.href = s;
    return false;
}


//from sacjin
function assignOOBandSW(userid, _category, _subcategory) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './assigntoken?_userid=' + encodeURIComponent(userid) + "&_category=" + _category + "&_subcategory=" + _subcategory;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpBridge(data._result, "error") == 0) {
                        Alert4Bridge("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpBridge(data._result, "success") == 0) {
                        Alert4Bridge("<span><font color=blue>" + data._message + "</font></span>");
                        //window.setTimeout(RefreshTokenList, 2000);

                    }
                }
            });
        }
    });
}


function assignhardtoken() {
    var s = './assigntoken';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#HardwareRegistrationform").serialize(),
        success: function(data) {
            if (strcmpBridge(data._result, "error") == 0) {
                Alert4Bridge("<span><font color=red>" + data._message + "</font></span>");
                $('#_serialnumber').val("");
            }
            else if (strcmpBridge(data._result, "success") == 0) {
                Alert4Bridge("<span><font color=blue>" + data._message + "</font></span>");
                $("#HardRegistration").modal('hide');
               
            }
        }
    });

}
function changeotptokensstatus(status, userid, _category, _subcategory) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        }
        else {
            var s = './changeotpstatus?_status=' + status + '&_userid=' + encodeURIComponent(userid) + "&_category=" + _category + "&_subcategory=" + _subcategory;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpBridge(data._result, "error") == 0) {
                        Alert4Bridge("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpBridge(data._result, "success") == 0) {
                        Alert4Bridge("<span><font color=blue>" + data._message + "</font></span>");


                    }
                }
            });
        }
    });
}

function changeOOBandSW(userid, _category, _subcategory) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './changetokentype?_userid=' + encodeURIComponent(userid) + "&_category=" + _category + "&_subcategory=" + _subcategory;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpBridge(data._result, "error") == 0) {
                        Alert4Bridge("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpBridge(data._result, "success") == 0) {
                        Alert4Bridge("<span><font color=blue>" + data._message + "</font></span>");


                    }
                }
            });
        }
    });
}
function assignOOBandSW(userid, _category, _subcategory) {
    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './assigntoken?_userid=' + encodeURIComponent(userid) + "&_category=" + _category + "&_subcategory=" + _subcategory;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpBridge(data._result, "error") == 0) {
                        Alert4Bridge("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strcmpBridge(data._result, "success") == 0) {
                        Alert4Bridge("<span><font color=blue>" + data._message + "</font></span>");
                        //window.setTimeout(RefreshTokenList, 2000);

                    }
                }
            });
        }
    });
}

function sendreg(userid, _category, _subcategory) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            //var s = './sendregistration?_userid=' + userid;
            var s = './sendregistration?_userid=' + encodeURIComponent(userid) + "&_category=" + _category + "&_subcategory=" + _subcategory;

            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpBridge(data._result, "success") == 0) {
                        Alert4Bridge("<span><font color=blue>" + data._message + "</font></span>");
                    } else {
                        Alert4Bridge("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
        }
    });
}
function loadHarddetails(userid, _category, _subcategory) {
    
    $("#_userid").val(userid);
    $("#_category").val(_category);
    $("#_subcategory").val(_subcategory);
    $("#HardRegistration").modal();
}
//end from sachin

function ChangeReportOOBForBridge(value) {

    if (value == 1) {
        $('#_oobreportType').val("1");
        $('#_oobreportType-div').html("CSV");
    } else {
            $('#_oobreportType').val("0");
        $('#_oobreportType-div').html("PDF");
    }

}

function CardUserOOBAudits(_cardNumber,_cardUserid,_cardIbUserid){
    var val1 = encodeURIComponent(document.getElementById('startoobdate').value);
    var val2 = encodeURIComponent(document.getElementById('endoobdate').value);
    var val3 = encodeURIComponent(document.getElementById('_oobreportType').value);
    var s = './getUserTokenAudits?_cardNumber='+encodeURIComponent(_cardNumber)+'&_cardUserid='+encodeURIComponent(_cardUserid)+'&_cardIbUserid='+encodeURIComponent(_cardIbUserid)+ "&_format="+val3+"&_startdate="+val1+"&_enddate="+val2+"&_itemtype=OTPTOKENS";
    window.location.href = s;
    return false;
}

function CardUserTxAudits(_cardid){
    var val1 = encodeURIComponent(document.getElementById('starttxdate').value);
    var val2 = encodeURIComponent(document.getElementById('endtxdate').value);
    var val3 = encodeURIComponent(document.getElementById('_txReportType').value);
    var s = './getTransactionAudit?_cardid='+encodeURIComponent(_cardid)+ "&_format="+val3+"&_startdate="+val1+"&_enddate="+val2+"&_itemtype=OTPTOKENS";
    window.location.href = s;
    return false;
}