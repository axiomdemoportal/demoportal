<%@page import="com.mollatech.axiom.connector.mobiletrust.Location"%>
<%@page import="com.mollatech.axiom.v2.core.rss.AxiomData"%>
<%@include file="header.jsp" %>
<script src="./assets/js/login.js"></script>

<%    String _ammount = (String) session.getAttribute("_ammount");
    String _source = (String) session.getAttribute("_source");
    String _dest = (String) session.getAttribute("_dest");
    String _signaturekey = (String) session.getAttribute("_signaturekey");
    String _otp = (String) session.getAttribute("_sotp");
    AxiomData aData = (AxiomData) session.getAttribute("_axiomData");
    Location loc = (Location) session.getAttribute("_Location");

%>
<div class="container-fluid">
    <h1 class="text-success">Transaction Details Validation...!!! </h1>

    <br>
    <div class="row-fluid">
        <form class="form-horizontal" id ="transactionauth" name="transactionauth">
            <fieldset> 

                <% if (aData.getIErrorCode() == 0) {%>
                <div class="control-group"> 
                    <div class="controls"> 

                        <h2>You are transferring amount from the specified account : <%=_source%>  to the destination account : <%=_dest%> and the amount you specified for transaction is :<%=_ammount%></h2>
                    </div> 
                </div> 
                <%}%>

                <div class="control-group"> 
                    <div class="controls"> 
                        <% if (aData.getIErrorCode() == 0) {
                                if (loc == null) {%>

                        <h2>Your Geo-Fencing is "Successful".</h2>
                        <%} else {%>
                        <h2>Your Geo-Fencing is "Successful" Your Location is <%=loc.country%></h2>

                        <%}%>
                        <%} else if (aData.getIErrorCode() == -14
                                || aData.getIErrorCode() == -7
                                || aData.getIErrorCode() == -10
                                || aData.getIErrorCode() == -11
                                || aData.getIErrorCode() == -12
                                || aData.getIErrorCode() == -13) {
                            if (loc == null) {%>

                        <h2>Your Geo-Fencing is "Failed".</h2>
                        <%} else {%>
                        <h2>Your Geo-Fencing is "Failed" Your Location is "<%=loc.country%>"</h2>

                        <%}%>



                        <%}%>
                    </div>
                </div>

                <div class="control-group"> 
                    <div class="controls"> 
                        <% if (aData.getIErrorCode() == 0) {%>
                        <h2>Your One Time Password "<%=_otp%>" Verification was Successful.</h2>
                        <%} else if (aData.getIErrorCode() == -9
                                || aData.getIErrorCode() == -16
                                || aData.getIErrorCode() == -17
                                || aData.getIErrorCode() == -22) {%>
                        <h2>Your One Time Password "<%=_otp%>" Verification was Failed.</h2>
                        <%}%>
                    </div>
                </div> 

                <div class="control-group"> 
                    <div class="controls"> 
                        <% if (aData.getIErrorCode() == 0) {%> 
                        <h2>Your RSA PKCS#1 Signature <%=aData.getSignature()%> with archive <%=aData.getArchiveid()%>ID  Verification Successful.</h2>
                        <%} else {%>
                        <h2>Your Remote Signature Signing Failed.</h2>
                        <%}%>
                    </div>
                </div> 


                <div class="control-group">
                    <div class="controls">
                        <div id="savecrtsettings-result"></div> 
                        <!--                        <button class="btn btn-primary" onclick="transauthentication()" type="button" id="savecrtsettings">OK >></button>                        -->
                        <a class="btn btn-primary" href="./transaction.jsp" type="button" id="savecrtsettings">More Transaction >></a>                        
                    </div>
                </div>
            </fieldset>
        </form>
    </div>

</div>

<%@include file="footer.jsp" %>