/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package axiom.web.service;

import com.mollatech.axiom.bridge.crypto.LoadSettings;
import com.mollatech.axiom.bridge.crypto.UtilityFunctions;

import com.mollatech.axiom.v2.core.rss.EasyCheckIn;
import com.mollatech.axiom.v2.core.rss.EasyCheckInSessions;
//import com.mollatech.axiom.v2.core.Axiom;
//import com.mollatech.axiom.v2.core.AxiomCoreInterfaceImplService;
import com.mollatech.axiom.v2.core.rss.AuthenticationPackage;
import com.mollatech.axiom.v2.core.rss.AxiomChallengeResponse;
import com.mollatech.axiom.v2.core.rss.AxiomData;
import com.mollatech.axiom.v2.core.rss.AxiomException;
import com.mollatech.axiom.v2.core.rss.AxiomStatus;
import com.mollatech.axiom.v2.core.rss.AxiomTokenData;
import com.mollatech.axiom.v2.core.rss.InitTransactionPackage;
import com.mollatech.axiom.v2.core.rss.RSSCoreInterfaceImplService;
import com.mollatech.axiom.v2.core.rss.RemoteSigningInfo;
import com.mollatech.axiom.v2.core.rss.Remotesignature;
//import com.mollatech.axiom.v2.core.rss.Remotesignature;
import com.mollatech.axiom.v2.core.rss.Rss;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import com.mollatech.axiom.v2.core.rss.RssUserDetails;
import com.mollatech.axiom.v2.core.rss.Securephrase;
import com.mollatech.axiom.v2.core.rss.TransactionStatus;
import com.mollatech.axiom.v2.core.rss.VerifyRequest;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.xml.namespace.QName;
import org.bouncycastle.util.encoders.Base64;

/**
 *
 * @author vikramsareen
 */
public class AxiomWrapper {

    private RSSCoreInterfaceImplService m_service = null;
    private Rss m_port = null;
    private String m_channelid = null;

//    private AxiomCoreInterfaceImplService m_Aservice = null;
//    private Axiom m_Aport = null;
//    private String m_sessionid = null;
    //added
//    public static final int TOKEN_STATUS_ACTIVE = 1;
//    public static final int TOKEN_STATUS_LOCKEd = -1;
//    public static final int TOKEN_STATUS_ASSIGNED = 0;
//    public static final int TOKEN_STATUS_UNASSIGNED = -10;
//    public static final int TOKEN_STATUS_SUSPENDED = -2;
//    public static final int TOKEN_STATUS_ALL = 2;
//    public static final int TOKEN_STATUS_LOST = -5;
//    public static final int TOKEN_STATUS_FREE = -10;
//    //categories
//    public static final int SOFTWARE_TOKEN = 1;
//    public static final int HARDWARE_TOKEN = 2;
//    public static final int OOB_TOKEN = 3;
//    //subcategory of OOB
//    public static final int OOB__SMS_TOKEN = 1;
//    public static final int OOB__VOICE_TOKEN = 2;
//    public static final int OOB__USSD_TOKEN = 3;
//    public static final int OOB__EMAIL_TOKEN = 4;
//    //subcategory of SOFTWARE
//    public static final int SW_WEB_TOKEN = 1;
//    public static final int SW_MOBILE_TOKEN = 2;
//    public static final int SW_PC_TOKEN = 3;
//    public static final int HW_MINI_TOKEN = 1;
//    public static final int HW_CR_TOKEN = 2;
//    final int ACTIVE_STATUS = 1;
//    //final int SUSPEND_STATUS = 0;
//    //final int SMS = 1;
//    final int SUCCESS = 0;
//    //static final int FREE = 0;
//
//    //templates Name
//    //for active
//    public static final String SW_TOKEN_STATUS_ACTIVE_TEMPLATE_NAME = "Activated Token";
//    public static final String HW_TOKEN_STATUS_ACTIVE_TEMPLATE_NAME = "Activated Hardware Token";
//    public static final String OOB_TOKEN_STATUS_ACTIVE_TEMPLATE_NAME = "Activated OOBToken";
//    // for locked
////    public static final String SW_TOKEN_STATUS_LOCKEd_TEMPLATE_NAME = "";
////    public static final String HW_TOKEN_STATUS_LOCKEd_TEMPLATE_NAME = "";
////    public static final String OOB_TOKEN_STATUS_LOCKEd_TEMPLATE_NAME = "";
//    //for unassign
//    public static final String SW_TOKEN_STATUS_UNASSIGNED_TEMPLATE_NAME = "Unassign Token";
//    public static final String HW_TOKEN_STATUS_UNASSIGNED_TEMPLATE_NAME = "Unassign Hardware Token";
//    public static final String OOB_TOKEN_STATUS_UNASSIGNED_TEMPLATE_NAME = "Unassign OOBToken";
//    //for suspended
//    public static final String SW_TOKEN_STATUS_SUSPENDED_TEMPLATE_NAME = "Suspended Token";
//    public static final String HW_TOKEN_STATUS_SUSPENDED_TEMPLATE_NAME = "Suspended  Hardware Token";
//    public static final String OOB_TOKEN_STATUS_SUSPENDED_TEMPLATE_NAME = "Suspended OOBToken";
//    //for assign
//    public static final String SW_TOKEN_STATUS_ASSIGNED_TEMPLATE_NAME = "Assign Token";
//    public static final String OOB_TOKEN_STATUS_ASSIGNED_TEMPLATE_NAME = "Assign OOBToken";
//    public static final String HW_TOKEN_STATUS_ASSIGNED_TEMPLATE_NAME = "Assign Hardware Token";
//    //  public static final String SOFTWARE_TOKEN_OTP_TEMPLATE_NAME = "Software Token";
////    public static final String HARDWARE_TOKEN_OTP_TEMPLATE_NAME = "";
//    public static final String OOB_SMS_TOKEN_OTP_TEMPLATE_NAME = "OOB_SMS_OTP Token";
//    public static final String OOB_USSD_TOKEN_OTP_TEMPLATE_NAME = "OOB_USSD_OTP Token";
//    public static final String OOB_VOICE_TOKEN_OTP_TEMPLATE_NAME = "OOB_VOICE_OTP Token";
//    public static final String OOB_EMAIL_TOKEN_OTP_TEMPLATE_NAME = "OOB_EMAIL_OTP Token";
//    public static final String OOB_SMS_TOKEN_SOTP_TEMPLATE_NAME = "OOB_SMS_SOTP Token";
//    public static final String OOB_USSD_TOKEN_SOTP_TEMPLATE_NAME = "OOB_USSD_SOTP Token";
//    public static final String OOB_VOICE_TOKEN_SOTP_TEMPLATE_NAME = "OOB_VOICE_SOTP Token";
//    public static final String OOB_EMAIL_TOKEN_SOTP_TEMPLATE_NAME = "OOB_EMAIL_SOTP Token";
//    public static final String TOKEN_TYPE_CHANGE_TEMPLATE_NAME = "Change Type Of Token";
//    public static final String SOFTWARE_TOKEN_REGCODE_TEMPLATE_NAME = "Registration Code";
//    public static final String HARDWARE_TOKEN_LOST_TEMPLATE_NAME = "Hardware Lost";
//    public static final String HARDWARE_TOKEN_OTP_RESYNC_TEMPLATE_NAME = "Resyn Hardware Token";
//    public static final String SOFTWARE_TOKEN_OTP_RESYNC_TEMPLATE_NAME = "Resyn Software Token";
//    //final int SUCCESS = 0;
//    final String strSUCCESS = "SUCCESS";
//    final int FAILED = -1;
//    final String strFAILED = "FAILED";
//    public static final int FAILED_TO_UPLOAD = -999999;
//    public static final int SUCCESS_TO_UPLOAD = 999999;
//    public static final int OTP_ALREADY_EXIST = -8;
//    public static final int OTP_TOKEN_NOT_FOUND = -17;
    //nilesh
    public static final int GET_USER_BY_USERID = 1;
    public static final int GET_USER_BY_PHONENUMBER = 2;
    public static final int GET_USER_BY_EMAILID = 3;
    public static final int GET_USER_BY_FULLNAME = 4;
    public static final int PKI_SOFTWARE_TOKEN = 8;

    public static final int PASSWORD = 1;
    public static final int SOFTWARE_TOKEN = 2;
    public static final int HARDWARE_TOKEN = 3;
    public static final int OUTOFBOUND_TOKEN = 4;
    public static final int CERTIFICATE = 5;
    public static final int CHALLAEGE_RESPONSE = 6;
    public static final int BIOMETRIX = 7;
    public static final int SECURE_PHRASE = 10;
    public static final int OTP_TOKEN_OUTOFBAND_SMS = 1;
    public static final int OTP_TOKEN_OUTOFBAND_VOICE = 3;
    public static final int OTP_TOKEN_OUTOFBAND_USSD = 2;
    public static final int OTP_TOKEN_OUTOFBAND_EMAIL = 4;
    public static final int OTP_TOKEN_SOFTWARE_MOBILE = 2;
    public static final int OTP_TOKEN_SOFTWARE_PC = 3;
    public static final int OTP_TOKEN_SOFTWARE_WEB = 1;
    public static final int OTP_TOKEN_HARDWARE_MINI = 1;
    public static final int OTP_TOKEN_HARDWARE_CR = 2;

    public static final int TOKEN_STATUS_ACTIVE = 1;
    public static final int TOKEN_STATUS_LOCKEd = -1;
    public static final int TOKEN_STATUS_ASSIGNED = 0;
    public static final int TOKEN_STATUS_UNASSIGNED = -10;
    public static final int TOKEN_STATUS_SUSPENDED = -2;
    public static final int TOKEN_STATUS_ALL = 2;
    public static final int TOKEN_STATUS_LOST = -5;
    public static final int TOKEN_STATUS_FREE = -10;

    //subcategory of OOB
    public static final int OOB__SMS_TOKEN = 1;
    public static final int OOB__VOICE_TOKEN = 2;
    public static final int OOB__USSD_TOKEN = 3;
    public static final int OOB__EMAIL_TOKEN = 4;
    //subcategory of SOFTWARE
    public static final int SW_WEB_TOKEN = 1;
    public static final int SW_MOBILE_TOKEN = 2;
    public static final int SW_PC_TOKEN = 3;

    public static int OTP = 2;
    public static int SignatureOTP = 3;
    public static int ChallengeAndResponse = 4;
    public static int Password = 1;
    public static int BioMetrix = 5;

    public static final int REGISTER = 1;
    public static final int LOGIN = 2;
    public static final int TRANSACTION = 3;
    public static final int CHANGEINPROFILE = 4;

    public static final int BLUE = 1;
    public static final int GREEN = 2;
    public static final int RED = 3;
    public static final int GRAY = 4;
    public static final int ORANGE = 5;
    public static final int YELLOW = 6;
//    public static final int PINK = 7;
    public static final int MAGENTA = 7;

    private String m_sessionid = null;

    public AxiomWrapper() {
        try {
            String wsdlname = LoadSettings.g_sSettings.getProperty("axiom.wsdl.name");
            String channelid = LoadSettings.g_sSettings.getProperty("axiom.channelid");
            m_channelid = channelid;
            String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
            String password = LoadSettings.g_sSettings.getProperty("axiom.password");
            String ipAddress = LoadSettings.g_sSettings.getProperty("axiom.ipaddress");
            String strPort = LoadSettings.g_sSettings.getProperty("axiom.port");
            String strSecured = LoadSettings.g_sSettings.getProperty("axiom.secured");

            String strDebug = null;
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    Date d = new Date();
                    System.out.println(d + ">>" + "channelid::" + channelid);
                    System.out.println(d + ">>" + "remotelogin::" + remotelogin);
                    System.out.println(d + ">>" + "password::" + password);
                    System.out.println(d + ">>" + "ipAddress::" + ipAddress);
                    System.out.println(d + ">>" + "strPort::" + strPort);
                    System.out.println(d + ">>" + "strSecured::" + strSecured);
                }
            } catch (Exception ex) {
            }

            SSLContext sslContext = null;
            try {
                HttpsURLConnection.setDefaultHostnameVerifier(new AllVerifier());
                try {
                    sslContext = SSLContext.getInstance("TLS");
                } catch (NoSuchAlgorithmException ex) {
                    //Logger.getLogger(NewClass.class.getName()).log(Level.SEVERE, null, ex);
                    ex.printStackTrace();
                }
                sslContext.init(null, new TrustManager[]{new AllTrustManager()}, null);
                HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

            } catch (KeyManagementException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//            RssUserCerdentials userObj = null;
            java.lang.String sessionid = null;
            try { // Call Web Service Operation

                String wsdlUrl = "http://" + ipAddress + ":" + strPort + "/" + wsdlname + "/RSSCoreInterfaceImpl?wsdl";
                if (strSecured != null && strSecured.compareToIgnoreCase("yes") == 0) {
                    wsdlUrl = "https://" + ipAddress + ":" + strPort + "/" + wsdlname + "/RSSCoreInterfaceImpl?wsdl";
                }

                URL url = new URL(wsdlUrl);
                QName qName = new QName("http://rss.core.v2.axiom.mollatech.com/", "RSSCoreInterfaceImplService");
                //m_service = new MobileTrustInterfaceImplService(url, qName);
                //m_port = m_service.getMobileTrustInterfaceImplPort();
                m_service = new RSSCoreInterfaceImplService(url, qName);
                m_port = m_service.getRSSCoreInterfaceImplPort();
//                byte[] SHA1hash = UtilityFunctions.SHA1(channelid + remotelogin + password);
//                integritycheck = new String(Base64.encode(SHA1hash));
//                m_sessionid = m_port.openSession(channelid, remotelogin, password, integritycheck);

                //System.out.println("Result = " + sessionid);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

//    public AxiomWrapper(int webService) {
//        try {
//            String wsdlname = LoadSettings.g_sSettings.getProperty("axiom.wsdl.name");
//            String channelid = LoadSettings.g_sSettings.getProperty("axiom.channelid");
//            m_channelid = channelid;
//            String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
//            String password = LoadSettings.g_sSettings.getProperty("axiom.password");
//            String ipAddress = LoadSettings.g_sSettings.getProperty("axiom.ipaddress");
//            String strPort = LoadSettings.g_sSettings.getProperty("axiom.port");
//            String strSecured = LoadSettings.g_sSettings.getProperty("axiom.secured");
//
//            String strDebug = null;
//            try {
//                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                    Date d = new Date();
//                    System.out.println(d + ">>" + "channelid::" + channelid);
//                    System.out.println(d + ">>" + "remotelogin::" + remotelogin);
//                    System.out.println(d + ">>" + "password::" + password);
//                    System.out.println(d + ">>" + "ipAddress::" + ipAddress);
//                    System.out.println(d + ">>" + "strPort::" + strPort);
//                    System.out.println(d + ">>" + "strSecured::" + strSecured);
//                }
//            } catch (Exception ex) {
//            }
//
//            SSLContext sslContext = null;
//            try {
//                HttpsURLConnection.setDefaultHostnameVerifier(new AllVerifier());
//                try {
//                    sslContext = SSLContext.getInstance("TLS");
//                } catch (NoSuchAlgorithmException ex) {
//                    //Logger.getLogger(NewClass.class.getName()).log(Level.SEVERE, null, ex);
//                    ex.printStackTrace();
//                }
//                sslContext.init(null, new TrustManager[]{new AllTrustManager()}, null);
//                HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
//
//            } catch (KeyManagementException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//
////            RssUserCerdentials userObj = null;
//            java.lang.String sessionid = null;
//            try { // Call Web Service Operation
//
//                String wsdlUrl = "http://" + ipAddress + ":" + strPort + "/" + wsdlname + "/AxiomCoreInterfaceImpl?wsdl";
//                if (strSecured != null && strSecured.compareToIgnoreCase("yes") == 0) {
//                    wsdlUrl = "https://" + ipAddress + ":" + strPort + "/" + wsdlname + "/AxiomCoreInterfaceImpl?wsdl";
//                }
//
//                URL url = new URL(wsdlUrl);
//                QName qName = new QName("http://core.v2.axiom.mollatech.com/", "AxiomCoreInterfaceImplService");
//                //m_service = new MobileTrustInterfaceImplService(url, qName);
//                //m_port = m_service.getMobileTrustInterfaceImplPort();
//                m_Aservice = new AxiomCoreInterfaceImplService(url, qName);
//                m_Aport = m_Aservice.getAxiomCoreInterfaceImplPort();
//                m_sessionid = m_Aport.openSession(channelid, remotelogin, password);
//                //System.out.println("Result = " + sessionid);
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
    public String OpenSession(String channelId, String username, String password) {
        try {
            if (channelId != null) {
                byte[] SHA1hash = UtilityFunctions.SHA1(channelId + username + password);
                String integritycheck = new String(Base64.encode(SHA1hash));
                String sessionID = m_port.openSession(channelId, username, password, integritycheck);
                return sessionID;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public RssUserCerdentials getUserCerdentials(String sessionId, String username, int type, String trustPayLoad) {
        try {
            if (sessionId != null) {
//                byte[] SHA1hash = UtilityFunctions.SHA1(sessionId + username + type + trustPayLoad);
//                String integritycheck = new String(Base64.encode(SHA1hash));
                byte[] SHA1hash = null;
                if (trustPayLoad == null) {
                    SHA1hash = UtilityFunctions.SHA1(sessionId + username + type);
                } else {
                    SHA1hash = UtilityFunctions.SHA1(sessionId + username + type + trustPayLoad);
                }
                String integritycheck = new String(Base64.encode(SHA1hash));
                RssUserCerdentials sessionID = m_port.getRSSUser(sessionId, username, type, integritycheck, trustPayLoad);
                return sessionID;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public AxiomStatus verifyCredential(String sessionId, String userId, VerifyRequest vRequest, String trustPayLoad) {
        try {
            if (sessionId != null) {
                System.out.println("integrity check =" + sessionId + userId + vRequest.getCredential() + trustPayLoad);
                byte[] SHA1hash = null;
                if (trustPayLoad == null) {
                    SHA1hash = UtilityFunctions.SHA1(sessionId + userId + vRequest.getCredential() + vRequest.getCategory());
                } else {
                    SHA1hash = UtilityFunctions.SHA1(sessionId + userId + vRequest.getCredential() + vRequest.getCategory() + trustPayLoad);
                }
                String integritycheck = new String(Base64.encode(SHA1hash));

                AxiomStatus astatus = m_port.verifyCredential(sessionId, userId, vRequest, integritycheck, trustPayLoad);
                return astatus;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public AxiomStatus CloseSession(String sessionId) {
        try {
            if (sessionId != null) {
                byte[] SHA1hash = UtilityFunctions.SHA1(sessionId);
                String integritycheck = new String(Base64.encode(SHA1hash));
                AxiomStatus status = m_port.closeSession(sessionId, integritycheck);
                return status;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public AxiomStatus ChangeUserCredentials(String sessionId, RssUserCerdentials credentials, String trustPayLoad) {
        try {
            if (sessionId != null) {
//                byte[] SHA1hash = UtilityFunctions.SHA1(sessionId + credentials.getRssUser().getEmailid() + credentials.getRssUser().getUserId() + credentials.getRssUser().getPhoneNumber() + credentials.getRssUser().getUserName() + trustPayLoad);
                byte[] SHA1hash = null;
                if (trustPayLoad == null) {
                    SHA1hash = UtilityFunctions.SHA1(sessionId + credentials.getRssUser().getEmailid() + credentials.getRssUser().getUserId() + credentials.getRssUser().getPhoneNumber() + credentials.getRssUser().getUserName());
                } else {
                    SHA1hash = UtilityFunctions.SHA1(sessionId + credentials.getRssUser().getEmailid() + credentials.getRssUser().getUserId() + credentials.getRssUser().getPhoneNumber() + credentials.getRssUser().getUserName() + trustPayLoad);
                }
//                String integritycheck = new String(Base64.encode(SHA1hash));
                String integritycheck = new String(Base64.encode(SHA1hash));
                AxiomStatus status = m_port.changeCredential(sessionId, credentials, integritycheck, trustPayLoad);
                return status;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

//      public AxiomStatus ChangeTokenStatus(String sessionId,RssUserCerdentials credentials) {
//        try {
//            if (sessionId != null) {
//                byte[] SHA1hash = UtilityFunctions.SHA1(sessionId);
//                integritycheck = new String(Base64.encode(SHA1hash));
//                AxiomStatus status = m_port.
//                return status;
//            } else {
//                return null;
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
    public AxiomStatus ChangeTokenStatus(String sessionId, String userId, String signData, int type, String integrityCheck) {
        AxiomStatus astatus = null;
        try {
            byte[] SHA1hash = null;

            SHA1hash = UtilityFunctions.SHA1(sessionId + userId + type);

            String integritycheck = new String(Base64.encode(SHA1hash));
            astatus = m_port.aLertEvent(sessionId, userId, null, type, null);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return astatus;

    }

    public AxiomStatus CreateRSSUser(String sessionid, RssUserDetails rssUser, String integritycheck, String trustPayLoad) {
        try {
            if (sessionid != null) {
                AxiomStatus aStatus = new AxiomStatus();
                byte[] SHA1hash = UtilityFunctions.SHA1(sessionid + rssUser.getEmailid() + rssUser.getUserName() + rssUser.getPhoneNumber() + trustPayLoad);
                integritycheck = new String(Base64.encode(SHA1hash));

                aStatus = m_port.createRSSUser(sessionid, rssUser, integritycheck, trustPayLoad);
                return aStatus;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public AxiomStatus ChangeRSSUserDetails(String sessionid, String rssUserID, RssUserDetails rssUser, String integritycheck, String trustPayLoad) {

        try {
            if (sessionid != null) {
                AxiomStatus aStatus = new AxiomStatus();

                byte[] SHA1hash = UtilityFunctions.SHA1(sessionid + rssUserID + rssUser.getEmailid() + rssUser.getUserName() + rssUser.getPhoneNumber() + trustPayLoad);
                integritycheck = new String(Base64.encode(SHA1hash));

                aStatus = m_port.changeRSSUserDetails(sessionid, rssUserID, rssUser, integritycheck, trustPayLoad);
                return aStatus;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public AxiomStatus ResetCredential(String sessionid, RssUserCerdentials credential, String integritycheck, String trustPayLoad) {
        try {
            if (sessionid != null) {
                AxiomStatus aStatus = new AxiomStatus();

                byte[] SHA1hash = UtilityFunctions.SHA1(sessionid + credential.getRssUser().getEmailid() + credential.getRssUser().getUserId()
                        + credential.getRssUser().getPhoneNumber() + credential.getRssUser().getUserName());
                integritycheck = new String(Base64.encode(SHA1hash));
                aStatus = m_port.resetCredential(sessionid, credential, integritycheck, trustPayLoad);
                return aStatus;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public AxiomStatus AssignCredential(String sessionid, RssUserCerdentials credentials, String integritycheck, String trustPayLoad) {
        try {
            if (sessionid != null) {
                AxiomStatus aStatus = new AxiomStatus();

                byte[] SHA1hash = null;
                if (trustPayLoad == null) {
                    SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.getRssUser().getEmailid() + credentials.getRssUser().getUserId() + credentials.getRssUser().getPhoneNumber() + credentials.getRssUser().getUserName());
                } else {
                    SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.getRssUser().getEmailid() + credentials.getRssUser().getUserId() + credentials.getRssUser().getPhoneNumber() + credentials.getRssUser().getUserName() + trustPayLoad);
                }
                aStatus = m_port.assignCredential(sessionid, credentials, integritycheck, trustPayLoad);
                return aStatus;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public AxiomChallengeResponse getQuestionsForRegistration(String sessionid, int noOfQuestion, String integritycheck) throws AxiomException {

        try {
            if (sessionid != null) {
                AxiomChallengeResponse acResponse = new AxiomChallengeResponse();

                byte[] SHA1hash = UtilityFunctions.SHA1(sessionid + noOfQuestion);
                integritycheck = new String(Base64.encode(SHA1hash));
                acResponse = m_port.getQuestionsForRegistration(sessionid, noOfQuestion, integritycheck);
                return acResponse;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public String GenerateSignatureCode(String sessionid, String[] data, String integritycheck) throws AxiomException {

        try {
            if (sessionid != null) {
                String sCode = null;

                List<String> lData = new ArrayList<String>();
                String strData = "";
                for (int i = 0; i < data.length; i++) {
                    lData.add(data[i]);
                    if (strData == null) {
                        strData = data[i];

                    } else {
                        strData = strData + data[i];
                    }
                }

                byte[] SHA1hash = UtilityFunctions.SHA1(sessionid + strData);
                integritycheck = new String(Base64.encode(SHA1hash));

                sCode = m_port.generateSignatureCode(sessionid, lData, integritycheck);
                return sCode;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public String EnforceSecurity(String sessionid, String userid, String data, String integritycheck, String trustPayLoad) throws AxiomException {
        try {
            if (sessionid != null) {
                String response = null;

                byte[] SHA1hash = UtilityFunctions.SHA1(sessionid + userid + data + trustPayLoad);
                integritycheck = new String(Base64.encode(SHA1hash));
                response = m_port.enforceSecurity(sessionid, userid, data, integritycheck, trustPayLoad);
                return response;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public String ConsumeSecurity(String sessionid, String userid, String data, String integritycheck, String trustPayLoad) throws AxiomException {

        try {
            if (sessionid != null) {
                String response = null;

                byte[] SHA1hash = UtilityFunctions.SHA1(sessionid + userid + data + trustPayLoad);
                integritycheck = new String(Base64.encode(SHA1hash));
                response = m_port.consumeSecurity(sessionid, userid, data, integritycheck, trustPayLoad);
                return response;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public AxiomData VerifySignature(String sessionid, String userid, String dataToSign, String envelopedData, int docType, String integritycheck, String trustPayLoad) {

        try {
            if (sessionid != null) {
                AxiomData aData = new AxiomData();

                byte[] SHA1hash = UtilityFunctions.SHA1(sessionid + userid + dataToSign + envelopedData + docType + trustPayLoad);
                integritycheck = new String(Base64.encode(SHA1hash));

                aData = m_port.verifySignature(sessionid, userid, dataToSign, envelopedData, docType, integritycheck, trustPayLoad);
                return aData;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public AxiomData ValidateUserAndSignTransaction(String sessionid, String userid, VerifyRequest verifyRequest, RemoteSigningInfo rsInfo, boolean bNotifyUser, String integritycheck, String trustPayLoad) throws AxiomException {
        try {
            if (sessionid != null) {
                AxiomData aData = new AxiomData();

                byte[] SHA1hash = null;
                if (trustPayLoad == null) {
                    SHA1hash = UtilityFunctions.SHA1(sessionid + userid + verifyRequest.getCredential() + verifyRequest.getCategory() + rsInfo.getClientLocation()
                            + rsInfo.getDataToSign() + rsInfo.getDesignation() + rsInfo.getName() + rsInfo.getReason() + rsInfo.getReferenceId() + rsInfo.isBAddTimestamp()
                            + rsInfo.isBNotifyUser() + rsInfo.isBReturnSignedDocument() + rsInfo.getType() + bNotifyUser);
                    integritycheck = new String(Base64.encode(SHA1hash));
                } else {
                    SHA1hash = UtilityFunctions.SHA1(sessionid + userid + verifyRequest.getCredential() + verifyRequest.getCategory() + rsInfo.getClientLocation()
                            + rsInfo.getDataToSign() + rsInfo.getDesignation() + rsInfo.getName() + rsInfo.getReason() + rsInfo.getReferenceId() + rsInfo.isBAddTimestamp()
                            + rsInfo.isBNotifyUser() + rsInfo.isBReturnSignedDocument() + rsInfo.getType() + bNotifyUser + trustPayLoad);
                }
                integritycheck = new String(Base64.encode(SHA1hash));

                aData = m_port.validateUserAndSignTransaction(sessionid, userid, verifyRequest, rsInfo, bNotifyUser, integritycheck, trustPayLoad);
                return aData;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

//    public int SendRegistrationCode(String sessionid, String userid, int category) {
//        try {
//            //String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
//            //String password = LoadSettings.g_sSettings.getProperty("axiom.password");
//            //String sessionid = m_port.openSession(m_channelid, remotelogin, password);
//           
//                com.mollatech.axiom.v2.core.AxiomStatus as = m_Aport.sendRegistrationCode(sessionid, userid, category);
//                //m_port.closeSession(m_sessionid);
//                if (as.getErrorcode() != 0) {
//                    return as.getErrorcode();
//                } else {
//                    return 0;
//                }
//          
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return -100;
//        }
//    }
//
//    public int SendSignatureOneTimePassword(String sessionid, String userid, String[] data) {
//        try {
//            //String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
//            //String password = LoadSettings.g_sSettings.getProperty("axiom.password");
//            //String sessionid = m_port.openSession(m_channelid, remotelogin, password);
//            
//                List<String> aData = new ArrayList<String>();
//                for (int i = 0; i < data.length; i++) {
//                    aData.add(data[i]);
//                }
//                com.mollatech.axiom.v2.core.AxiomStatus as = m_Aport.sendSignatureOTP(sessionid, userid, aData);
//                //m_port.closeSession(m_sessionid);
//                if (as.getErrorcode() != 0) {
//                    return as.getErrorcode();
//                } else {
//                    return 0;
//                }
//           
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return -100;
//        }
//    }
//
//    public int SendOneTimePassword(String sessionid, String userid) {
//        try {
//            //String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
//            //String password = LoadSettings.g_sSettings.getProperty("axiom.password");
//            //String sessionid = m_port.openSession(m_channelid, remotelogin, password);
//           
//                com.mollatech.axiom.v2.core.AxiomStatus as = m_Aport.sendOTP(sessionid, userid);
//                //m_port.closeSession(m_sessionid);
//                if (as.getErrorcode() != 0) {
//                    return as.getErrorcode();
//                } else {
//                    return 0;
//                }
//            
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return -100;
//        }
//    }
//    public int SendRegistrationCode(String sessionid, String userid, int category) {
//        try {
//            //String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
//            //String password = LoadSettings.g_sSettings.getProperty("axiom.password");
//            //String sessionid = m_port.openSession(m_channelid, remotelogin, password);
//
//            com.mollatech.axiom.v2.core.AxiomStatus as = m_Aport.sendRegistrationCode(sessionid, userid, category);
//            //m_port.closeSession(m_sessionid);
//            if (as.getErrorcode() != 0) {
//                return as.getErrorcode();
//            } else {
//                return 0;
//            }
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return -100;
//        }
//    }
//    public int SendSignatureOneTimePassword(String sessionid, String userid, String[] data) {
//        try {
//            //String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
//            //String password = LoadSettings.g_sSettings.getProperty("axiom.password");
//            //String sessionid = m_port.openSession(m_channelid, remotelogin, password);
//
//            List<String> aData = new ArrayList<String>();
//            for (int i = 0; i < data.length; i++) {
//                aData.add(data[i]);
//            }
//            com.mollatech.axiom.v2.core.AxiomStatus as = m_Aport.sendSignatureOTP(sessionid, userid, aData);
//            //m_port.closeSession(m_sessionid);
//            if (as.getErrorcode() != 0) {
//                return as.getErrorcode();
//            } else {
//                return 0;
//            }
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return -100;
//        }
//    }
//    public int SendOneTimePassword(String sessionid, String userid) {
//        try {
//            //String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
//            //String password = LoadSettings.g_sSettings.getProperty("axiom.password");
//            //String sessionid = m_port.openSession(m_channelid, remotelogin, password);
//
//            com.mollatech.axiom.v2.core.AxiomStatus as = m_Aport.sendOTP(sessionid, userid);
//            //m_port.closeSession(m_sessionid);
//            if (as.getErrorcode() != 0) {
//                return as.getErrorcode();
//            } else {
//                return 0;
//            }
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return -100;
//        }
//    }
    public AxiomWrapper(int webService) {
        try {
            String wsdlname = LoadSettings.g_sSettings.getProperty("axiom.wsdl.name");
            String channelid = LoadSettings.g_sSettings.getProperty("axiom.channelid");
            m_channelid = channelid;
            String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
            String password = LoadSettings.g_sSettings.getProperty("axiom.password");
            String ipAddress = LoadSettings.g_sSettings.getProperty("axiom.ipaddress");
            String strPort = LoadSettings.g_sSettings.getProperty("axiom.port");
            String strSecured = LoadSettings.g_sSettings.getProperty("axiom.secured");

            String strDebug = null;
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    Date d = new Date();
                    System.out.println(d + ">>" + "channelid::" + channelid);
                    System.out.println(d + ">>" + "remotelogin::" + remotelogin);
                    System.out.println(d + ">>" + "password::" + password);
                    System.out.println(d + ">>" + "ipAddress::" + ipAddress);
                    System.out.println(d + ">>" + "strPort::" + strPort);
                    System.out.println(d + ">>" + "strSecured::" + strSecured);
                }
            } catch (Exception ex) {
            }

            SSLContext sslContext = null;
            try {
                HttpsURLConnection.setDefaultHostnameVerifier(new AllVerifier());
                try {
                    sslContext = SSLContext.getInstance("TLS");
                } catch (NoSuchAlgorithmException ex) {
                    //Logger.getLogger(NewClass.class.getName()).log(Level.SEVERE, null, ex);
                    ex.printStackTrace();
                }
                sslContext.init(null, new TrustManager[]{new AllTrustManager()}, null);
                HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

            } catch (KeyManagementException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//            RssUserCerdentials userObj = null;
            java.lang.String sessionid = null;
            try { // Call Web Service Operation

                String wsdlUrl = "http://" + ipAddress + ":" + strPort + "/" + wsdlname + "/AxiomCoreInterfaceImpl?wsdl";
                if (strSecured != null && strSecured.compareToIgnoreCase("yes") == 0) {
                    wsdlUrl = "https://" + ipAddress + ":" + strPort + "/" + wsdlname + "/AxiomCoreInterfaceImpl?wsdl";
                }

                URL url = new URL(wsdlUrl);
                QName qName = new QName("http://core.v2.axiom.mollatech.com/", "AxiomCoreInterfaceImplService");
                //m_service = new MobileTrustInterfaceImplService(url, qName);
                //m_port = m_service.getMobileTrustInterfaceImplPort();
//                m_Aservice = new AxiomCoreInterfaceImplService(url, qName);
//                m_Aport = m_Aservice.getAxiomCoreInterfaceImplPort();
//                m_sessionid = m_Aport.openSession(channelid, remotelogin, password);
                //System.out.println("Result = " + sessionid);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {

        try {
            VerifyRequest vReq = new VerifyRequest();
            String[] data = new String[3];
            data[0] = "451";
            data[1] = "789";
            data[2] = "14789";

            vReq.setCategory(AxiomWrapper.SignatureOTP);
            vReq.setCredential("123456");
            List<String> lData = new ArrayList<String>();
            for (int i = 0; i < data.length; i++) {
                lData.add(data[i]);
                vReq.getSigningData().add(data[i]);
                System.out.println(vReq.getSigningData().get(i));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //nilesh
    public static final int REGISTERUSER = 5;
    public static final int GENERATEAUTHENTICATIONPACKAGE = 5;
    public static final int GETIMAGE = 6;
    public static final int generateAuthenticationPackage = 7;

    public String checkUserCerdentials(String sessionId, RssUserCerdentials credentials, String cookieString, int type, String trustPayLoad) {
        try {
            if (sessionId != null) {
                byte[] SHA1hash = null;
                if (trustPayLoad == null) {

                    SHA1hash = UtilityFunctions.SHA1(sessionId + credentials.getRssUser().getEmailid() + credentials.getRssUser().getUserId()
                            + credentials.getRssUser().getPhoneNumber() + credentials.getRssUser().getUserName() + cookieString);
                } else {
                    SHA1hash = UtilityFunctions.SHA1(sessionId + credentials.getRssUser().getEmailid() + credentials.getRssUser().getUserId()
                            + credentials.getRssUser().getPhoneNumber() + credentials.getRssUser().getUserName() + cookieString + trustPayLoad);
                }
                String integritycheck = new String(Base64.encode(SHA1hash));
                String sessionID = m_port.checkUser(sessionId, credentials, cookieString, integritycheck, trustPayLoad);
                return sessionID;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public Securephrase getImage(String sessionId, RssUserCerdentials credentials, String cookieString, int type, String trustPayLoad) {
        try {
            if (sessionId != null) {
                byte[] SHA1hash = null;
                if (trustPayLoad == null) {

                    SHA1hash = UtilityFunctions.SHA1(sessionId + credentials.getRssUser().getEmailid() + credentials.getRssUser().getUserId()
                            + credentials.getRssUser().getPhoneNumber() + credentials.getRssUser().getUserName() + cookieString);
                } else {
                    SHA1hash = UtilityFunctions.SHA1(sessionId + credentials.getRssUser().getEmailid() + credentials.getRssUser().getUserId()
                            + credentials.getRssUser().getPhoneNumber() + credentials.getRssUser().getUserName() + cookieString + trustPayLoad);
                }
                String integritycheck = new String(Base64.encode(SHA1hash));
                Securephrase sessionID = m_port.getImage(sessionId, credentials, cookieString, sessionId, null, trustPayLoad);
                return sessionID;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public AuthenticationPackage generateAuthenticationPackage(String sessionId, RssUserCerdentials credentials, String cookieString, String trustPayLoad) {
        try {
            if (sessionId != null) {
                byte[] SHA1hash = null;
                if (trustPayLoad == null) {

                    SHA1hash = UtilityFunctions.SHA1(sessionId + credentials.getRssUser().getEmailid() + credentials.getRssUser().getUserId()
                            + credentials.getRssUser().getPhoneNumber() + credentials.getRssUser().getUserName());
                } else {
                    SHA1hash = UtilityFunctions.SHA1(sessionId + credentials.getRssUser().getEmailid() + credentials.getRssUser().getUserId()
                            + credentials.getRssUser().getPhoneNumber() + credentials.getRssUser().getUserName() + trustPayLoad);
                }
                String integritycheck = new String(Base64.encode(SHA1hash));
                AuthenticationPackage sessionID = m_port.generateAuthenticationPackage(sessionId, credentials, cookieString, sessionId,
                        integritycheck, trustPayLoad);
                return sessionID;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public AxiomStatus initTransaction(InitTransactionPackage package1, String trustPayLoad) {
        try {
            if (package1.getSessionid() != null) {
                byte[] SHA1hash = null;
                if (trustPayLoad == null) {
                    SHA1hash = UtilityFunctions.SHA1(package1.getSessionid() + package1.getUserid() + package1.getAuthid());
                } else {
                    SHA1hash = UtilityFunctions.SHA1(package1.getSessionid() + package1.getUserid() + package1.getAuthid() + trustPayLoad);
                }
                String integritycheck = new String(Base64.encode(SHA1hash));
                AxiomStatus status = m_port.initTransaction(package1, integritycheck, trustPayLoad);
                return status;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public TransactionStatus getStatus(String sessionId, String authId, String trustPayLoad) {
        try {
            if (sessionId != null) {
                byte[] SHA1hash = null;
                if (trustPayLoad == null) {
                    SHA1hash = UtilityFunctions.SHA1(sessionId + authId);
                } else {
                    SHA1hash = UtilityFunctions.SHA1(sessionId + authId + trustPayLoad);
                }
                String integritycheck = new String(Base64.encode(SHA1hash));
                TransactionStatus status = m_port.getStatus(sessionId, authId, integritycheck, trustPayLoad);
                return status;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public AxiomStatus sendNotification(String sessionid, String userid, int type, List<String> l, String trustPayLoad) {
        try {
            // Logger.WriteLog(LogLevelL4N.INFO, "GSending notification");
            String[] data = null;

            byte[] SHA1hash = null;
            if (trustPayLoad == null) {
                SHA1hash = UtilityFunctions.SHA1(sessionid + userid + type);
            } else {
                SHA1hash = UtilityFunctions.SHA1(sessionid + userid + type + trustPayLoad);
            }
            String integritycheck = new String(Base64.encode(SHA1hash));

            AxiomStatus astatus = m_port.sendNotification(sessionid, userid, type, l, integritycheck, trustPayLoad);

            return astatus;

        } catch (Exception ex) {
            ex.printStackTrace();
            // Logger.WriteLog(LogLevelL4N.ERROR, ex.Message.ToString());

        }
        return null;
    }

    public AxiomTokenData ActivateToken(String sessionid, RssUserCerdentials credentials, String regcode, String trustPayLoad) {
        try {
            byte[] SHA1hash = null;
            if (trustPayLoad == null) {

                SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.getRssUser().getUserId() + regcode);
            } else {
                SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.getRssUser().getUserId() + regcode + trustPayLoad);
            }
            String integritycheck = new String(Base64.encode(SHA1hash));
            AxiomTokenData AtData = m_port.activateToken(sessionid, credentials, regcode, integritycheck, trustPayLoad);
            return AtData;

        } catch (Exception ex) {
            ex.printStackTrace();

        }
        return null;
    }

    public AxiomStatus signPdf(java.lang.String sessionId, java.lang.String filename, java.lang.String useremailId, java.lang.String emailId, java.lang.String otp, java.lang.String referenceId, java.lang.String latitude, java.lang.String longitude) throws AxiomException {
        AxiomStatus as = null;
        try {
            as = m_port.signPdf(sessionId, filename, useremailId, emailId, otp, referenceId, latitude, longitude);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return as;
    }

    public RssUserCerdentials getRSSUser(java.lang.String sessionid, java.lang.String keyword, int type, java.lang.String integrityCheckString, java.lang.String trustPayLoad) {
        RssUserCerdentials as = null;
        try {
            as = m_port.getRSSUser(sessionid, keyword, type, integrityCheckString, trustPayLoad);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return as;
    }

    public AxiomStatus createRSSUser(java.lang.String sessionid, com.mollatech.axiom.v2.core.rss.RssUserDetails rssUser, java.lang.String integrityCheckString, java.lang.String trustPayLoad) {
        AxiomStatus as = null;
        try {
            as = m_port.createRSSUser(sessionid, rssUser, integrityCheckString, trustPayLoad);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return as;
    }

    public String generateSignatureCodeRSS(java.lang.String sessionid, java.lang.String userid, java.util.List<java.lang.String> data, java.lang.String clientip) throws AxiomException {

        String signCode = null;
        try {
            signCode = m_port.generateSignatureCodeRSS(sessionid, userid, data, clientip);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return signCode;

    }

    public AxiomData verifyCredentialsAndSignTransactionRSS(java.lang.String sessionid, java.lang.String userid, java.lang.String sotp, java.lang.String challenge, java.util.List<java.lang.String> data, int type, java.lang.String plusFactor, java.lang.String clientip) {
        AxiomData ad = null;
        try {
            ad = m_port.verifyCredentialsAndSignTransactionRSS(sessionid, userid, sotp, challenge, data, type, plusFactor, clientip);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ad;
    }

    public int updateRemoteSignaturesByRequest(java.lang.String sessionId, java.lang.String refid, java.lang.String deviceid, java.lang.String authorisationDetailsforPdf) {
        AxiomStatus as = null;
        try {
            as = m_port.updateRemoteSignaturesByRequest(sessionId, refid, deviceid, authorisationDetailsforPdf);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return as.getErrorcode();
    }

    public AxiomStatus signPdfWithQR(java.lang.String sessionId, java.lang.String filename, java.lang.String emailId, java.lang.String otp, java.lang.String referenceId, java.lang.String latitude, java.lang.String longitude, java.lang.String qrCodeData, java.lang.String allowedPhoneAndCountry) throws AxiomException {
        AxiomStatus as = null;
        try {

            as = m_port.signPdfWithQR(sessionId, filename, emailId, otp, referenceId, latitude, longitude, qrCodeData, allowedPhoneAndCountry);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return as;
    }

    public Remotesignature[] getAllSignatureByUserid(java.lang.String sessionId, String userid, int type) throws AxiomException {
        Remotesignature[] as = null;
        try {
            List<Remotesignature> al = m_port.getAllSignaturebyUserId(sessionId, userid, type);
            as = new Remotesignature[al.size()];
            for (int i = 0; i < al.size(); i++) {
                as[i] = new Remotesignature();
                as[i] = al.get(i);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return as;
    }

    public int addDownloadRequest(java.lang.String sessionId, java.lang.String refid, java.lang.String deviceid, java.lang.String authorisationDetailsforPdf) {
        AxiomStatus as = null;
        try {
            as = m_port.addDownloadRequest(sessionId, refid, deviceid, authorisationDetailsforPdf);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return as.getErrorcode();
    }

    public AxiomStatus verifyEasyLoginRequest(String sessionId,String userid,String _lattitude,String _longitude){
         AxiomStatus astatus=new AxiomStatus();
          try {
            astatus = m_port.verifyEasyLoginRequest(sessionId,userid,_lattitude,_longitude);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
         return astatus;
     }
   public EasyCheckInSessions getEasyLoginRequest(java.lang.String sessionId, String userid) {
        EasyCheckInSessions elogin = null;
        try {
            elogin = m_port.getEasyLoginRequest(sessionId, userid);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return elogin;
    }
 public EasyCheckInSessions addEasyLoginRequest(java.lang.String sessionId, EasyCheckInSessions eloginsession) {
        EasyCheckInSessions elogin = null;
        try {
            elogin = m_port.addEasyLoginRequest(sessionId, eloginsession);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return elogin;
    }
      
    
    
     public EasyCheckIn getEasyLogin(String sessionId, String EasyloginId) {
        EasyCheckIn login = null;
        try {
            login = m_port.getEasyLogin(sessionId, EasyloginId);        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return login;
    }
     
     
//    public AxiomStatus saveAxiomPushResponce(String Sessionid, String callback, String txid, String type) {
//        try {
//            if (Sessionid != null) {
//              
//                AxiomStatus status = 
//                        m_port.saveAxiomPushResponce(Sessionid, callback, txid,type);
//                return status;
//            } else {
//                return null;
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
    
    
    
}
