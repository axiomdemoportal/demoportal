/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.web.token;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ashish
 */
public class SetAuthType extends HttpServlet {

    public final static int PUSH = 1;
    public final static int WITHOUTPUSH = 15;
    public final static int WEBTOKEN = 3;
//    public final static int PKI = 4;
    public final static int PDF_SIGNING = 4;
    public final static int QnA = 9;
    public final static int securePhrase = 6;
    public final static int esigner = 5;
    public final static int Tokens = 7;
    public final static int SIGN_TX = 2;
    public final static int NOPASSWORD = 10;
    public final static int PDF_AUTH = 8;
    public final static int WEB_SEAL = 11;
    public final static int PAM = 12;
    public final static int LOC_AWARE_EASY_LOGIN = 17;
    public final static int EPIN = 18;
    public final static int WEBPUSH=22;
    public final static int FBIO=23;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getSession(true);
        int type = Integer.parseInt(request.getParameter("type"));
        request.getSession().setAttribute("type", type);

        if (type == PUSH) {
            response.sendRedirect("./index.jsp");
        } else if (type == WITHOUTPUSH) {
            response.sendRedirect("./loginWithoutPush.jsp");
        } else if (type == securePhrase) {
            response.sendRedirect("./loginWithoutPush.jsp");
        } else if (type == WEBTOKEN) {
            response.sendRedirect("./loginWithoutPush.jsp");
        } else if (type == PDF_SIGNING) {
            response.sendRedirect("./loginWithoutPush.jsp");
        } else if (type == QnA) {
            response.sendRedirect("./loginWithoutPush.jsp");
        } else if (type == esigner) {
            response.sendRedirect("./loginWithoutPush.jsp");
        } else if (type == Tokens) {
            response.sendRedirect("./loginWithoutPush.jsp");
        } else if (type == SIGN_TX) {
            response.sendRedirect("./signtx.jsp");
        } else if (type == NOPASSWORD) {
            response.sendRedirect("./loginWithPush.jsp");
        } else if (type == PDF_AUTH) {
            response.sendRedirect("./loginWithoutPush.jsp");
        } else if (type == WEB_SEAL) {
            response.sendRedirect("./loginWithoutPush.jsp");
        } else if (type == PAM) {
            response.sendRedirect("./pamDemo.jsp");
        } else if (type == LOC_AWARE_EASY_LOGIN) {
            response.sendRedirect("./Easylogin.jsp");
        } else if (type == EPIN) {
            response.sendRedirect("./loginWithoutPush.jsp");
        } else if (type == WEBPUSH) {
            response.sendRedirect("./loginWithoutPush.jsp");
        }else if(type == FBIO){
            response.sendRedirect("./loginWithoutPush.jsp");
        }

    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
