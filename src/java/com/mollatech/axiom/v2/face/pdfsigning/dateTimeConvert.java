/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.pdfsigning;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author Ideasventure
 */
public class dateTimeConvert {
    public static XMLGregorianCalendar convertStringToXmlGregorian(String dateString) throws DatatypeConfigurationException, ParseException
{
        // Optimize exception handling
        SimpleDateFormat sdf= new SimpleDateFormat("dd-MM-yy");
    Date date = sdf.parse(dateString);
    GregorianCalendar gc = (GregorianCalendar) GregorianCalendar.getInstance();
    gc.setTime(date);
    return DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
    
}
    
    
}
