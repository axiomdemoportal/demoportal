/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler.ImageAuth;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.bridge.crypto.LoadSettings;
import com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import com.mollatech.web.token.SetAuthType;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class isRegisteredDevice extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        JSONObject payload = new JSONObject();
        int type = (Integer) request.getSession().getAttribute("type");
        boolean securephraseflag=false;
        try {
            String _cookieString = request.getParameter("_cookieString");
            String _lattitude = request.getParameter("_lattitude");
            String _longitude = request.getParameter("_longitude");
            String sessionId = (String) request.getSession().getAttribute("_userSessinId");
            RssUserCerdentials userObj = (RssUserCerdentials) request.getSession().getAttribute("_rssDemoUserCerdentials");
            String _locationChunk = request.getSession().getAttribute("_locationChunk").toString();
            String locationType = request.getSession().getAttribute("locationType").toString();
//            System.out.println(_userName);
//            System.out.println(_lattitude);
//            System.out.println(_longitude);

            payload.put("ip", request.getRemoteAddr());
            payload.put("longi", _longitude);
            payload.put("latti", _lattitude);
            payload.put("txType", AxiomWrapper.LOGIN);
            payload.put("locationChunk", _locationChunk);
            payload.put("locationType", locationType);
            byte[] bytePayload = Base64.encode(payload.toString().getBytes());
            String strPayLoad = new String(bytePayload);
            if (_lattitude.equalsIgnoreCase("latitude")) {
                _lattitude = null;
                _longitude = null;
                strPayLoad = null;
            }

            String result = "success";
            String message = "successful credential verification....";
            //String url = "validation.jsp";
            String url = "";

            int retValue = -1;

            AxiomWrapper axWraper = new AxiomWrapper();
            String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
            String password = LoadSettings.g_sSettings.getProperty("axiom.password");
            String channelid = LoadSettings.g_sSettings.getProperty("axiom.channelid");
//            String sessionId = axWraper.OpenSession(channelid, remotelogin, password);

            if (sessionId == null) {
                result = "error";
                message = "Invalid Credentials!!!";
                url = "index.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }

//            RssUserCerdentials userObj = axWraper.getUserCerdentials(sessionId, _userName, AxiomWrapper.GET_USER_BY_FULLNAME, strPayLoad);
            AxiomCredentialDetails axiomCredentialDetails = null;
            AxiomCredentialDetails axiomCredentalSecurePhrase = null;
            if (userObj != null) {
                if (userObj.getTokenDetails() != null) {
                    for (int i = 0; i < userObj.getTokenDetails().size(); i++) {
                        axiomCredentialDetails = userObj.getTokenDetails().get(i);
                        if (axiomCredentialDetails != null) {
                            if (axiomCredentialDetails.getCategory() == AxiomWrapper.SECURE_PHRASE) {
                                axiomCredentalSecurePhrase = axiomCredentialDetails;
                                
                            }
                        }
                    }
                }
            }
            
            if(axiomCredentalSecurePhrase.getSecurePhraseImage()!=null){
             securephraseflag = true;
            }
            
            String cookieString = null;
            if (axiomCredentalSecurePhrase != null) {

                if (_cookieString != null) {
                    cookieString = axWraper.checkUserCerdentials(sessionId, userObj, _cookieString, retValue, strPayLoad);
                } else {
                    cookieString = axWraper.checkUserCerdentials(sessionId, userObj, null, retValue, strPayLoad);
                }

//                if(cookieString != null){
//                   byte[] b = axWraper.getImage(sessionId, userObj, cookieString, AxiomWrapper.SECURE_PHRASE, strPayLoad);
//                }
//                String _username = userObj.getRssUser().getUserName();
                json.put("_result", result);
                json.put("_message", message);
                boolean flag = false;
                for (int i = 0; i < userObj.getTokenDetails().size(); i++) {
                    axiomCredentialDetails = userObj.getTokenDetails().get(i);
                    if (axiomCredentialDetails != null) {
                        if (axiomCredentialDetails.getCategory() == AxiomWrapper.SOFTWARE_TOKEN
                                && axiomCredentialDetails.getSubcategory() == AxiomWrapper.SW_WEB_TOKEN) {
                            flag = true;
                            break;
                        }
                    }
                }
//                 if(type == SetAuthType.PKI){
//                    for (int i = 0; i < userObj.getTokenDetails().size(); i++) {
//                    axiomCredentialDetails = userObj.getTokenDetails().get(i);
//                    if (axiomCredentialDetails != null) {
//                        if (axiomCredentialDetails.getCategory() == AxiomWrapper.PKI_SOFTWARE_TOKEN
//                               && axiomCredentialDetails.getStatus() == AxiomWrapper.TOKEN_STATUS_ACTIVE) {
//                            flag = true;
//                            break;
//                        }
//                    }
//                } 
//                 }
                if (type == SetAuthType.NOPASSWORD) {
                    url = "./payment.jsp";
                } else if (type == SetAuthType.securePhrase) {
                    if(securephraseflag==true){
                        url = "./2fa.jsp";
                    }                   
                    else{
                        url = "./securephrase.jsp";
                    }

                } else if (type == SetAuthType.WEBTOKEN) {
//                    if (flag == true) {
                        url = "./2faa.jsp";
//                    }
                }
//                else if (type == SetAuthType.PKI) {
//                    if (flag == true) {
//                        url = "./2faa.jsp";
//                    }
//                }
            else if (type == SetAuthType.PDF_SIGNING) {
//                    if (flag == true) {
                        url = "./pdfsigningab.jsp";
//                    }
                }else if (type == SetAuthType.QnA) {
                  
                        url = "./2fa.jsp";
                    
                }else if (type == SetAuthType.esigner) {
                  
                        url = "/face";
                    
                }else if (type == SetAuthType.Tokens) {
                  
                        url = "./allTokens.jsp";
                    
                }else if (type == SetAuthType.SIGN_TX) {
                  
                        url = "./2farss.jsp";
                    
                }else if (type == SetAuthType.PDF_AUTH) {
                  
                        url = "./pdfAuthNew.jsp";
                    
                }
                else if (type == SetAuthType.LOC_AWARE_EASY_LOGIN) {
                    url = "./payment.jsp";
                }else if (type == SetAuthType.EPIN) {                  
                        url = "./2fa.jsp";                    
                } else if (type == SetAuthType.WEBPUSH) {                  
                        url = "./RegisterWebPush.jsp";                    
                }                
//                json.put("_username", _username);
                json.put("_url", url);
//                json.put("_cookieString", cookieString);
                out.print(json);
                out.flush();
                return;
            } else {
                result = "error";
                message = "User not found...!!!";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }
            
            

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
