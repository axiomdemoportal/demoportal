/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.bridge.crypto.LoadSettings;
import com.mollatech.axiom.v2.core.rss.AxiomStatus;
import com.mollatech.axiom.v2.core.rss.RssUserDetails;
import com.mollatech.axiom.v2.core.rss.VerifyRequest;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author parimal
 */
public class createuser extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    
        
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        JSONObject payload = new JSONObject();
//        int type = (Integer) request.getSession().getAttribute("type");

        try {

//            String _userName = (String) request.getSession().getAttribute("_username");
            String _userName = request.getParameter("username");
            String _email = request.getParameter("emailid");
            String _phoneno = request.getParameter("phoneno");

            String _lattitude = request.getParameter("_lattitude");
            String _longitude = request.getParameter("_longitude");

            String _xCoordinate = request.getParameter("_xCoordinate");
            String _yCoordinate = request.getParameter("_yCoordinate");
            String _locationChunk = (String) request.getSession().getAttribute("_locationChunk");
//            int locationType = (Integer) request.getSession().getAttribute("locationType");
//            int ixCoordinate = -1;
//            if (_xCoordinate != null) {
//                ixCoordinate = Integer.parseInt(_xCoordinate);
//            }
//            int iyCoordinate = -1;
//            if (_yCoordinate != null) {
//                iyCoordinate = Integer.parseInt(_yCoordinate);
//            }

            System.out.println(_userName);
            System.out.println(_email);
            System.out.println(_lattitude);
            System.out.println(_longitude);

            VerifyRequest vReq = null;

            payload.put("ip", request.getRemoteAddr());
            payload.put("longi", _longitude);
            payload.put("latti", _lattitude);
            payload.put("txType", AxiomWrapper.GENERATEAUTHENTICATIONPACKAGE);
            payload.put("locationChunk", _locationChunk);
           // payload.put("locationType", locationType);
            byte[] bytePayload = Base64.encode(payload.toString().getBytes());
            String strPayLoad = new String(bytePayload);
//            if (_lattitude.equalsIgnoreCase("latitude")) {
//                _lattitude = null;
//                _longitude = null;
//                strPayLoad = null;
//            }

            String result = "success";
            String message = "successful credential verification....";
            //String url = "validation.jsp";
            String url = "./selectAuthType.jsp";
            

            int retValue = -1;
 
            AxiomWrapper axWraper = new AxiomWrapper();
            String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
            String password = LoadSettings.g_sSettings.getProperty("axiom.password");
            String channelid = LoadSettings.g_sSettings.getProperty("axiom.channelid");
            String sessionId = axWraper.OpenSession(channelid, remotelogin, password);

//            String sessionId = (String) request.getSession().getAttribute("_userSessinId");

            if (sessionId == null) {
                result = "error";
                message = "Invalid Credentials!!!";
                url = "index.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }
            RssUserDetails rssObj=new RssUserDetails();
            rssObj.setUserId(_email); 
            rssObj.setUserName(_userName);
            rssObj.setEmailid(_email);
            rssObj.setPhoneNumber(_phoneno);
            rssObj.setOrganisationUnit("Business Development");
            rssObj.setCountry("MY");
            rssObj.setLocation("KL");
           rssObj.setOrganisation("MSC Trustgate.com Sdn Bhd.");

             AxiomStatus as=axWraper.createRSSUser(sessionId, rssObj, null, strPayLoad);
             if(as.getErrorcode()==0){
                 
                 json.put("_result", "success");
                 json.put("_message", "user created successfully!!!");
                 json.put("_url",url);
                 out.print(json);
                 out.flush();
                  
            }else if(as.getErrorcode() == -242){
                 json.put("_result", "error");
                 json.put("_message", "Phone number already used!!!");
                 out.print(json);
                 out.flush();
            }else if(as.getErrorcode() == -240){
                 json.put("_result", "error");
                 json.put("_message", "Email ID already used!!!");
                 out.print(json);
                 out.flush();
            }else if (retValue == -1) {
                json.put("_result", "error");
                json.put("_message", "Duplicate user entry!!!");
                out.print(json);
                out.flush();               
            }else{
                json.put("_result", "error");
                json.put("_message", "User Creation Failed!!!");
                out.print(json);
                out.flush();  
            }
//            RssUserCerdentials userObj = axWraper.getUserCerdentials(sessionId, _userName, AxiomWrapper.GET_USER_BY_FULLNAME, strPayLoad);

        }catch(Exception e){
            e.printStackTrace();
        
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
