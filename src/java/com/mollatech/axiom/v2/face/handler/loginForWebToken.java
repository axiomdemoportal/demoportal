/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.face.handler;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.bridge.crypto.LoadSettings;
import com.mollatech.axiom.v2.core.rss.AuthenticationPackage;
import com.mollatech.axiom.v2.core.rss.AxiomChallengeResponse;
import com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails;
import com.mollatech.axiom.v2.core.rss.AxiomQuestionAndAnswer;
import com.mollatech.axiom.v2.core.rss.AxiomStatus;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import com.mollatech.axiom.v2.core.rss.VerifyRequest;
import com.mollatech.web.token.SetAuthType;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
public class loginForWebToken extends HttpServlet {

    final String itemType = "LOGIN";
    int SUCCESS = 0;
    int FAILED = -1;
    String BLOCKED = "BLOCKED_IP";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        JSONObject payload = new JSONObject();
        int type = (Integer) request.getSession().getAttribute("type");

        try {

            String _userName = (String) request.getSession().getAttribute("_username");
            //String _userName = request.getParameter("_name");
            String _userpassword = request.getParameter("txtotp");
            RssUserCerdentials userObj = (RssUserCerdentials) request.getSession().getAttribute("_rssDemoUserCerdentials");
            String _lattitude = request.getParameter("_lattitude");
            String _longitude = request.getParameter("_longitude");

            String _xCoordinate = request.getParameter("_xCoordinate");
            String _yCoordinate = request.getParameter("_yCoordinate");
            String _locationChunk = (String) request.getSession().getAttribute("_locationChunk");
            int locationType = (Integer) request.getSession().getAttribute("locationType");
            int ixCoordinate = -1;
            if (_xCoordinate != null) {
                ixCoordinate = Integer.parseInt(_xCoordinate);
            }
            int iyCoordinate = -1;
            if (_yCoordinate != null) {
                iyCoordinate = Integer.parseInt(_yCoordinate);
            }

            System.out.println(_userName);
            System.out.println(_userpassword);
            System.out.println(_lattitude);
            System.out.println(_longitude);

            VerifyRequest vReq = null;

            payload.put("ip", request.getRemoteAddr());
            payload.put("longi", _longitude);
            payload.put("latti", _lattitude);
            payload.put("txType", AxiomWrapper.GENERATEAUTHENTICATIONPACKAGE);
            payload.put("locationChunk", _locationChunk);
            payload.put("locationType", locationType);
            byte[] bytePayload = Base64.encode(payload.toString().getBytes());
            String strPayLoad = new String(bytePayload);
            if (_lattitude.equalsIgnoreCase("latitude")) {
                _lattitude = null;
                _longitude = null;
                strPayLoad = null;
            }

            String result = "success";
            String message = "successful credential verification....";
            //String url = "validation.jsp";
            String url = "./home.jsp";
            if (type == SetAuthType.QnA) {
                _userpassword = "";
            } else if (type == SetAuthType.EPIN) {
                _userpassword = "";
            }
            if (type == SetAuthType.securePhrase) {
                _userpassword = "";
            }
            if (_userName == null) {
                result = "error";
                message = "Invalid Credentials!! 1st";
                url = "index.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }

            int retValue = -1;

            AxiomWrapper axWraper = new AxiomWrapper();
            String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
            String password = LoadSettings.g_sSettings.getProperty("axiom.password");
            String channelid = LoadSettings.g_sSettings.getProperty("axiom.channelid");
//            String sessionId = axWraper.OpenSession(channelid, remotelogin, password);

            String sessionId = (String) request.getSession().getAttribute("_userSessinId");

            if (sessionId == null) {
                result = "error";
                message = "Invalid Credentials!!!";
                url = "index.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }

//            RssUserCerdentials userObj = axWraper.getUserCerdentials(sessionId, _userName, AxiomWrapper.GET_USER_BY_FULLNAME, strPayLoad);
            //RssUserCerdentials userObj = axWraper.getUserCerdentials(sessionId, _userName, AxiomWrapper.GET_USER_BY_FULLNAME, strPayLoad);
            AxiomCredentialDetails axiomCredentialDetails = null;
//            AxiomCredentialDetails axiomCredentalSecurePhrase = null;
            if (userObj != null) {
                if (userObj.getTokenDetails() != null) {
                    for (int i = 0; i < userObj.getTokenDetails().size(); i++) {
                        axiomCredentialDetails = userObj.getTokenDetails().get(i);
                        if (axiomCredentialDetails != null && type != SetAuthType.QnA) {
                            if (axiomCredentialDetails.getCategory() == AxiomWrapper.SECURE_PHRASE) {
                                axiomCredentialDetails = userObj.getTokenDetails().get(i);
                                axiomCredentialDetails.setXCoordinate(ixCoordinate);
                                axiomCredentialDetails.setYCoordinate(iyCoordinate);
                                userObj.getTokenDetails().set(i, axiomCredentialDetails);
                            }
                        } else if (axiomCredentialDetails != null && type != SetAuthType.QnA) {
                            if (axiomCredentialDetails.getCategory() == AxiomWrapper.SECURE_PHRASE) {
                                axiomCredentialDetails = userObj.getTokenDetails().get(i);
                                axiomCredentialDetails.setXCoordinate(ixCoordinate);
                                axiomCredentialDetails.setYCoordinate(iyCoordinate);
                                userObj.getTokenDetails().set(i, axiomCredentialDetails);
                            }
                        }
                        if (type == SetAuthType.QnA) {
                            if (axiomCredentialDetails.getCategory() == AxiomWrapper.CHALLAEGE_RESPONSE) {
                                axiomCredentialDetails = userObj.getTokenDetails().get(i);
                                AxiomChallengeResponse challengeResponse = (AxiomChallengeResponse) request.getSession().getAttribute("challengeResponse");
                                for (int j = 0; j < challengeResponse.getWebQAndA().size(); j++) {
                                    AxiomQuestionAndAnswer answer = (AxiomQuestionAndAnswer) challengeResponse.getWebQAndA().get(j);
                                    answer.setAnswerByUser(request.getParameter("answer" + answer.getQuestionid()));
                                    challengeResponse.getWebQAndA().set(j, answer);
                                }
                                vReq = new VerifyRequest();
                                vReq.setQas(challengeResponse);
                                axiomCredentialDetails.setQas(challengeResponse);
                                userObj.getTokenDetails().set(i, axiomCredentialDetails);
                            }
                        } else if (type == SetAuthType.EPIN) {
                            if (axiomCredentialDetails.getCategory() == AxiomWrapper.CHALLAEGE_RESPONSE) {
                                axiomCredentialDetails = userObj.getTokenDetails().get(i);
                                AxiomChallengeResponse challengeResponse = (AxiomChallengeResponse) request.getSession().getAttribute("challengeResponse");
                                for (int j = 0; j < challengeResponse.getWebQAndA().size(); j++) {
                                    AxiomQuestionAndAnswer answer = (AxiomQuestionAndAnswer) challengeResponse.getWebQAndA().get(j);
                                    answer.setAnswerByUser(request.getParameter("answer" + answer.getQuestionid()));
                                    challengeResponse.getWebQAndA().set(j, answer);
                                }
                                vReq = new VerifyRequest();
                                vReq.setQas(challengeResponse);
                                axiomCredentialDetails.setQas(challengeResponse);
                                userObj.getTokenDetails().set(i, axiomCredentialDetails);
                            }
                        }
                    }
                }
            }

//            AuthenticationPackage authPackage = axWraper.generateAuthenticationPackage(sessionId, userObj, "cookie", strPayLoad);
            AuthenticationPackage authPackage = axWraper.generateAuthenticationPackage(sessionId, userObj, "cookie", strPayLoad);

            if (authPackage != null) {
                System.out.println("Geo Check User =" + authPackage.getGeoCheck());
                System.out.println("Valid Image =" + authPackage.getGetValidImage());
                System.out.println("Ip Check =" + authPackage.getIpCheck());
                System.out.println("Register Device =" + authPackage.getRegisteredDevice());
                System.out.println("Registered User =" + authPackage.getRegisteredUser());
                System.out.println("Sweet Spot =" + authPackage.getSweetSpot());

                if (type == SetAuthType.securePhrase) {
                    if (authPackage.getGetValidImage() != 0) {
                        result = "error";
                        message = "Invalid Image...!!!";
                        url = "index.jsp";
                        json.put("_result", result);
                        json.put("_message", message);
                        json.put("_url", url);
                        out.print(json);
                        out.flush();
                        return;
                    }
                }

                if (authPackage.getGeoCheck() == 0) {
                    request.getSession().setAttribute("geoCheck", "success");
                } else {
                    request.getSession().setAttribute("geoCheck", "error");
                }
                if (authPackage.getGetValidImage() == 0) {
                    request.getSession().setAttribute("validImage", "success");
                } else {
                    request.getSession().setAttribute("validImage", "error");
                }
                if (authPackage.getIpCheck() == 0) {
                    request.getSession().setAttribute("IpCheck", "success");
                } else {
                    request.getSession().setAttribute("IpCheck", "error");
                }
                if (authPackage.getRegisteredDevice() == 0) {
                    request.getSession().setAttribute("DeviceCheck", "success");
                } else {
                    request.getSession().setAttribute("DeviceCheck", "error");
                }

                if (authPackage.getRegisteredUser() == 0) {
                    request.getSession().setAttribute("UserCheck", "success");
                } else {
                    request.getSession().setAttribute("UserCheck", "error");
                }
                if (authPackage.getSweetSpot() == 0) {
                    request.getSession().setAttribute("SweetSpotCheck", "success");
                } else {
                    request.getSession().setAttribute("SweetSpotCheck", "error");
                }

            }

//            AxiomCredentialDetails axiomCredentialDetails = null;
            if (userObj != null) {

                if (userObj.getTokenDetails() != null) {
                    for (int i = 0; i < userObj.getTokenDetails().size(); i++) {
                        axiomCredentialDetails = userObj.getTokenDetails().get(i);
                        if (axiomCredentialDetails != null) {
                            if (axiomCredentialDetails.getCategory() == AxiomWrapper.PASSWORD) {
                                if (axiomCredentialDetails.getStatus() == AxiomWrapper.TOKEN_STATUS_ACTIVE) {
                                    retValue = 0;
                                    break;
                                } else {
                                    result = "error";
                                    message = "User status is not Active...!!!";
                                    url = "index.jsp";
                                    json.put("_result", result);
                                    json.put("_message", message);
                                    json.put("_url", url);
                                    out.print(json);
                                    out.flush();
                                    return;
                                }
                            }
                        }

                    }
                } else {
                    result = "error";
                    message = "User not found...!!!";
                    url = "index.jsp";
                    json.put("_result", result);
                    json.put("_message", message);
                    json.put("_url", url);
                    out.print(json);
                    out.flush();
                    return;
                }
            } else {
                result = "error";
                message = "User not found...!!!";
                url = "index.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }

//            VerifyRequest vReq = new VerifyRequest();
//            vReq.setCategory(axWraper.Password);
//            vReq.setCredential(_userpassword);
            AxiomStatus aStatus = null;
            if (type == SetAuthType.WEBTOKEN) {
                vReq = new VerifyRequest();
                vReq.setCategory(AxiomWrapper.OTP);
                vReq.setCredential(_userpassword);
                aStatus = axWraper.verifyCredential(sessionId, userObj.getRssUser().getUserId(), vReq, strPayLoad);
            } else if (type == SetAuthType.QnA) {
                vReq.setCategory(AxiomWrapper.CHALLAEGE_RESPONSE);
                aStatus = axWraper.verifyCredential(sessionId, userObj.getRssUser().getUserId(), vReq, strPayLoad);
            } else if (type == SetAuthType.EPIN) {
                vReq.setCategory(AxiomWrapper.CHALLAEGE_RESPONSE);
                aStatus = axWraper.verifyCredential(sessionId, userObj.getRssUser().getUserId(), vReq, strPayLoad);
            } else if (type == SetAuthType.securePhrase) {
                if (authPackage.getSweetSpot() == 0) {

                    aStatus = new AxiomStatus();
                    aStatus.setError("success");
                    aStatus.setErrorcode(0);
                } else {
                    aStatus = new AxiomStatus();
                    aStatus.setError("success");
                    aStatus.setErrorcode(0);
                }
//                aStatus = axWraper.verifyCredential(sessionId, userObj.getRssUser().getUserId(), vReq, strPayLoad);
            } else if (aStatus == null) {
                vReq = new VerifyRequest();
                vReq.setCategory(AxiomWrapper.Password);
                vReq.setCredential(_userpassword);
                aStatus = axWraper.verifyCredential(sessionId, userObj.getRssUser().getUserId(), vReq, strPayLoad);
            } else if (aStatus.getErrorcode() == 0) {
                vReq = new VerifyRequest();
                vReq.setCategory(AxiomWrapper.Password);
                vReq.setCredential(_userpassword);
                aStatus = axWraper.verifyCredential(sessionId, userObj.getRssUser().getUserId(), vReq, strPayLoad);
            }
//            if (aStatus.getErrorcode() != 0) {
//                vReq.setCategory(AxiomWrapper.OTP_TOKEN_SOFTWARE_MOBILE);
//                vReq.setCredential(_userpassword);
//                aStatus = axWraper.verifyCredential(sessionId, userObj.getRssUser().getUserId(), vReq, strPayLoad);
//            }

            if (authPackage.getSweetSpot() == 0 && type == SetAuthType.securePhrase) {

                if (aStatus.getErrorcode() == 0) {
                    HttpSession session = request.getSession(true);
                    session.setAttribute("_apOprAuth", "yes");
                    session.setAttribute("_rssUserCerdentials", userObj);
                    session.setAttribute("_userSessinId", sessionId);
                    session.setAttribute("_lattitude", _lattitude);
                    session.setAttribute("_longitude", _longitude);
                    json.put("_result", result);
                    json.put("_message", message);
                    json.put("_url", url);
                    out.print(json);
                    out.flush();
                    return;
                } else {
                    url = "index.jsp";
                    result = "error";
                    message = "Invalid credentials!!!";
                    json.put("_result", result);
                    json.put("_message", message);
                    json.put("_url", url);
                    out.print(json);
                    out.flush();
                    return;
                }
            } else if (aStatus.getErrorcode() == 0) {
                HttpSession session = request.getSession(true);
                session.setAttribute("_apOprAuth", "yes");
                session.setAttribute("_rssUserCerdentials", userObj);
                session.setAttribute("_userSessinId", sessionId);
                session.setAttribute("_lattitude", _lattitude);
                session.setAttribute("_longitude", _longitude);
                if (type == SetAuthType.EPIN) {
                    AxiomWrapper wrapper = new AxiomWrapper();
                    AxiomStatus status = wrapper.sendNotification(sessionId, userObj.getRssUser().getUserId(), type, null, strPayLoad);
//                   if(status)
                    result = Integer.toString(status.getErrorcode());
                    message = status.getError();
                    url="#";
                }
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            } else {
                url = "index.jsp";
                result = "error";
                message = "Authentication failed!!!";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
