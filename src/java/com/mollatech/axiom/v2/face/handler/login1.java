package com.mollatech.axiom.v2.face.handler;

import axiom.web.service.AxiomWrapper;
import com.mollatech.axiom.bridge.crypto.LoadSettings;
import com.mollatech.axiom.v2.core.rss.AxiomCredentialDetails;
import com.mollatech.axiom.v2.core.rss.AxiomStatus;
import com.mollatech.axiom.v2.core.rss.RssUserCerdentials;
import com.mollatech.axiom.v2.core.rss.VerifyRequest;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

public class login1 extends HttpServlet {

    final String itemType = "LOGIN";
    int SUCCESS = 0;
    int FAILED = -1;
    String BLOCKED = "BLOCKED_IP";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        JSONObject payload = new JSONObject();
        String tokenToUse=request.getParameter("tokenTypeuse");
      
        try {

            String _userName = (String) request.getSession().getAttribute("_username");
            //String _userName = request.getParameter("_name");
            String _userpassword = request.getParameter("password");
              String typeOfToken = request.getParameter("tokenTypeuse");
            String _lattitude = request.getParameter("_lattitude");
            String _longitude = request.getParameter("_longitude");
            String _agent = request.getParameter("_agent");
          //  String _xCoordinate = request.getParameter("_xCoordinate");
            //String _yCoordinate = request.getParameter("_yCoordinate");
            String _locationChunk = (String) request.getSession().getAttribute("_locationChunk");
            int locationType = (Integer) request.getSession().getAttribute("locationType");
            
           // int ixCoordinate = -1;
           // if (_xCoordinate != null) {
//                ixCoordinate = Integer.parseInt(_xCoordinate);
//            }
//            int iyCoordinate = -1;
//            if (_yCoordinate != null) {
//                iyCoordinate = Integer.parseInt(_yCoordinate);
//            }

            System.out.println(_userName);
            System.out.println(_userpassword);
            System.out.println(_lattitude);
            System.out.println(_longitude);

            payload.put("ip", request.getRemoteAddr());
            payload.put("longi", _longitude);
            payload.put("latti", _lattitude);
            payload.put("txType", AxiomWrapper.GENERATEAUTHENTICATIONPACKAGE);
            payload.put("locationChunk", _locationChunk);
            payload.put("locationType", locationType);
            byte[] bytePayload = Base64.encode(payload.toString().getBytes());
            String strPayLoad = new String(bytePayload);
            if (_lattitude.equalsIgnoreCase("latitude")) {
                _lattitude = null;
                _longitude = null;
                strPayLoad = null;
            }

            String result = "success";
            String message = "successful credential verification....";
            //String url = "validation.jsp";
            String url = "./home.jsp";

            if (_userName == null || _userpassword == null) {
                result = "error";
                message = "Invalid Credentials!! 1st";
                url = "index.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }

            int retValue = -1;

            AxiomWrapper axWraper = new AxiomWrapper();
            String remotelogin = LoadSettings.g_sSettings.getProperty("axiom.remotelogin");
            String password = LoadSettings.g_sSettings.getProperty("axiom.password");
            String channelid = LoadSettings.g_sSettings.getProperty("axiom.channelid");
//            String sessionId = axWraper.OpenSession(channelid, remotelogin, password);

            String sessionId = (String) request.getSession().getAttribute("_userSessinId");

            if (sessionId == null) {
                result = "error";
                message = "Invalid Credentials!!!";
                url = "index.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }

//            RssUserCerdentials userObj = axWraper.getUserCerdentials(sessionId, _userName, AxiomWrapper.GET_USER_BY_FULLNAME, strPayLoad);
            RssUserCerdentials userObj = axWraper.getUserCerdentials(sessionId, _userName, AxiomWrapper.GET_USER_BY_FULLNAME, strPayLoad);
            AxiomCredentialDetails axiomCredentialDetails = null;
//            AxiomCredentialDetails axiomCredentalSecurePhrase = null;
           
         
//            AuthenticationPackage authPackage = axWraper.generateAuthenticationPackage(sessionId, userObj, "cookie", strPayLoad);
//            AuthenticationPackage authPackage = axWraper.generateAuthenticationPackage(sessionId, userObj, "cookie", strPayLoad);
//            if (authPackage != null) {
//                System.out.println("Geo Check User =" + authPackage.getGeoCheck());
//                System.out.println("Valid Image =" + authPackage.getGetValidImage());
//                System.out.println("Ip Check =" + authPackage.getIpCheck());
//                System.out.println("Register Device =" + authPackage.getRegisteredDevice());
//                System.out.println("Registered User =" + authPackage.getRegisteredUser());
//                System.out.println("Sweet Spot =" + authPackage.getSweetSpot());
//
//                if (authPackage.getGetValidImage() != 0) {
//                    result = "error";
//                    message = "Invalid Image...!!!";
//                    url = "index.jsp";
//                    json.put("_result", result);
//                    json.put("_message", message);
//                    json.put("_url", url);
//                    out.print(json);
//                    out.flush();
//                    return;
//                }
//
//            }
         //   }
//            AxiomCredentialDetails axiomCredentialDetails = null;
            if (userObj != null) {

                if (userObj.getTokenDetails() != null) {
                    for (int i = 0; i < userObj.getTokenDetails().size(); i++) {
                        axiomCredentialDetails = userObj.getTokenDetails().get(i);
                        if (axiomCredentialDetails != null) {
                            if(tokenToUse.equals("password"))
                            {
                            if (axiomCredentialDetails.getCategory() == AxiomWrapper.PASSWORD) {
                                if (axiomCredentialDetails.getStatus() == AxiomWrapper.TOKEN_STATUS_ACTIVE) {
                                    retValue = 0;
                                    break;
                                }
                                else {
                                    result = "error";
                                    message = "User status is not Active...!!!";
                                    url = "index.jsp";
                                    json.put("_result", result);
                                    json.put("_message", message);
                                    json.put("_url", url);
                                    out.print(json);
                                    out.flush();
                                    return;
                                }
                            }
                            }
                            else if(tokenToUse.equals("OOB"))
                            {
                               if (axiomCredentialDetails.getCategory() == AxiomWrapper.OUTOFBOUND_TOKEN) {
                                if (axiomCredentialDetails.getStatus() == AxiomWrapper.TOKEN_STATUS_ACTIVE) {
                                    retValue = 0;
                                    break;
                                }else {
                                    result = "error";
                                    message = "User status is not Active...!!!";
                                    url = "index.jsp";
                                    json.put("_result", result);
                                    json.put("_message", message);
                                    json.put("_url", url);
                                    out.print(json);
                                    out.flush();
                                    return;
                                }
                                
                            }
                            }else if(tokenToUse.equals("MobileMobile"))
                            {
                                 if (axiomCredentialDetails.getCategory() == AxiomWrapper.SOFTWARE_TOKEN) {
                                if (axiomCredentialDetails.getStatus() == AxiomWrapper.TOKEN_STATUS_ACTIVE) {
                                    retValue = 0;
                                    break;
                                }else {
                                    result = "error";
                                    message = "User status is not Active...!!!";
                                    url = "index.jsp";
                                    json.put("_result", result);
                                    json.put("_message", message);
                                    json.put("_url", url);
                                    out.print(json);
                                    out.flush();
                                    return;
                                }
                                
                            }
                            }
                        }   else {
                                    result = "error";
                                    message = "User status is not Active...!!!";
                                    url = "index.jsp";
                                    json.put("_result", result);
                                    json.put("_message", message);
                                    json.put("_url", url);
                                    out.print(json);
                                    out.flush();
                                    return;
                                }
                            }
                        }
    
                  else {
                    result = "error";
                    message = "User not found...!!!";
                    url = "index.jsp";
                    json.put("_result", result);
                    json.put("_message", message);
                    json.put("_url", url);
                    out.print(json);
                    out.flush();
                    return;
                }
            } else {
                result = "error";
                message = "User not found...!!!";
                url = "index.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }

            VerifyRequest vReq = new VerifyRequest();
            if(tokenToUse.equals("password"))
            {
            vReq.setCategory(axWraper.OTP);
            }
            else if(tokenToUse.equals("OOB"))
            {
            vReq.setCategory(AxiomWrapper.OTP);
            }
            else if(tokenToUse.equals("Mobile"))
            {
                vReq.setCategory(AxiomWrapper.OTP);
            }
            else if(tokenToUse.equals("Hardware"))
            {
             vReq.setCategory(AxiomWrapper.OTP);
            }
            vReq.setCredential(_userpassword);
//            AxiomStatus aStatus = axWraper.verifyCredential(sessionId, userObj.getRssUser().getUserId(), vReq, strPayLoad);
            AxiomStatus aStatus = axWraper.verifyCredential(sessionId, userObj.getRssUser().getUserId(), vReq, strPayLoad);

//            if (aStatus.getErrorcode() != 0) {
//                vReq.setCategory(axWraper.OTP_TOKEN_SOFTWARE_MOBILE);
//                vReq.setCredential(_userpassword);
//          aStatus = axWraper.verifyCredential(sessionId, userObj.getRssUser().getUserId(), vReq, strPayLoad);
//            }

            if (aStatus.getErrorcode() == 0) {
                HttpSession session = request.getSession(true);
                session.setAttribute("_apOprAuth", "yes");
                session.setAttribute("_rssUserCerdentials", userObj);
                session.setAttribute("_userSessinId", sessionId);
                session.setAttribute("_lattitude", _lattitude);
                session.setAttribute("_longitude", _longitude);
                json.put("_result", result);
                json.put("_message", message);
                json.put("_message1","Geolocation Verified Successfully!!!!!!!!");
                json.put("_message2","User Verified Successfully(Credential)!!!!!!!!");
                json.put("_url", url);
                request.getSession().setAttribute("OTPVerificationResult",json);
                out.print(json);
                out.flush();
                return;
            } else { 
                url = "index.jsp";
                result = ""+aStatus.getErrorcode();
                message = aStatus.getRegCodeMessage();
                json.put("_result", "error");
                json.put("_result1", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            } 

        } catch (Exception ex) {
            ex.printStackTrace();
        }
//            finally {
//            out.print(json);
//            out.flush();
//        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
