/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.push;

import axiom.web.service.AxiomPushWrapper;
import axiom.web.service.AxiomWrapper;
import static com.mollatech.axiom.push.savePushWatchDetails.log;
import com.mollatech.axiom.v2.core.rss.AxiomStatus;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;

/**
 *
 * @author Pramod
 */
public class saveWebPushResponce extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Servlet started");
         HttpSession session = request.getSession();
        response.setContentType("application/json");
//           String txId = (String) session.getAttribute("txid");
//           String sessionId= (String) session.getAttribute("_userSessinId");
             
                   String txId = (String)request.getServletContext().getAttribute("txid");
                    String sessionId = (String)request.getServletContext().getAttribute("sessionId");

           
        log.debug("txId :: "+txId);
        String type = request.getParameter("repType");
        log.debug("Responce Type(Approved/Denied) :: "+type);
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String result = null;
        String message = null;
         AxiomStatus retValue = null;
 
            if (txId == null){
                           
                 result = "error";
                message = "Transaction Id can not be null.!!";
                 try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    log.error("Exception caught :: ",e);
                }
                out.print(json);
                out.flush();
                return;
            }
  
              

          AxiomWrapper awraper=new AxiomWrapper();
        
          //retValue = awraper.saveAxiomPushResponce(sessionId, null, txId, type);
        String resultString = "Failure";
        if(retValue != null){
        switch (retValue.getErrorcode()) {
            case 0:
                resultString = "Success";
                result = "Success";
                message = "Push Responce Updated successfuly..!!";
                break;
            case -6:
                resultString = "Success";
                result = "Success";
                message = "Already Responded..!!";
                break;
            case -2:
                resultString = "Success";
                result = "Success";
                message = "Transaction Session Expired..!!";
                break;
            default:
                result = "error";
                message = "failed to update Push Responce!!";
                break;
        }
           try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
            return;
        }
        }else{
               result = "error";
                message = "failed to update Push Responce!!";
        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception e) {
            log.error("Exception caught :: ",e);
        } finally {
            out.print(json);
            out.flush();
            log.info("Servlet ended");
            return;
        }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
