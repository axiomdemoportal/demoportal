/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.MessageDigest;

/**
 *
 * @author ashish
 */
public class AxiomProtectConnector {

    public static byte[] Bas64SHA1Inner(String message) {
        byte[] base64String = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.update(message.getBytes());
            byte[] hashValue = md.digest();
            base64String = hashValue;

        } catch (Exception e) {
            return null;
        }
        return base64String;
    }
    
    public static Object deserializeFromObject(ByteArrayInputStream binaryObject) {
        ObjectInputStream ins;
        try {
            ins = new ObjectInputStream(binaryObject);
            Object object = (Object) ins.readObject();
            ins.close();
            return object;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public static byte[] serializeToObject(Object objectToSerialize) {
        ObjectOutputStream oos = null;
        byte[] data = null;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(bos);
            oos.writeObject(objectToSerialize);
            oos.flush();
            oos.close();
            bos.close();
            data = bos.toByteArray();
            return data;
        } catch (IOException ex) {
            ex.printStackTrace();

        } finally {
            try {
                oos.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return data;
    }

}
