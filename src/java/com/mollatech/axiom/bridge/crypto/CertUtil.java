package com.mollatech.axiom.bridge.crypto;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.Security;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import javax.security.auth.x500.X500Principal;

import org.bouncycastle.jce.PKCS10CertificationRequest;
import org.bouncycastle.jce.PrincipalUtil;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.x509.X509V3CertificateGenerator;

public class CertUtil {

    static {
        
        Provider p = Security.getProvider("BC");
        if ( p == null )
        Security.addProvider(new BouncyCastleProvider()); // add it
    }
    private Certificate m_myCert = null;
    private PrivateKey m_myPrivateKey = null;
    private PublicKey m_myPubKey = null;

    public PrivateKey getPrivateKey() {
        return m_myPrivateKey;
    }

    public Certificate getCert() {
        return m_myCert;
    }

    public KeyPair generateKeyPair(int iStrength) throws NoSuchAlgorithmException, NoSuchProviderException {
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA", "BC");
        kpg.initialize(iStrength); //1024,2048....
        KeyPair kp = kpg.genKeyPair();
        
        return kp;
    }

    public byte[] generatePFX(String strCertAliasName, Certificate[] userCertChain, KeyPair kp, String strPIN) throws KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException, IOException {
        byte[] m_bap12 = null;
        KeyStore store = KeyStore.getInstance("PKCS12", "BC");
        store.load(null, null);
        store.setKeyEntry(strCertAliasName, kp.getPrivate(), null, userCertChain);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        store.store(baos, strPIN.toCharArray());
        store = null;
        m_bap12 = baos.toByteArray();
        return m_bap12;
    }

    public String generateB64EncodedCSR(KeyPair kp, Vector ordering,Hashtable   attributes) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, SignatureException {
        //X509Name subject = new X509Name(oid, values);
        X509Principal subject=null;
        subject = new X509Principal(ordering, attributes);
        
        String strALGO = LoadSettings.g_sSettings.getProperty("certificate.generation.algorithm");

          PKCS10CertificationRequest req1 = null;
        
        try{
        req1  = new PKCS10CertificationRequest(
                //"SHA1withRSA",
                strALGO,
                subject,
                kp.getPublic(),
                null,
                kp.getPrivate()
                ,"BC");
        }catch(Exception e){
           e.printStackTrace();
        }

        byte[] baDerCerReq = req1.getEncoded();
        return new String(Base64.encode(baDerCerReq));
    }

    public Certificate generateUserCert(Vector ordering,Hashtable   attributes, PublicKey userPublicKey,
            PrivateKey caPrivKey, X509Certificate caCert, int iValidityDays, String strSrNo)
            throws InvalidKeyException, CertificateException, NoSuchAlgorithmException, NoSuchProviderException, SignatureException {
        //X509Name subject = new X509Name(oid, values);
        X509Principal subject=null;
        subject = new X509Principal(ordering, attributes);
        X509V3CertificateGenerator v3CertGen = new X509V3CertificateGenerator();
        if ( strSrNo.startsWith("AP") == false)
            v3CertGen.setSerialNumber(new BigInteger(strSrNo));
        else {
            strSrNo = strSrNo.replaceFirst("AP", "");
            v3CertGen.setSerialNumber(new BigInteger(strSrNo));
        }
        v3CertGen.setIssuerDN(PrincipalUtil.getSubjectX509Principal(caCert));
        v3CertGen.setNotBefore(new Date(System.currentTimeMillis() - 1000L * 60));
        v3CertGen.setNotAfter(new Date(System.currentTimeMillis() + (1000L * 60 * 60 * 24 * iValidityDays)));
        v3CertGen.setSubjectDN(subject);
        v3CertGen.setPublicKey(userPublicKey);
        v3CertGen.setSignatureAlgorithm("SHA1WithRSAEncryption");

        //v3CertGen.addExtension(X509Extensions.SubjectKeyIdentifier, false, new SubjectKeyIdentifierStructure(userPublicKey));
        //v3CertGen.addExtension(X509Extensions.AuthorityKeyIdentifier, false, new AuthorityKeyIdentifierStructure(caCert));
        //v3CertGen.addExtension(X509Extensions.BasicConstraints, true, new BasicConstraints(0));

        X509Certificate cert = v3CertGen.generateX509Certificate(caPrivKey);
        return cert;
    }

//    public static void main(String[] args) {
//        Vector oid = new Vector();
//        Vector values = new Vector();
//        oid.add(X509Principal.C);
//        values.add("MY");
//        oid.add(X509Principal.O);
//        values.add("HLBB");
//        oid.add(X509Principal.OU);
//        values.add("100");
//        oid.add(X509Principal.L);
//        values.add("WP");
//        oid.add(X509Principal.CN);
//        values.add("vikram");
//        oid.add(X509Principal.UID);
//        values.add("Z1800357");
//        oid.add(X509Principal.EmailAddress);
//        values.add("vikram@ezmcom.com");
//
//        try {
//            CertUtil certutil = new CertUtil();
//            KeyPair kp = certutil.generateKeyPair(1024);
//            //String strCSR = certutil.generateB64EncodedCSR(kp, oid, values);
//            //System.out.println(strCSR);
//
//            int iResult = certutil.loadPfxFromFile("C:\\Users\\Vikram\\Desktop\\cert-backup.pfx", "rainbow5", "3505c5d6-8807-4b05-bee3-f382f8bab07f");
//            Certificate user_cert = certutil.generateUserCert(
//                    oid,
//                    values,
//                    kp.getPublic(),
//                    certutil.m_myPrivateKey,
//                    (X509Certificate) certutil.m_myCert,
//                    30,
//                    "98798798");
//
//            Certificate[] userCertChain = new Certificate[2];
//            userCertChain[0] = user_cert;
//            userCertChain[1] = certutil.m_myCert;
//
//
//            byte[] pfx = certutil.generatePFX("vikram's certificat 2", userCertChain, kp, "rainbow5");
//            FileOutputStream fos = new FileOutputStream("C:\\Users\\Vikram\\Desktop\\vikramcert2.pfx");
//            fos.write(pfx);
//            fos.close();
//            String strp12 = new String(Base64.encode(pfx));
//            System.out.println(strp12);
//
//        } catch (Exception e) {
//            System.out.println("Exception:" + e.getMessage());
//        }
//    }
    public int loadPfxFromFile(String strPFXPath, String strPassword, String strCAalias) throws Exception {

        //Security.addProvider(new BouncyCastleProvider()); // add it


        if (strPFXPath.compareTo("") == 0
                || strPassword.compareTo("") == 0
                || strCAalias.compareTo("") == 0) {
            return -1;
        }

        File fp12 = new File(strPFXPath);
        if (!fp12.exists()) {
            throw new Exception("PFX file path is invalid");
        }

        FileInputStream fin = null;
        try {

            fin = new FileInputStream(fp12);
            byte[] buffer = new byte[(int) fp12.length()];
            int iRead = fin.read(buffer);
            //if (iRead !=  fp12.length())
            //System.out.println("the bytes read are not correct." + iRead + " read while file had " + fp12.length());


            ByteArrayInputStream p12ByteArray = new ByteArrayInputStream(buffer);
            KeyStore ks = KeyStore.getInstance("PKCS12", "BC");
            InputStream in = p12ByteArray;
            ks.load(in, strPassword.toCharArray());
            in.close();
            fin.close();


            Enumeration e = ks.aliases();
            boolean b = false;
            while (e.hasMoreElements()) {
                String alias = (String) e.nextElement();
                //System.out.println(alias);
                //myLog.log(Level.FINE, "WORKING ON ALIAS [" + alias + "] FROM THE PFX");
                // Does alias refer to a private key?
                b = ks.isKeyEntry(alias);
                if (b == true) {
                    if (alias.equals(strCAalias) == true) {
                        //System.out.println("MATCHING ALIAS FOUND");
                        Key key = ks.getKey(strCAalias, strPassword.toCharArray());
                        if (key instanceof PrivateKey) {
                            m_myCert = ks.getCertificate(strCAalias);
                            m_myPubKey = m_myCert.getPublicKey();
                            m_myPrivateKey = (PrivateKey) key;
                        }
                    } else {
                        System.out.println("DOES NOT MATCH INPUT ALIAS [" + strCAalias + "]");
                        continue;
                    }
                }
            }
            if (b == false) {
                throw new Exception("Key corresponding to Alias is not present. Please check Alias Name Or password.");
            }
            //end
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                fin.close();
            } catch (IOException ex) {
                throw ex;
            }
        }
        return 0;
    }

    public Certificate generateServerCert(Vector ordering,Hashtable attributes, PublicKey myPublicKey,
            PrivateKey myPrivKey, X500Principal dnName, int iValidityDays, String strSrNo)
            throws InvalidKeyException, CertificateException, NoSuchAlgorithmException, NoSuchProviderException, SignatureException {
        X509Principal subject=null;
        subject = new X509Principal(ordering, attributes);        
        //X509Name subject = new X509Name(oid, values);
        X509V3CertificateGenerator v3CertGen = new X509V3CertificateGenerator();
        v3CertGen.setSerialNumber(new BigInteger(strSrNo));
        //v3CertGen.setIssuerDN(PrincipalUtil.getSubjectX509Principal(caCert));
        v3CertGen.setIssuerDN(dnName);
        v3CertGen.setNotBefore(new Date(System.currentTimeMillis() - 1000L * 60));
        v3CertGen.setNotAfter(new Date(System.currentTimeMillis() + (1000L * 60 * 60 * 24 * iValidityDays)));
        v3CertGen.setSubjectDN(subject);
        v3CertGen.setPublicKey(myPublicKey);
        v3CertGen.setSignatureAlgorithm("SHA1WithRSAEncryption");

        //v3CertGen.addExtension(X509Extensions.SubjectKeyIdentifier, false, new SubjectKeyIdentifierStructure(userPublicKey));
        //v3CertGen.addExtension(X509Extensions.AuthorityKeyIdentifier, false, new AuthorityKeyIdentifierStructure(caCert));
        //v3CertGen.addExtension(X509Extensions.BasicConstraints, true, new BasicConstraints(0));

        X509Certificate cert = v3CertGen.generateX509Certificate(myPrivKey);
        return cert;
    }

    public KeyStore convertPFX(String pkcs12File, String password, boolean saveToFile, String keyStoreType, String jksFile) throws Exception {
        FileInputStream fis = new FileInputStream(pkcs12File);

        //System.out.println("Loading PKCS12 key store from byte array");

        KeyStore ks = KeyStore.getInstance("PKCS12");

        ks.load(fis, password.toCharArray());

        //System.out.println("Initializing " + keyStoreType + " key store");

        KeyStore jksKeyStore = KeyStore.getInstance(keyStoreType);
        jksKeyStore.load(null, null);

        Enumeration aliases = ks.aliases();

        while (aliases.hasMoreElements()) {
            String strAlias = (String) aliases.nextElement();
            System.out.println("Found alias: " + strAlias + " in PKCS12 key store");

            if (ks.isKeyEntry(strAlias)) {
                //System.out.println("Adding key for alias " + strAlias + " to new " + keyStoreType + " key store");
                Key key = ks.getKey(strAlias, password.toCharArray());
                /*
                 * Verify what kind of key it is. If it is a private key we need a certificate chain for it, otherwise we can
                 * not add it to the new key store.
                 */
                if (key instanceof PrivateKey) {
                    Certificate[] chain = ks.getCertificateChain(strAlias);
                    if (chain != null) {
                        jksKeyStore.setKeyEntry(strAlias, key, password.toCharArray(), chain);
                    } else {
                        System.out.println("No certificate chain was available for the private key entry with alias: " + strAlias    + ". This could happen if the PFX has not been created correctly or no friendly name has been specified.");
                    }
                } else {
                    jksKeyStore.setKeyEntry(strAlias, key, password.toCharArray(), null);
                }
            }
        }

        if (saveToFile == true && jksFile != null && jksFile.isEmpty() == false) {

            //System.out.println("Saving " + keyStoreType + " key store to: " + jksFile);

            OutputStream out = new FileOutputStream(jksFile);
            jksKeyStore.store(out, password.toCharArray());
            out.close();
        }
        return jksKeyStore;
    }
    
//    TempFunction(){
//        
//    }
//    
//    PublicKey publicKey = 
//    KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(bytes));
//    PrivateKey privateKey = KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(privateKeyBytes));
//    
}
